/*
cd scraper/ushuru-project/
scrapy crawl pdfox -a google_search_strings="арктика|северный полюс" -a processing_urls="http://www.semantic-web-journal.net/ReviewedAccepted|https://www.semanticscholar.org/paper/"  -a link_filepath="./link_file_test.txt"  -a result_page_quantity=1 -a lang="ru"
```
`-a google_search_strings="арктика|северный полюс"` - search queries separated by `|`
`-a processing_urls="http://url1.com|https://url2.com"` - url for direct visit separated by `|`
*/

/*
 * читать файлы из папки
 * из каждого брать по 5 строк
 * для этих 5 строк запускать команду
 */
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

searchStringsFileDir = "/home/user/IdeaProjects/tsg/launcher/searchStingFiles"
chunkSize = 5
scrapyCrawlerPath = '/home/user/IdeaProjects/tsg/crawler/ushuru-project'
linkFilePath = "/home/user/IdeaProjects/tsg/launcher/output/link_file_test.txt"


new File(searchStringsFileDir).eachFile {
    // === Читаем файл с запросами в список ===
    println "=== Reading file ${it.getName()}..."
    list = []
    it.eachLine { line -> list.add(line) }

    // === Нарезаем список на порции (chunk) - получаем список порций (chunks) ===
    def chunks = []
    for (int i = 0; i < list.size(); i += chunkSize) {
        chunk = list.subList(i, Math.min(list.size(), i + chunkSize))
        chunks.add(chunk)
    }

    // === Для каждой порции ...
    chunks.forEach(chunk -> {
        println("... portion: ${chunk}")

        // ... собираем команду для запуска
        google_search_strings = chunk.join("|")
        google_search_strings = google_search_strings.replace("«", "\"")
        google_search_strings = google_search_strings.replace("»", "\"")

        scrapyLaucher = "scrapy crawl"
        args = "-agoogle_search_strings=\"${google_search_strings}\"" // +
                //" -alink_filepath=\"${linkFilePath}\"  " +
                " -aresult_page_quantity=1 -a lang=\"ru\""
        cmd = []
        cmd.addAll(scrapyLaucher.split(" "))
        cmd.add("pdfox")
        cmd.addAll(args.toString())
        println("=== Start command: ${cmd}")

        // ... запускаем команду ...
        def sout = new StringBuilder(), serr = new StringBuilder()
        def processBuilder = new ProcessBuilder(cmd)
        processBuilder.directory(new File(scrapyCrawlerPath))
        processBuilder.redirectErrorStream(true);
        def process = processBuilder.start()
        InputStreamReader isr = new InputStreamReader(process.getInputStream(), java.nio.charset.StandardCharsets.UTF_8);
        BufferedReader br = new BufferedReader(isr);
        String res;
        while ((res = br.readLine()) != null) {
            println(res)
        }
//        System.exit(0)
    })
    //================
    // создать подпапку - имя = имя файла с тэгами
    // цикл: перенос файлов
    Path filesStoreDir = Paths.get("/home/user/WORK/add_corpus/dwn/full");
    String newDirName = it.getName().substring(0, it.getName().length()-4);
    Path newDir = Paths.get("/home/user/WORK/add_corpus/dwn", newDirName, "full");
    new File(filesStoreDir).eachFile {
        Path newDirPath = Paths.get("/home/user/WORK/add_corpus/dwn", newDirName, "full")
        File file = new File(newDirPath.toString());
        // Creating the directory
        boolean bool = file.mkdirs();
        //println(bool)
        Files.move(Paths.get(it.getPath()), newDirPath.resolve(it.getName()), StandardCopyOption.REPLACE_EXISTING);
    }
    println("=== Files moved successfully into: [${newDir}]");

}


