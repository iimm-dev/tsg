## Launcher module
Use Groovy launcher to run the crawler module and form text corpus.

### Install groovy
[http://itisgood.ru/2018/12/26/kak-ustanovit-apache-groovy-v-ubuntu-18-04-ubuntu-16-04/]

### Add groovy to Idea
/home/user/.sdkman/bin/
[https://intellij-support.jetbrains.com/hc/en-us/community/posts/206224119-How-to-configure-new-Groovy-SDK]

### Start script
```
groovy launcher.groovy
``` 
