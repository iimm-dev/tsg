import json
import random
from pathlib import Path
from time import time

import spacy
from spacy.util import minibatch, compounding
from spacy.training import Example

# --- init logger ---
from text_corpora.utils.clogger import get_logger
logger = get_logger("./", "common")


def enable_GPU_for_training():
    """
    GPU using see https://spacy.io/usage#gpu
    To use GPU install FIRST (use correct cuda version - cudaXXX):
    pip install spacy[cuda101]
    ... then
    pip install cupy-cuda101

    pip install  --no-cache-dir cupy-cuda101

    Lemmatizer - spacy-lookups-data:
        pip install spacy-lookups-data

    """
    spacy.require_gpu()


def __load_trainset(path_to_train_JSON_file):
    with open(path_to_train_JSON_file, mode="r", encoding="utf-8") as f:
        logger.info("=== Loading train data from [{}] ===".format(path_to_train_JSON_file))
        TRAIN_DATA = json.loads(f.read())
        logger.info("=== ... got [{}] training instances ===".format(len(TRAIN_DATA)))
        return TRAIN_DATA
    return None


def train_NER_spacy_model(path_to_train_JSON_file, output_dir, model=None, empty_model_lang="ru", n_iter=100):
    TRAIN_DATA = __load_trainset(path_to_train_JSON_file)

    """Load the model, set up the pipeline [nlp] and train the entity recognizer."""
    if model is not None:
        nlp = spacy.load(model)  # load existing spaCy model
        logger.info("=== Loaded model [{}] ===".format(model))
    else:
        nlp = spacy.blank(empty_model_lang)  # create blank Language class
        logger.info("=== Created blank [{}] model ===", empty_model_lang)

    # create the built-in pipeline components and add them to the pipeline
    # nlp.create_pipe works for built-ins that are registered with spaCy
    if "ner" not in nlp.pipe_names:
        ner = nlp.add_pipe("ner", last=True)
    # otherwise, get it so we can add labels
    else:
        ner = nlp.get_pipe("ner")

    nlp.component_names
    # Add component "sentencizer" for sentence boundary detection
    # nlp.add_pipe('sentencizer')
    if 'sentencizer' not in nlp.component_names:
        nlp.add_pipe('sentencizer')

    # add labels/categories to ner component
    for _, annotations in TRAIN_DATA:
        for ent in annotations.get("entities"):
            ner.add_label(ent[2])

    # get names of other pipes to disable them during training
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != "ner"]
    with nlp.disable_pipes(*other_pipes):  # only train NER
        logger.info("=== Start training ===")
        if model is None:  # TODO what if model is provided
            nlp.begin_training()

        # New in v3.0 - see https://spacy.io/usage/v3#migrating-training and https://spacy.io/usage/training#api
        examples = []
        for text, annots in TRAIN_DATA:
            examples.append(Example.from_dict(nlp.make_doc(text), annots))  # Example contains annotated training data
        nlp.initialize(lambda: examples)

        for itn in range(n_iter):
            logger.info("=== Iteration [{}/{}] ===".format(itn, n_iter))
            start_iter_time = time()

            random.shuffle(examples)
            losses = {}
            # batch up the examples using spaCy's minibatch
            batches = minibatch(examples, size=compounding(4.0, 32.0, 1.001))
            batch_num = 0
            for batch in batches:
                # ... some logs
                if batch_num % 100 == 0:
                    logger.info(" iteration [{}/{}] - batch number - [{}] - itertime - [{} minutes]".format( itn, n_iter,  batch_num, round((time()-start_iter_time+1)/60,1)))
                # Train single on single batch
                nlp.update(
                    batch,  # data examples
                    drop=0.5,  # dropout - make it harder to memorise data
                    losses=losses,
                )
                batch_num += 1
            # print("Losses", losses)

    # save trained model to output directory
    if output_dir is not None:
        output_dir = Path(output_dir)
        if not output_dir.exists():
            output_dir.mkdir()
        nlp.to_disk(output_dir)
        logger.info("=== Saved model to [{}] ===".format(output_dir))
        logger.info("=== Train set size [{}] training instances ===".format(len(TRAIN_DATA)))
