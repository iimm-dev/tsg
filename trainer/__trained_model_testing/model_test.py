import spacy
import os
import json


# ===================================================
# === Testing the trained model on test texts =======
# ===================================================


def __collect_testing_texts(input_dir):
    """
    Extract text from files
    :param input_dir: path to text directory
    :return: texts as a string
    """
    filenames = os.listdir(input_dir)
    texts = []
    for file_name in filenames:
        file_path = os.path.join(input_dir, file_name)
        with open(file_path, "r", encoding="utf-8") as file:
            text = file.read()
            texts.append(text)
    text = " ".join(texts)
    return text


def __find_text_entities(text, model_dir):
    """
    Find entities in given text via spacy trained model
    :param text: str
    :param model_dir: path to model directory
    :return: list of entities
    """
    nlp = spacy.load(model_dir)
    doc = nlp(text)
    print("== Total sentences quantity: {}".format(len(list(doc.sents))))
    entities = []
    for sent in doc.sents:
        doc_x = nlp(str(sent))
        for ent in doc_x.ents:
            entities.append(ent.text)
    print("== Total entities quantity: {}".format(len(entities)))
    return entities


def __save_found_entities(path, entities):
    """
    :param path: str, e.g. "../estimator/ru_entities.json"
    :param entities: list
    """
    with open(path, "w") as file:
        file.write(json.dumps(entities, ensure_ascii=False))
