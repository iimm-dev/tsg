from trainer.spacy_training_pipeline import train_NER_spacy_model, enable_GPU_for_training

path_to_train_data = "/home/user/PycharmProjects/tsg-rep/tsg/training/spacy_train_data/train_data_lim.json"
output_model_dir = "/home/user/PycharmProjects/tsg-rep/tsg/training/trained_model"

# if GPU will be used
enable_GPU_for_training()

# to use with cuda11.1 install pytorch by
#  pip install torch==1.8.1+cu111 torchvision==0.9.1+cu111 torchaudio==0.8.1 -f https://download.pytorch.org/whl/torch_stable.html
# torch.cuda.set_device(gpu_id)

# Train blank RU model for NER
train_NER_spacy_model(path_to_train_data, output_model_dir, model=None, empty_model_lang="ru", n_iter=100)