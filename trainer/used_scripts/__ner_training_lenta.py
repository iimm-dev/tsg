from trainer.spacy_training_pipeline import train_NER_spacy_model, enable_GPU_for_training

# path_to_train_data = "/home/user/Downloads/lenta_set/lenta_trainset/trainsets/trainset_part_0.json"
# path_to_train_data = "/home/user/Downloads/lenta_set/lenta_trainset/trainsets/trainset_part_1.json"
# output_model_dir = "/home/user/Downloads/lenta_set/lenta_trainset/trained_model_no_aug"

# path_to_train_data = "/home/user/Downloads/lenta_set/lenta_trainset/out_augm_tmp/output_dir_with_train_files/trainset_part_0.json"
# path_to_train_data = "/home/user/Downloads/lenta_set/lenta_trainset/out_augm_tmp/output_dir_with_train_files/trainset_part_1.json"
# path_to_train_data = "/home/user/Downloads/lenta_set/lenta_trainset/out_augm_tmp/output_dir_with_train_files/trainset_part_2.json"
# path_to_train_data = "/home/user/Downloads/lenta_set/lenta_trainset/out_augm_tmp/output_dir_with_train_files/trainset_part_3.json"
# path_to_train_data = "/home/user/Downloads/lenta_set/lenta_trainset/out_augm_tmp/output_dir_with_train_files/trainset_part_4.json"

path_to_train_data = "/home/user/Downloads/lenta_set/lenta_trainset/out_augm_tmp/preparation_files_with_unextended_entities/finaltrain_data.json"
# input_model_dir = "/home/user/Downloads/lenta_set/lenta_trainset/trained_model_aug_3"
input_model_dir = None
output_model_dir = "/home/user/Downloads/lenta_set/lenta_trainset/trained_model_0_39sec_unextended_entities/"



# if GPU will be used
enable_GPU_for_training()

# Train blank RU model for NER
train_NER_spacy_model(path_to_train_data, output_model_dir, model=input_model_dir, empty_model_lang="ru", n_iter=100)