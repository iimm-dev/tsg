from trainer.spacy_training_pipeline import train_NER_spacy_model, enable_GPU_for_training

path_to_train_data = "/home/user/Downloads/habr_post/habr_trainset/test_output_dir/finaltrain_data.json"
output_model_dir = "/home/user/Downloads/habr_post/habr_trainset/test_output_dir/trained_model"

# if GPU will be used
enable_GPU_for_training()

# Train blank RU model for NER
train_NER_spacy_model(path_to_train_data, output_model_dir, model=None, empty_model_lang="ru", n_iter=100)