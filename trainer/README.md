## Trainer module

For training NER spacy model use `spacy_training_pipeline.py`.

Use `__ner_training_example.py` test script as template for spaCy model training. 
