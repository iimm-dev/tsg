import csv
import os
from typing import List, Iterable, Any
from pathlib import Path
import shutil
import json


def _file_paths(dir: str):
    """
    Generator of file paths in given dir.
    @param dir:
    @return:
    """
    for filename in os.listdir(dir):
        yield os.path.join(dir, filename)


def _row_portions_from_csv_files(dir: str, row_quantity: int):
    """
    Generator of row_portions from csv_files in given dir.
    @param dir:
    @param row_quantity: portion size
    @return:
    """
    rows = []
    for file_path in _file_paths(dir):
        fr = open(file_path, mode="r", encoding="utf8")

        for row in csv.reader(fr):
            rows.append(row)
            if len(rows) == row_quantity:
                t = rows
                rows = []
                yield t

    yield rows


def _rows_from_csv_files(dir: str, encoding="utf8"):
    """
    Generator of rows from csv_files in given dir.
    @param dir:
    @return:
    """
    rows = []
    for file_path in _file_paths(dir):
        fr = open(file_path, mode="r", encoding=encoding)
        for row in csv.reader(fr):
            yield row


def _rows_from_csv_file(file_path: str, encoding="utf8"):
    """
    Generator of rows from csv_file.
    @param file_path:
    @return:
    """
    rows = []
    fr = open(file_path, mode="r", encoding=encoding)
    for row in csv.reader(fr):
        yield row


def force_remove_dirs(dir_paths: List[str]):
    """
    Remove given dirs and all their subdirs and files.
    @param dir_paths:
    @return:
    """
    if not isinstance(dir_paths, list):
        raise TypeError(f"{dir_paths} is not a list ")
    for d in dir_paths:
        dirpath = Path(d)
        if dirpath.exists() and dirpath.is_dir():
            shutil.rmtree(dirpath)


def create_if_not_exists(dir_paths: List[str]):
    if not isinstance(dir_paths, list):
        raise TypeError(f"{dir_paths} is not a list ")
    for d in dir_paths:
        if not os.path.exists(d):
            os.makedirs(d)


def recreate_dirs(dir_paths: List[str]):
    """
    Remove given dirs if exist and create it again.
    @param dir_paths:
    @return:
    """
    if not isinstance(dir_paths, list):
        raise TypeError(f"{dir_paths} is not a list ")
    force_remove_dirs(dir_paths)
    create_if_not_exists(dir_paths)


def is_file_exists(path) -> bool:
    if os.path.exists(path) and os.path.isfile(path):
        return True
    return False


def _rows_to_csv_file(rows: Iterable[str], file_path):
    '''
    Write csv-rows into the file.
    @param rows:
    @param file_path:
    @return:
    '''
    with open(file_path, 'w') as f:
        writer = csv.writer(f)
        for row in rows:
            writer.writerow(row)


def _data_from_json_file(file_path: str):
    with open(file_path, 'r', encoding='utf-8') as file:
        return json.load(file)
