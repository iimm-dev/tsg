import unittest
import augmentator.augmentation as aa
import utils.file_utils as ut

class FileUtilsTestCase(unittest.TestCase):
    def setUp(self):
        self.dir_csv_files_with_borders = "./data/test_csv_files"

    def tearDown(self):
        return

    def test__get_next_portion_of_rows_from_csv_files(self):
        expected_row_quantity = 11
        expected_portion_quantity = 3
        portion_size = 5

        portion_quantity = 0
        row_quantity = 0

        for rows in ut._row_portions_from_csv_files(self.dir_csv_files_with_borders, 5):
            portion_quantity += 1
            row_quantity += len(rows)
            print(rows)
            assert len(rows) <= 5

        assert portion_quantity == expected_portion_quantity
        assert row_quantity == expected_row_quantity

    def test__rows_from_csv_files(self):
        expected_row_quantity = 11
        row_quantity = 0

        rows = []
        for row in ut._rows_from_csv_files(self.dir_csv_files_with_borders):
            rows.append(row)
            row_quantity += 1

        assert int(rows[0][0]) == 0
        assert int(rows[10][0]) == 10
        assert row_quantity == expected_row_quantity
