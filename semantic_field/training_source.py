from gensim.utils import simple_preprocess
import zipfile
import utils.file_utils as fut
import text_corpora.utils.helpers as hp

# --- init logger ---
from text_corpora.utils.clogger import get_logger
logger = get_logger("./", "common")


class GensimTrainingData:
    """
    Return generator to tokenized sentences of a given corpus.
    Each sentence is represented as token list.
    Corpus can be represented as a text file directory or an archive (e.g. "docs.zip/docs/text_1...text_n").

    """
    def __init__(self, training_source, make_lemmatization=False):
        """
        :param training_source: str, path to training corpus - set of docs
        :param make_lemmatization: convert each corpus word to normal form
        :return: sentence token list generator
        """

        self._training_source = training_source
        self._make_lemmatization = make_lemmatization

    def __iter__(self):
        """
        :return: sentence token list generator
        """
        for file in self._training_source.read_files():
            for sentence in hp.sentences(file):
                # assume there's one sentence per line, tokens separated by whitespace
                tokenized_sentence = simple_preprocess(sentence, max_len=20)
                tokenized_sentence_without_stopwords = [token for token in tokenized_sentence
                                                        if token not in hp.stop_words()]
                if self._make_lemmatization:
                    normalized_tokenized_sentence = [hp.get_lemma(token) for token in tokenized_sentence_without_stopwords]
                    yield normalized_tokenized_sentence
                yield tokenized_sentence_without_stopwords


class DirectorySource:
    def __init__(self, source_path):
        self._source_path = source_path

    def read_files(self):
        for file_name in fut._file_paths(self._source_path):
            logger.info(f'=== Processing {file_name} doc ===')
            with open(file_name, encoding='utf8') as file:
                yield file.read()


class ArchiveSource:
    def __init__(self, source_path):
        self._source_path = source_path

    def read_files(self):
        if self._source_path.endswith('zip'):
            return self._read_zip_archive()

    def _read_zip_archive(self):
        zip_file = zipfile.ZipFile(self._source_path, 'r')
        for file_name in zip_file.namelist():
            logger.info(f'=== Processing {file_name} doc ===')
            with zip_file.open(file_name) as file:
                yield file.read().decode('utf8')

    def _read_rar_archive(self):
        pass

    def _read_7z_archive(self):
        pass
