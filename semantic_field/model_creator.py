import string
from gensim.models import Word2Vec, KeyedVectors
from gensim.models.phrases import Phrases
from abc import ABC, abstractmethod
from semantic_field.model import Model
from semantic_field.model import GensimModel, SpacyModel, NgramGensimModel
import spacy

# --- init logger ---
from text_corpora.utils.clogger import get_logger

logger = get_logger("./", "common")


class ModelCreator(ABC):
    @abstractmethod
    def create_model(self, model_path: string) -> Model:
        pass

    @abstractmethod
    def load_model(self, model_path: string) -> Model:
        pass

    @abstractmethod     # not needed
    def save_model(self, model_path: string) -> Model:
        pass


class NgramGensimModelCreator(ModelCreator):
    def __init__(self, training_data=None, ngram_size=2, use_lite_model=False):
        self._training_data = training_data
        self._ngram_size = ngram_size
        self._use_lite_model = use_lite_model

    def create_model(self, model_path: string) -> Model:
        logger.info('=== Start training Word2vec n-gram model ===')
        ngrams = self._form_ngrams(self._training_data)
        ngram_model = Word2Vec(ngrams[self._training_data], min_count=1)
        logger.info('=== Finish training Word2vec n-gram model ===')
        if self._use_lite_model:
            logger.info('=== Saving the lite model... ===')
            word_vectors = ngram_model.wv
            return NgramGensimModel(word_vectors.save(model_path), self._ngram_size)
        return NgramGensimModel(ngram_model.save(model_path), self._ngram_size)

    def load_model(self, model_path: string) -> Model:
        if self._use_lite_model:
            logger.info('=== Loading lite (KeyedVectors) n-gram model ===')
            return NgramGensimModel(KeyedVectors.load(model_path), self._ngram_size)
        logger.info('=== Loading Word2vec n-gram model ===')
        return NgramGensimModel(Word2Vec.load(model_path), self._ngram_size)

    def save_model(self, model_path: string) -> Model:
        pass   # TODO to implement

    def _form_ngrams(self, sentences):
        """
        Form n-grams of a given size
        :param sentences: each sentence is represented as token list
        """
        if self._ngram_size in [2, 3]:
            bigrams = Phrases(sentences, min_count=1, threshold=1, delimiter=' ')
            if self._ngram_size == 3:
                trigrams = Phrases(bigrams[sentences], min_count=1, threshold=1, delimiter=' ')
                return trigrams
            return bigrams
        else:
            raise ValueError("The max n-gram size = '3'")


class GensimModelCreator(ModelCreator):
    def __init__(self, training_data=None):
        self._training_data = training_data

    def create_model(self, model_path: string) -> Model:
        logger.info('=== Start training Word2vec model ===')
        model = Word2Vec(self._training_data, min_count=1)
        logger.info('=== Finish training Word2vec model ===')
        return GensimModel(model.save(model_path))

    def load_model(self, model_path: string) -> Model:
        logger.info('=== Loading Word2vec model ===')
        return GensimModel(Word2Vec.load(model_path))

    def save_model(self, model_path: string) -> Model:
        pass   # TODO to implement


class SpacyModelCreator(ModelCreator):

    def create_model(self, model_path) -> Model:
        pass   # TODO to implement

    def load_model(self, model_path: string) -> Model:
        return SpacyModel(spacy.load(model_path))

    def save_model(self, model_path: string) -> Model:
        pass  # TODO to implement
