import logging
import string
import re
import text_corpora.utils.helpers as hp
from abc import ABC, abstractmethod
from collections import OrderedDict
from statistics import mean
from nltk.util import ngrams
from typing import List, Tuple

# --- init logger ---
from text_corpora.utils.clogger import get_logger

logger = get_logger("./", "common")
logger.setLevel(logging.ERROR)


class Model(ABC):
    @abstractmethod
    def get_similarities(self, sentence: string, domain_words: List[Tuple[str]], make_lemmatization=False) -> OrderedDict:
        """
        Get descending sorted list of sentence tokens and their similarity values with domain words
        :param sentence: str
        :param domain_words: List[Tuple[str]] e.g. List[("пакет", "акций"), ("пакет",) ...]
        :param make_lemmatization: convert each word to normal form
        :return: OrderedDict(token : similarity value])
        """
        pass

    def _get_sentence_words(self, sentence: string, make_lemmatization=False):
        """
        :param sentence: str
        :param make_lemmatization: convert each word to normal form
        :return: list
        """
        sentence = re.compile('[^А-Яа-яA-Za-z]').sub(' ', sentence)
        sentence_words = [word.lower() for word in hp.get_words(sentence) if word.lower() not in hp.stop_words()
                          and word not in string.punctuation]
        if make_lemmatization:
            normalized_sentence_words = [hp.get_lemma(word) for word in sentence_words]
            return normalized_sentence_words
        return sentence_words

    def _get_processed_domain_words(self, domain_words: List[Tuple[str]], make_lemmatization=False):
        """
        :param domain_words: List[Tuple[str]] e.g. List[("пакет", "акций"), ("пакет",) ...]
        :param make_lemmatization: convert each word to normal form
        :return: list
        """
        domain_words_to_string_list = [' '.join(word) for word in domain_words]
        processed_domain_words = [word.lower() for word in domain_words_to_string_list]
        if make_lemmatization:
            normalized_domain_words = [hp.get_ngram_lemmas(ngram) for ngram in processed_domain_words]
            return normalized_domain_words
        return processed_domain_words


class NgramGensimModel(Model):
    def __init__(self, gensim_model, ngram_size=2, use_lite_model=False):
        self._model = gensim_model
        self._ngram_size = ngram_size
        self._use_lite_model = use_lite_model

    def get_similarities(self, sentence: string, domain_words: List[Tuple[str]], make_lemmatization=False) -> OrderedDict:
        # logger.info('=== Start - get_similarities via Gensim model with n-grams ===')
        sentence_tokens = self._get_sentence_words(sentence, make_lemmatization=make_lemmatization)
        domain_ngrams = self._get_processed_domain_words(domain_words, make_lemmatization=make_lemmatization)

        sentence_ngrams = [' '.join(ngram) for ngram in ngrams(sentence_tokens, self._ngram_size)]
        sentence_ngrams.extend(sentence_tokens)

        sent_ngram_with_domain_ngrams_similarities = {}
        if self._use_lite_model:
            for sentence_ngram in sentence_ngrams:
                sent_ngram_with_domain_tokens_similarity = self._get_similarity_via_lite_model(self._model,
                                                                                               sentence_ngram,
                                                                                               domain_ngrams)
                sent_ngram_with_domain_ngrams_similarities[sentence_ngram] = sent_ngram_with_domain_tokens_similarity
        else:
            for sentence_ngram in sentence_ngrams:
                sent_ngram_with_domain_tokens_similarity = self._get_similarity_via_full_model(self._model,
                                                                                               sentence_ngram,
                                                                                               domain_ngrams)
                sent_ngram_with_domain_ngrams_similarities[sentence_ngram] = sent_ngram_with_domain_tokens_similarity
        sorted_sent_ngram_with_domain_ngrams_similarities = OrderedDict(
            sorted(sent_ngram_with_domain_ngrams_similarities.items(), key=lambda x: x[1], reverse=True))
        # logger.info('=== End - get_similarities via Gensim model with n-grams ===')
        return sorted_sent_ngram_with_domain_ngrams_similarities

    def _get_similarity_via_full_model(self, model, sentence_ngram: string, domain_ngrams: List[str]):
        if sentence_ngram in model.wv.key_to_index.keys():
            # ..counts average similarity value between all similarity values of sentence ngram and all domain words
            sent_ngram_with_domain_ngrams_similarity = mean([float(model.wv.similarity(sentence_ngram, domain_ngram))
                                                             for domain_ngram in domain_ngrams if domain_ngram in model.wv.key_to_index.keys()])
            return sent_ngram_with_domain_ngrams_similarity
        else:
            # logger.info(f"=== Word '{sentence_ngram}' is OOV - skip... ===")
            return 0

    def _get_similarity_via_lite_model(self, model, sentence_ngram: string, domain_ngrams: List[str]):
        if sentence_ngram in model.key_to_index.keys():
            # ..counts average similarity value between all similarity values of sentence ngram and all domain words
            sent_ngram_with_domain_ngrams_similarity = mean([float(model.similarity(sentence_ngram, domain_ngram))
                                                             for domain_ngram in domain_ngrams if domain_ngram in model.key_to_index.keys()])
            return sent_ngram_with_domain_ngrams_similarity
        else:
            logger.info(f"=== Word '{sentence_ngram}' is OOV - skip... ===")
            return 0


class GensimModel(Model):
    def __init__(self, gensim_model):
        self._model = gensim_model

    def get_similarities(self, sentence: string, domain_words: List[Tuple[str]], make_lemmatization=False) -> OrderedDict:
        logger.info('=== Start - get_similarities via Gensim model ===')
        sentence_tokens = self._get_sentence_words(sentence, make_lemmatization=make_lemmatization)
        domain_tokens = self._get_processed_domain_words(domain_words, make_lemmatization=make_lemmatization)

        sent_token_with_domain_tokens_similarities = {}
        for sentence_token in sentence_tokens:
            if sentence_token in self._model.wv.key_to_index.keys():
                # ..counts average similarity value between all similarity values of sentence token and all domain words
                sent_token_with_domain_tokens_similarity = mean(
                    [float(self._model.wv.similarity(sentence_token, domain_token))
                     for domain_token in domain_tokens if domain_token in self._model.wv.key_to_index.keys()])
                sent_token_with_domain_tokens_similarities[sentence_token] = sent_token_with_domain_tokens_similarity
            else:
                logger.info(f"=== Word '{sentence_token}' is OOV - skip... ===")
                sent_token_with_domain_tokens_similarities[sentence_token] = 0
        sorted_sent_token_with_domain_tokens_similarities = OrderedDict(
            sorted(sent_token_with_domain_tokens_similarities.items(), key=lambda x: x[1], reverse=True))
        logger.info('=== End - get_similarities via Gensim model ===')
        return sorted_sent_token_with_domain_tokens_similarities


class SpacyModel(Model):
    def __init__(self, spacy_model):
        self._model = spacy_model

    def get_similarities(self, sentence: string, domain_words: List[Tuple[str]], make_lemmatization=False) -> OrderedDict:
        logger.info('=== Start - get_similarities via spaCy model ===')
        nlp = self._model

        sentence_tokens = nlp(' '.join(self._get_sentence_words(sentence, make_lemmatization=make_lemmatization)))
        domain_tokens = nlp(' '.join(self._get_processed_domain_words(domain_words, make_lemmatization=make_lemmatization)))

        sent_token_with_domain_tokens_similarities = {}
        for sentence_token in sentence_tokens:
            # ..counts average similarity value between all similarity values of sentence token and all domain words
            # see https://spacy.io/usage/linguistic-features#vectors-similarity
            sent_token_with_domain_tokens_similarity = mean(
                [float(sentence_token.similarity(domain_token)) for domain_token in domain_tokens])
            sent_token_with_domain_tokens_similarities[sentence_token] = sent_token_with_domain_tokens_similarity
        sorted_sent_token_with_domain_tokens_similarities = OrderedDict(
            sorted(sent_token_with_domain_tokens_similarities.items(),
                   key=lambda x: x[1], reverse=True))
        logger.info('=== End - get_similarities via spaCy model ===')
        return sorted_sent_token_with_domain_tokens_similarities
