from semantic_field.model_creator import *
from semantic_field.training_source import *


# ======================================================================
# === Create semantic field (calculating similarity between words) =====
# ======================================================================

# === Init paths ===
in_dir_with_files = "./test/data/docs/"
in_dir_with_zipfile = "./test/data/docs.zip"
out_path_to_trained_model = "./test/data/model.bin"
spacy_model_name = "ru_core_news_sm"

# === Init training source ===
# ...use directory with files
training_source = DirectorySource(in_dir_with_files)
# ...or use archive with files
training_source = ArchiveSource(in_dir_with_zipfile)
# ...get training data
training_data = GensimTrainingData(training_source)

# === Create Gensim model to calculate similarity between words ===
# creator = GensimModelCreator(training_data)
creator = NgramGensimModelCreator(training_data, ngram_size=2)
creator.create_model(out_path_to_trained_model)


# ==== Load trained Word2Vec model or existing model ===
gensim_model = creator.load_model(out_path_to_trained_model)

# OR - load spaCy model to calculate similarity between words
creator = SpacyModelCreator()
spacy_model = creator.load_model(spacy_model_name)

# === Create semantic field ===
sentence = 'Генеральное представительство Чечни в Российской Федерации намерено временно приостановить свою деятельность.'
domain_words = [('Украина',), ('кандидат',), ('Москва',), ('милиция',)]

# ...calculate similarities via Gensim model
# NOTE: make sure that make_lemmatization value is the same as the model
gensim_model.get_similarities(sentence, domain_words, make_lemmatization=False)

# ...or calculate similarities via spaCy model
spacy_model.get_similarities(sentence, domain_words)
