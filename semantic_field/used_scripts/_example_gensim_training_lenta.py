from gensim.utils import simple_preprocess
from gensim.models import Word2Vec
import zipfile

from nltk.corpus import stopwords
stop_words = stopwords.words('russian')

# =======================================================
# =========== !!! WARNING!!! ============================
# This example from GoogleColab demonstrates how the ====
# initial model was trained on the entire Lenta corpus ==
# ========= !not for local use! =========================
# =======================================================


class LentaModel:
    def __init__(self, zipfile_path):
        self.zipfile = zipfile.ZipFile(zipfile_path, 'r')

    def __iter__(self):
        for file_name in self.zipfile.namelist():
            print('Processing {} doc'.format(file_name))
            for line in self.zipfile.open(file_name):
                # tokenize
                tokenized_list = simple_preprocess(line)
                tokenized_list_without_stopwords = [token for token in tokenized_list if token not in stop_words]
                yield tokenized_list_without_stopwords


# Train the Model
path_to_zipfile = '/content/drive/My Drive/Work/lenta_news/docs.zip'
model = Word2Vec(LentaModel(path_to_zipfile), min_count=1)
print(model)

# Save Model
model.save('/content/drive/My Drive/Work/lenta_news/lenta_model.bin')

# Check Model
model = Word2Vec.load('/content/drive/My Drive/Work/lenta_news/lenta_model.bin')
sims = model.wv.most_similar('россия', topn=10)
print(sims)

pairs = [
    ('россия', 'путин'),
    ('россия', 'украина'),
    ('россия', 'навальный'),
    ('россия', 'москва'),
    ('россия', 'полиция'),
]
for w1, w2 in pairs:
    print('%r\t%r\t%.2f' % (w1, w2, model.wv.similarity(w1, w2)))
