import unittest
from semantic_field.model_creator import *
import os
from text_corpora.utils.utils import remove_file
from semantic_field.training_source import *


class SemanticFieldCreationCase(unittest.TestCase):

    def setUp(self):

        upper_dir, current_dir = os.path.split(os.path.dirname(os.getcwd()))
        training_source_path = os.path.join(upper_dir, "semantic_field", "test", "data", "docs.zip")
        self.path_to_trained_model = os.path.join(upper_dir, "semantic_field", "test", "data", "model.bin")
        spacy_model_name = "ru_core_news_sm"

        # get gensim training data
        training_source = ArchiveSource(training_source_path)
        training_data = GensimTrainingData(training_source)
        # create gensim model
        gensim_creator = NgramGensimModelCreator(training_data)
        gensim_creator.create_model(self.path_to_trained_model)
        # load gensim model
        self.gensim_model = gensim_creator.load_model(self.path_to_trained_model)

        # load spacy model
        spacy_creator = SpacyModelCreator()
        self.spacy_model = spacy_creator.load_model(spacy_model_name)

        self.domain_words = [("властью",), ("президент", "России"), ("взрывы",)]
        # self.domain_words = ["аэросани", "ледокол", "полярный трактор"]
        # self.domain_words = ['Россия', 'Украина', 'Москва', 'Ирак', 'Грузия', 'Крым', 'Cирия', 'Турция', 'Чечня',
        #                      'Израиль', 'Киргизия', 'Осетия', 'Ливия', 'Великобритания', 'Багдад', 'Париж', 'Курск',
        #                      'Аджария', 'Беслан', 'Нальчик', 'Цхинвали', 'Киев', 'Египет', 'Франция', 'Брюссель',
        #                      'компания', 'страна', 'данные', 'город', 'область', 'процент', 'доллар', 'тысяча',
        #                      'миллион', 'рубль', 'ЮКОС', 'миллиард', 'банк', 'акция', 'выборы', 'представитель',
        #                      'президент', 'глава', 'теракт', 'взрыв', 'власть', 'партия', 'террорист', 'полиция',
        #                      'государство', 'референдум', 'правительство', 'госдума', 'оппозиция', 'коалиция', 'рада',
        #                      'Трамп', 'Путин', 'Арафат', 'Ющенко', 'Литвиненко', 'Голунов', 'Рыбкин', 'Обама', 'Бакиев',
        #                      'Ладен', 'Ходорковский', 'Навальный', 'Янукович', 'Немцов', 'Вороненков', 'Зеленский',
        #                      'Порошенко', 'Соколов', 'сборная', 'команда', 'матч', 'чемпионат', 'игра', 'клуб',
        #                      'результат', 'самолёт', 'метро', 'станция', 'аэропорт', 'вертолёт', 'корабль']

        self.sentence = 'Номеру нанесен ущерб примерно на 1200 фунтов стерлингов , ' \
                        'заявил пресс-секретарь полиции Сассекса.'
        # self.sentence = 'В хрестоматийном толковом словаре Даля лёд определяется как ' \
        #                 '«мёрзлая вода; застывшая и отверделая от стужи жидкость».'
        self.sentence = 'С 2012 по 2014 год он выступал за клуб Континентальной хоккейной лиги «Салават ' \
                        'Юлаев».Российского оппозиционера, председателя незарегистрированной Партии прогресса ' \
                        'Алексея Навального забросали яйцами в Новосибирске, передает ТАСС.'

    def tearDown(self):
        remove_file(self.path_to_trained_model)

    def test__get_normalised_domain_words(self):
        domain_words = self.gensim_model._get_processed_domain_words(self.domain_words)
        domain_words = self.gensim_model._get_processed_domain_words(self.domain_words, make_lemmatization=True)
        print(domain_words)

    def test__get_sentence_words(self):
        sentence_words = self.gensim_model._get_sentence_words(self.sentence)
        sentence_words = self.gensim_model._get_sentence_words(self.sentence, make_lemmatization=True)
        print(sentence_words)

    def test__get_gensim_similarities(self):
        similarities = self.gensim_model.get_similarities(self.sentence, self.domain_words)
        print(similarities)

    def test__get_spacy_similarities(self):
        similarities = self.spacy_model.get_similarities(self.sentence, self.domain_words)
        print(similarities)
