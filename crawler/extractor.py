import textract
import os
import docx2txt


def write_file(file_path, text):
    """ 
    Write text to file 
    """
    with open(file_path, "w") as file:
        file.write(text)


def extract_text(input_dir, output_dir):
    """
    Extract text from files of a given directory
    :param input_dir: path to source directory
    :param output_dir: path to output directory
    """
    filenames = os.listdir(input_dir)

    for file_name in filenames:
        out_file_path = os.path.join(output_dir, os.path.splitext(file_name)[0] + ".txt")
        try:
            if file_name.endswith(".pdf"):
                file_path = os.path.join(input_dir, file_name)
                text = textract.process(file_path, method='pdftotext')
                text = text.decode(encoding="utf-8")
                write_file(out_file_path, text)
                print("File created: {}".format(out_file_path))

            elif file_name.endswith(".docx"):
                file_path = os.path.join(input_dir, file_name)
                text = docx2txt.process(file_path)
                write_file(out_file_path, text)
                print("File created: {}".format(out_file_path))

            else:
                file_path = os.path.join(input_dir, file_name)
                text = textract.process(file_path)
                text = text.decode(encoding="utf-8")
                write_file(out_file_path, text)
                print("File created: {}".format(out_file_path))

        except TypeError:
            print('File', file_name, 'cannot be extracted! - skipped')
        except textract.exceptions.ShellError:
            print('File', file_name, 'cannot be decoded! - skipped')
        except UnicodeDecodeError:
            print('File', file_name, 'cannot be decoded (charmap codec cannot decode bytes)! - skipped')
