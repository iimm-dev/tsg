from unittest import TestCase
from ..ushuru.spiders.ggsearcher import GoogleSearcher
import os


class TestGoogleSearcher(TestCase):
    link_filepath = "./link_file_test.txt"

    def setUp(self):
        self.searcher = GoogleSearcher(page_quantity=1)
        pass

    def tearDown(self):
        pass

    def test_exec_queries(self):
        pass
        # query_strings = "Власти Норвегии не планируют участвовать в развитии Северного морского пути, " \
        #                 "однако предлагают проверить его на соответствие экологическим стандартам для " \
        #                 "корабельных маршрутов, заявила в интервью газете «Известия» министр иностранных " \
        #                 "дел Норвегии Ине Мари Эриксон Серейде. «Пока маршрут действует в рамках имеющихся " \
        #                 "соглашений. Но следует проверить его на соответствие экологическим стандартам, " \
        #                 "которые выдвигаются для корабельных маршрутов в Арктике. Насколько я вижу, у СМП " \
        #                 "есть серьёзные проблемы в отношении всего ‒ от поисково-спасательных работ и недостаточной " \
        #                 "инфраструктуры на всём протяжении трассы до крайне сурового климата. Это сильно " \
        #                 "усложняет задачу сделать этот маршрут выгодным и коммерчески успешным, как многие " \
        #                 "того хотели бы», — сказала министр.".split(' ')
        #
        # query_strings ="Где искать вода солнце".split(' ')
        #
        # self.searcher.exec_queries(query_strings, query_interval=2)

    def test_collect_links_to_file(self):
        query_strings ="вода солнце".split(' ')
        self.searcher.collect_links_to_file(query_strings, links_filepath=self.link_filepath)
        links=self.searcher.read_saved_links(links_filepath=self.link_filepath)
        print("=== test_collect_links_to_file - get [{}] links".format(len(links)))
        # if os.path.exists(self.link_filepath):
        #     os.remove(self.link_filepath)

        query_strings = "zzzzzzzxxxxxxxxxxxxxxcccccccccccc zzzzzzzxxxxxxxxxxxxxxcccccccccccczz".split(' ')
        self.searcher.collect_links_to_file(query_strings, links_filepath=self.link_filepath)
        links=self.searcher.read_saved_links(links_filepath=self.link_filepath)
        print("=== test_collect_links_to_file - get [{}] links".format(len(links)))
        # if os.path.exists(self.link_filepath):
        #     os.remove(self.link_filepath)

