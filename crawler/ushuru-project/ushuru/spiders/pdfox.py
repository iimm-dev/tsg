import scrapy
from scrapy.loader import ItemLoader
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.link import Link
from ..items import FileItem
from .ggsearcher import GoogleSearcher
import logging
import re
import scrapy.pipelines.files
logger = logging.getLogger()


class HrefLinkExtractor(LinkExtractor):
    """
    Get all links from <A href=link> tags
    """

    def extract_links(self, response):
        # Get all link in <a href>
        selectors = response.css("a[href]::attr(href)")
        txt_links = [l for s in selectors for l in s.getall()]
        url_links = []
        for link in txt_links:
            lnk = Link(link)
            url_links.append(lnk)
            logger.info('Extracted link: [%s]', link)

        return url_links[0:2]


class PDFox(CrawlSpider):
    name = 'pdfox'
    search_string_separator = '|'

    def __init__(self, google_search_strings='',
                 result_page_quantity=1,
                 processing_urls='',
                 link_filepath="scraper/links.txt",
                 lang="ru",
                 file_extensions='.pdf|.docx|.txt'):
        self.google_search_strings = google_search_strings.split(self.search_string_separator)
        self.result_page_quantity = result_page_quantity
        self.lang = lang
        self.processing_urls = [u for u in processing_urls.split(self.search_string_separator) if PDFox.is_valid(u)]
        self.link_filepath = link_filepath
        self.file_extensions = file_extensions.split(self.search_string_separator)
        self.google_searcher = GoogleSearcher(lang=self.lang,
                                              page_quantity=self.result_page_quantity)

    def start_requests(self):
        """
        must return an iterable of Requests (you can return a list of requests or write a generator function)
        which the Spider will begin to crawl from. Subsequent requests will be generated successively
        from these initial requests.
        :return: [Request]
        """

        logger.info("=== Send [" + ", ".join(self.google_search_strings) + "] queries to search engine",
                    )
        urls = self.google_searcher.exec_queries(self.google_search_strings)
        logger.info("=== Get [%d] links from searcher", len(urls))
        urls.extend(self.processing_urls)

        saved_links = self.read_saved_links(links_filepath=self.link_filepath)
        logger.info("=== Get [%d] links from file [%s]", len(saved_links), self.link_filepath)
        urls.extend(saved_links)

        logger.info("=== Final url quantity to process [%d]", len(urls))

        for url in urls:
            if self.is_valid(url) is True:
                yield scrapy.Request(url=url, callback=self.parse_links)

    rules = (
        # Rule with extractor to follow deeper url + callback="parse_item"
        Rule(HrefLinkExtractor, callback="parse_links", follow=True),
    )

    def parse_links(self, response):
        """
        The parse() method usually parses the response, extracting the scraped data
        as dicts and also finding new URLs to follow and creating new requests (Request) from them.
        :param response:
        :return:
        """
        logger.info("=== Parse response [%s]", response)
        for url in response.css('a::attr(href)'):
            link = url.extract()
            if link.endswith(tuple(self.file_extensions)):
                logger.info('... Extracted link to download: [%s]', link)
                # Create Items with link for download pipeline
                item_loader = ItemLoader(item=FileItem(), selector=url)
                item_loader.add_value("file_urls", link)
                yield item_loader.load_item()

    def __get_full_url(self, url, response):
        """
        Construct full url if that is needed.
        :param url: url to check and convert
        :param response: scrapy response to get base url part
        :return:
        """
        return url if url.startswith("http") else response.urljoin(url)

    @staticmethod
    def is_valid(url):
        """
        Is that valid url?
        Source https://stackoverflow.com/questions/7160737/python-how-to-validate-a-url-in-python-malformed-or-not
        :param url:
        :return:
        """
        if url:
            regex = re.compile(
                r'^(?:http|ftp)s?://'  # http:// or https://
                r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
                r'localhost|'  # localhost...
                r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
                r'(?::\d+)?'  # optional port
                r'(?:/?|[/?]\S+)$', re.IGNORECASE)
            return re.match(regex, url) is not None

    def read_saved_links(self, links_filepath):
        links = []
        try:
            with open(links_filepath, mode="r") as f:
                links = [line.rstrip('\n') for line in f]
            return list(dict.fromkeys(links).keys())
        except FileNotFoundError:
            return links
