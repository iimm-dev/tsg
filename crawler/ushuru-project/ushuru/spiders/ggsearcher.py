from google import google
from time import sleep
import os
import logging

logger = logging.getLogger()


class GoogleSearcher:
    """
    Search links by google-api. Used by crawlers to get start urls.
    """

    def __init__(self, page_quantity=1, lang="ru"):
        self.page_quantity = int(page_quantity)
        self.lang = lang

    def exec_query(self, search_string):
        search_results = google.search(search_string, pages=self.page_quantity, lang=self.lang)
        return [r.link for r in search_results]

    def exec_queries(self, search_strings, query_interval=0):
        links = []
        for search_string in search_strings:
            logger.info("=== Start - search_string = {}".format(search_string))
            links.extend(self.exec_query(search_string))
            logger.info("=== End - link quantity = {}".format(len(links)))
            sleep(query_interval)
        return links

    def collect_links_to_file(self, search_strings, links_filepath="scraper/link_file.txt", query_interval=0):
        links = self.read_saved_links(links_filepath=links_filepath)
        for search_string in search_strings:
            logger.info("=== Start - search_string = {}".format(search_string))
            obtained_links = self.exec_query(search_string)
            if obtained_links:
                logger.info("=== End - obtained link quantity = {}".format(len(obtained_links)))
                links.extend(obtained_links)
                self.save_links(links, links_filepath=links_filepath)
            else:
                logger.info("=== ERR - Empty result obtained (maybe google limits) - exiting ===")
                return links_filepath
        return links_filepath

    def read_saved_links(self, links_filepath="scraper/link_file.txt"):
        links = []
        try:
            with open(links_filepath, mode="r") as f:
                links = [line.rstrip('\n') for line in f]
            return list(dict.fromkeys(links).keys())
        except FileNotFoundError:
            return links

    def save_links(self, links, links_filepath="scraper/link_file.txt"):
        links = [l+os.linesep for l in list(dict.fromkeys(links).keys()) if l is not None]
        with open(links_filepath, mode="w") as f:
            f.writelines(links)
        return links_filepath
