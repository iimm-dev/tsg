# grable



## Scrapy how-to

## Start crawling
Go to the project dir (with `scrapy.cfg` file) and start spider `pdfox`:
```
cd scraper/ushuru-project/
scrapy crawl pdfox -a google_search_strings="арктика|северный полюс" -a processing_urls="http://www.semantic-web-journal.net/ReviewedAccepted|https://www.semanticscholar.org/paper/"  -a link_filepath="./link_file_test.txt"  -a result_page_quantity=1 -a lang="ru"
```
`-a google_search_strings="арктика|северный полюс"` - search queries separated by `|`
`-a processing_urls="http://url1.com|https://url2.com"` - url for direct visit separated by `|`


## Extracting data from webpage
The best way to learn how to extract data with Scrapy is trying selectors using the Scrapy shell:

```scrapy shell 'http://quotes.toscrape.com/page/1/```

... then - get list of `Selectors`:

```
>>> response.css('title')
[<Selector xpath='descendant-or-self::title' data='<title>Quotes to Scrape</title>'>]
```
get text + tag from `Selector`
```
>>> response.css('title').getall()
['<title>Quotes to Scrape</title>']
```
... get text from `Selector`
```
>>> response.css('title::text').getall()
['Quotes to Scrape']
>>> response.css('title::text').get()
'Quotes to Scrape'
```
... get text by regex:
```python
>>> response.css('title::text').re(r'Quotes.*')
['Quotes to Scrape']
>>> response.css('title::text').re(r'Q\w+')
['Quotes']
>>> response.css('title::text').re(r'(\w+) to (\w+)')
['Quotes', 'Scrape']
```
Use [SelectorGadget](https://selectorgadget.com/) - browser extension to quickly define css-selector.

### CSS selectors examples
Get TEXT of tags with links to `pdf`-files:
```
response.css("a[href$=\".pdf\"]::text").get()
```
GET attrib. value (href-value) from tag (see [selecting-attributes](https://docs.scrapy.org/en/latest/topics/selectors.html#selecting-attributes)):
```python
response.css("a[href$=\".pdf\"]::attr(href)").get()
```

### Save exctacted data
Generation of quotes.json file containing 
all scraped items, serialized in JSON or JSON-line:
```
scrapy crawl SPIDER-NAME -o quotes.json
scrapy crawl SPIDER-NAME -o quotes.jl
```

### Download files by extracted urls

From off. doc [download#using-the-files-pipeline](https://docs.scrapy.org/en/latest/topics/media-pipeline.html?highlight=file%20download#using-the-files-pipeline)

1. In a Spider, you scrape an item and put the URLs of the desired into a file_urls field.
2. The item is returned from the spider and goes to the item pipeline.
3. When the item reaches the FilesPipeline, the URLs in the file_urls field are scheduled for download using the standard Scrapy scheduler and downloader (which means the scheduler and downloader middlewares are reused), but with a higher priority, processing them before other pages are scraped. The item remains “locked” at that particular pipeline stage until the files have finish downloading (or fail for some reason).
4. When the files are downloaded, another field (files) will be populated with the results. This field will contain a list of dicts with information about the downloaded files, such as the downloaded path, the original scraped url (taken from the file_urls field) , and the file checksum. The files in the list of the files field will retain the same order of the original file_urls field. If some file failed downloading, an error will be logged and the file won’t be present in the files field.

Define new `Item` in `items.py`:

```python
import scrapy

class FileItem(scrapy.Item):
    file_urls = scrapy.Field()
    files = scrapy.Field()
```
Add creation of `Items` by [ItemLoader](https://docs.scrapy.org/en/latest/topics/loaders.html) in `parse(...)` method:

```python
selectors = response.css("a[href$=\".pdf\"]::attr(href)")
for num,selector in enumerate(selectors):
    if num > 2: return item_loader.load_item() # TODO remove
    url = selector.get()
    item_loader = ItemLoader(item=FileItem(), selector = selector)
    item_loader.add_value("file_urls",url)
    logger.info('Extracted PDF link: [%s]', url)
    yield item_loader.load_item()
```


Enable `filepipeline` in `settings.py`

```python
ITEM_PIPELINES = {'scrapy.pipelines.files.FilesPipeline': 1}
DOWNLOAD_TIMEOUT = 1200
FILES_STORE = "/home/user/PycharmProjects/text-utils/scraper/ushuru/dwn"
```


## Reference
* [off tutorial](https://docs.scrapy.org/en/latest/intro/tutorial.html)
* [css selectors](https://learn.javascript.ru/css-selectors)
