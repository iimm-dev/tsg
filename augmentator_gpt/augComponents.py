import string
import json
from spacy.language import Language
from spacy.language import Doc
from _ast import mod

from transformers import GPT2LMHeadModel, GPT2Tokenizer
import numpy as np
import torch
import spacy
import spacy.pipeline.sentencizer


@Language.component("my_component")
def my_component(doc: Doc) -> Doc:
    for token in doc:
        print(token)
        if token.text == "IMW</s":
            token.is_sent_start = True
    # Do something to the doc here
    print("Do something to the doc here")
    # doc[1].is_sent_start = False
    return doc


def get_sentence_to_entity_items(json_file: string) -> {string: string}:
    sentence_to_entity = {}
    with open(json_file, mode="r", encoding="utf8") as file:
        json_data = json.load(file)
        for n, line in enumerate(json_data):
            str = line[0]
            val = line[1]
            lb = val["entities"][0][0]
            rb = val["entities"][0][1]
            cat = val["entities"][0][2]
            entity = str[lb:rb]
            sentence_to_entity[str] = entity
    return sentence_to_entity


def generate_sample(model_rep_path: string, text: string, max_length=50) -> string:
    tokenizer = GPT2Tokenizer.from_pretrained(model_rep_path)
    model = GPT2LMHeadModel.from_pretrained(model_rep_path).cuda()
    input_ids = tokenizer.encode(text, return_tensors="pt").cuda()
    out = model.generate(input_ids.cuda(), max_length=max_length,
                         repetition_penalty=5.0,
                         do_sample=True, top_k=5, top_p=0.95,
                         temperature=1)
    generated_text = list(map(tokenizer.decode, out))[0]
    return generated_text
