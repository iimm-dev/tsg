from spacy.language import Language
from spacy.language import Doc
import spacy
import spacy.pipeline.sentencizer

'''
Example of how to create custom spacy pipeline.
'''


@Language.component("my_component")
def my_component(doc: Doc) -> Doc:
    # Do something to the doc here
    print("Do something to the doc here")
    # doc[1].is_sent_start = False
    return doc

@Language.component("doc_as_single_sentence_sentencizer")
def doc_as_single_sentence_sentencizer(doc: Doc) -> Doc:
    """
    TODO does't work as expected use default nlp.add_pipe('sentencizer')
    @param doc:
    @return:
    """
    doc[len(doc)-1].is_sent_start = True
    return doc

"""
Construct spacy pipeline.
"""

nlp = spacy.blank("en")
# nlp.add_pipe('sentencizer')
# nlp.add_pipe("my_component", after="sentencizer")
nlp.add_pipe("doc_as_single_sentence_sentencizer")
nlp.add_pipe("my_component", after="doc_as_single_sentence_sentencizer")


# doc = nlp("This is a sentence. This is another sentence.")
doc = nlp("This is single sentence doc.")

for n,sent in enumerate(doc.sents):
    print(n, sent.text)
