from _ast import mod

from transformers import GPT2LMHeadModel, GPT2Tokenizer
import numpy as np
import torch

model_rep_path = "/home/user/Downloads/rugpt2small_habr/"
model_rep_path = "/home/user/Downloads/rugpt2small_habr/checkpoint-184500"

model_name_or_path = model_rep_path
tokenizer = GPT2Tokenizer.from_pretrained(model_name_or_path)
model = GPT2LMHeadModel.from_pretrained(model_name_or_path).cuda()
# text = "Александр Сергеевич Пушкин родился в "
text = """
<s>sent: Apple нормально принимает Qt приложения, саму Qt нужно немного пропатчить.
ent: Apple
"""
input_ids = tokenizer.encode(text, return_tensors="pt").cuda()
# out = model.generate(input_ids.cuda())
out = model.generate(input_ids.cuda(), max_length=50, repetition_penalty=5.0, do_sample=True, top_k=5, top_p=0.95, temperature=1)
generated_text = list(map(tokenizer.decode, out))[0]
print(generated_text)

# AttributeError: module 'torch' has no attribute '__version__'

# from torch.optim.lr_scheduler import SAVE_STATE_WARNING

# np.random.seed(42)
# torch.manual_seed(42)
#
# model_rep_path = "/home/user/PycharmProjects/rugpt2large"
#
# tok = GPT2Tokenizer.from_pretrained(model_rep_path)
# model = GPT2LMHeadModel.from_pretrained(model_rep_path)
# model.cuda()
#
# text = "<s>Тема: «Создает человека природа, но развивает и образует его общество». (В.Т. Белинский)\nСочинение: "
# inpt = tok.encode(text, return_tensors="pt")
#
# out = model.generate(inpt.cuda(), max_length=500, repetition_penalty=5.0, do_sample=True, top_k=5, top_p=0.95, temperature=1)
#
# tok.decode(out[0])