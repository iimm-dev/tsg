import csv
from collections import Counter
from text_corpora.utils.helpers import get_ngram_lemmas
from typing import List


# ======================================================================
# === Get estimates of fullness, precision and f-measure ===============
# === (comparison of the test file and the result file) ==========
# ======================================================================


def read_data_from_csv_file(file_path: str, encoding='windows-1251', row_delimiter=','):
    """
    :param file_path: path to file
    :param encoding: windows-1251
    :param row_delimiter: depends on file format, e.g. ';' or ','
    :return: [data]
    """
    data = []
    with open(file_path, 'r', encoding=encoding) as csv_file:
        for row in csv.reader(csv_file, delimiter=row_delimiter):
            if len(row) > 0:
                data.append(row)
    return data


def get_entity_lemmas(entities: List):
    """
    :param entities: entity list
    :return: entity list in normal form
    """
    return [get_ngram_lemmas(entity) for entity in entities]


def get_overall_estimates(test_entities: List, result_entities: List):
    """
    Count precision, fullness and f-measure for 2 data lists
    :param test_entities: [entities] from test_file
    :param result_entities: [entities] from result_file
    :return: {"precision": 0, "fullness": 0, "f-measure": 0}
    """
    overall_estimates = {}
    res_test_intersection = list((Counter(test_entities) & Counter(result_entities)).elements())
    precision = len(res_test_intersection) / len(result_entities) if len(result_entities) > 0 else 0
    fullness = len(res_test_intersection) / len(test_entities) if len(test_entities) > 0 else 0
    f_measure = 2 * ((precision * fullness) / (precision + fullness))
    overall_estimates["precision"] = precision
    overall_estimates["fullness"] = fullness
    overall_estimates["f-measure"] = f_measure
    return overall_estimates
