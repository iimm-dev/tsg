## Estimator module

Use `estimator.py` script to get estimates of fullness, precision and f-measure when comparing 
validation and result files. 

Use `__estimator_example.py` test script as estimator template. 
