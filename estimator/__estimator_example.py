import os
from setgen.raw_trainset_generation import get_entities
import estimator.main_estimator as est
from utils.file_utils import _data_from_json_file


# ==== Init logger ======
from text_corpora.utils.corpus_readers import get_logger
logger = get_logger("./", "common")

# ==== Init test and result files ========
# ... File that contains the expected entities that the model should find
test_file = os.path.abspath(os.path.join("data", "test_set.csv"))
# ... File that contains the entities that the model actually found
result_file = os.path.abspath(os.path.join("data", "ru_entities.json"))

# === Init for comparison - file with entities that were found using the multi-language spaCy model ===
# result_file = os.path.abspath(os.path.join("data", "multi-lang_entities.json"))

# === Init file with new entities that can be found as a result of the trained model ===
new_entities_file = os.path.abspath(os.path.join("data", "test_set_new_entities.csv"))


# === Get categoryToEntitiesDict (e.g ontology classes and individuals) ===
# === e.g. {"ТСДПД": ["полярное исполнение", "ледокол", "белая сова"]} ====
categoryToEntitiesDict_file = os.path.abspath(os.path.join("..", "text_corpora", "test_text_corpus",
                                                           "resources", "category_to_entities_dict.json"))
categoryToEntitiesDict = _data_from_json_file(categoryToEntitiesDict_file)
# ontology_entities = get_entities(categoryToEntitiesDict)
ontology_entities = [w for words in categoryToEntitiesDict.values() for w in words]
ontology_entities = [w for words in ontology_entities for w in words]


# === Get test entities - entities that the model should find ===
test_data = est.read_data_from_csv_file(test_file, row_delimiter=';')
# ...choose only needed data from each data row for evaluation
test_entities = [row[1] for row in test_data if row[1] is not None]
# === Get result entities - entities that the model found ===
result_entities = _data_from_json_file(result_file)

# === Get result and ontology lemmas ==========
# ... ontology entities in normal form
ontology_lemmas = est.get_entity_lemmas(ontology_entities)
# ... entities that the model found - in normal form
result_lemmas = est.get_entity_lemmas(result_entities)

# === New entities (e.g. that are not in the ontology) ===
new_result_lemmas = [r_lemma for r_lemma in result_lemmas if r_lemma not in ontology_lemmas]
new_test_data = est.read_data_from_csv_file(new_entities_file, row_delimiter=';')
# ...choose only needed data from each data row for evaluation
new_entities = [row[1] for row in new_test_data if row[1] is not None]
new_test_lemmas = est.get_entity_lemmas(new_entities)

logger.info("Overall estimates = {}".format(est.get_overall_estimates(test_entities, result_entities)))
logger.info("Estimates for new entities = {}".format(est.get_overall_estimates(new_test_lemmas, new_result_lemmas)))


# ==== Relation evaluation ===
result_file = os.path.abspath(os.path.join("..", "entity_enricher", "test", "data",
                                           "main_result_set_all_relations.csv"))
test_file = os.path.abspath(os.path.join("..", "entity_enricher", "test", "data", "main_test_set.csv"))

result_data = est.read_data_from_csv_file(result_file)
result_relations = ["_".join([row[0], row[1]]) for row in result_data]
logger.info("result_relations - {}".format(len(result_relations)))

test_data = est.read_data_from_csv_file(test_file, row_delimiter=';')
test_relations = []
for row in test_data:
    # ...to consider relations with direct and reverse order of entities
    test_relations.append("_".join([row[1], row[2]]))
    test_relations.append("_".join([row[2], row[1]]))
logger.info("test_relations - {}".format(len(test_relations)))

logger.info("Overall estimates = {}".format(est.get_overall_estimates(test_relations, result_relations)))
