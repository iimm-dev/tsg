import string
from typing import List, Tuple

from spacy.tokens import Doc, Token

from entity_enricher.helpers import tokens_to_string
from entity_enricher.helpers import sort_tokens_by_order_in_sentence

import enum


class RelationType(enum.Enum):
    '''
    RelationType - type of relation.
    Use:  some_relation.type = RelationType.unknown
    '''
    unknown = "UNK"
    kind_of = "KIND_OF"

    def __str__(self):
        return str(self.value)


class RelationDTO:

    def __init__(self):
        self.conceptA = []  # List[Token]
        self.conceptB = []  # List[Token]
        self.type = RelationType.unknown
        self.info = ""

    def __str__(self):

        if len(self.info) > 0:
            relation = f"{self.type}, {self.info}"
        else:
            relation = f"{self.type}"

        return f"({tokens_to_string(self.conceptA)})--[{relation}]--({tokens_to_string(self.conceptB)})"

    def __hash__(self):
        all_tokens = []
        all_tokens.extend(self.conceptA)
        all_tokens.extend(self.conceptB)
        all_tokens_sorted_by_text = sorted(all_tokens, key=lambda t: t.text)

        string_of_all_sorted_tokens = tokens_to_string(all_tokens_sorted_by_text)
        rel = (self.type.value, self.info, string_of_all_sorted_tokens)
        return rel.__hash__()

    def __eq__(self, other):
        return isinstance(other, RelationDTO) and self._are_concept_equal(other)

    def _are_concept_equal(self, other_relation):
        conceptA_hash = tokens_to_string(self.conceptA).__hash__()
        conceptB_hash = tokens_to_string(self.conceptB).__hash__()
        conceptA1_hash = tokens_to_string(other_relation.conceptA).__hash__()
        conceptB1_hash = tokens_to_string(other_relation.conceptB).__hash__()
        if conceptA_hash == conceptA1_hash and conceptB_hash == conceptB1_hash:
            return True
        if conceptA_hash == conceptB1_hash and conceptB_hash == conceptA1_hash:
            return True
        return False

    def as_csv_row(self)-> Tuple[str]:
        row = [tokens_to_string(self.conceptA),
               tokens_to_string(self.conceptB),
               self.type,
               self.info]
        return row




def is_relation_with_verb(relation: RelationDTO) -> (bool, Token, List[Token]):
    for t in relation.conceptA:
        if t.pos_ == "VERB":
            return True, t, relation.conceptA
    for t in relation.conceptB:
        if t.pos_ == "VERB":
            return True, t, relation.conceptB
    return False, None, None


def is_contains_verb(tokens: List[Token]):
    for t in tokens:
        if t.pos_ == "VERB":
            return True
    return False


def get_upper_token(tokens, token_pos=None) -> Token:
    upper_token = tokens[0]
    for token in [t for t in tokens if token_pos is None or t.pos_ == token_pos]:
        if upper_token in [child for child in token.children]:
            upper_token = token
    return upper_token


