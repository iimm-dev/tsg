import os
import unittest
from typing import Tuple, List

import spacy
from spacy.tokens import Doc, Token

import augmentator.helpers as hp
# import entity_clarifier.entity_clarifier as cl
# import semantic_field. as sf
import entity_enricher.enricher as enr
import entity_enricher.helpers as enrhp
import entity_enricher.st_graph as stgraph
# import augmentator.augmentation as aa
import utils.file_utils as ut
from entity_enricher.class_relation import RelationDTO
from entity_enricher.class_relation import RelationType
from entity_enricher.relation_heuristics import join_relations_by_verbs
from entity_enricher.relation_heuristics import try_to_assign_isKindOf_type_to_relations
from entity_enricher.helpers import are_neighbour_tokens
from entity_enricher.helpers import get_combinations_with_good_closeness
from entity_enricher.helpers import is_domain_concept
from entity_enricher.helpers import print_tree
from entity_enricher.helpers import sort_tokens_by_order_in_sentence
from entity_enricher.st_graph import get_relations_from_paths_and_token_combinations

# import semantic_field as smf
from semantic_field import model_creator
from semantic_field.model import Model
from entity_enricher.test.domain_words import test_domain_words


class EntityEnricherTestCase(unittest.TestCase):
    def setUp(self):
        self.init()
        self.base_words = test_domain_words
        self.similarity_model_threshold = 0.04
        return

    def tearDown(self):
        return

    def init(self):
        # init spacy model ...
        self.nlp = spacy.load("ru_core_news_sm")

        cur_model_creator = model_creator.GensimModelCreator()
        cur_model_creator = model_creator.NgramGensimModelCreator()
        # TODO enadle
        self.model = cur_model_creator.load_model("/home/user/Downloads/lenta_set/lenta_model2/bigram_model_full.bin")

    def test_obj_properties_entities(self):

        in_dir = "./data/"
        in_file = os.path.join(in_dir, "section_0_100_lines.csv")

        verb_token_count = 0
        other_token_count = 0

        for row in ut._rows_from_csv_file(in_file):

            sentence = row[0]
            parsed_sent = self.nlp(sentence)

            if parsed_sent is None:
                continue
            # ==============================
            # === lowest + its ancestors ===
            # ==============================
            # print(f"=== {sentence} ")
            # childless_tokens = enr._get_token_with_no_children(parsed_sent)
            # # for t in childless_tokens:
            # #     print(f"{t} -- {t.pos_}")
            #
            # for child_token in childless_tokens:
            #     for ancestor_token in child_token.ancestors:
            #         if enr._is_suitable_token(ancestor_token):
            #             print(f"{ancestor_token} ({ancestor_token.pos_}) --> {child_token}")
            #             if ancestor_token.pos_=="VERB":
            #                 verb_token_count+=1
            #             else:
            #                 other_token_count+=1

            # =============
            # === Gip A ===
            # =============
            print(f"=== {sentence}")
            verb_tokens = enr._get_verb_token(parsed_sent)
            for verb_token in verb_tokens:
                left_children, right_children = enr._get_left_and_right_noun_children(verb_token)
                for l_token in left_children:
                    for r_token in right_children:
                        l_tokens = hp._clarify_entity_tokens_by_nearest_childs_of_head_token([l_token], l_token)
                        r_tokens = hp._clarify_entity_tokens_by_nearest_childs_of_head_token([r_token], r_token)
                        s = " "
                        l_token_str = s.join([t.text for t in l_tokens])
                        r_token_str = s.join([t.text for t in r_tokens])
                        print(f"possible relation: {l_token_str} --{verb_token}-- {r_token_str}")

        print(
            f" === verb_token_count = {verb_token_count}, other_token_count={other_token_count}, total = {verb_token_count + other_token_count}")

        #
        # tmp_output_dir = "./data/out/draft_clarified_entities_border_csv"
        # out_draft_clarified_entities_csv_file = "./data/out/draft_clarified_entities.csv"
        # output_dir = "./data/out/clarified_entities_border_csv"
        #
        # cl.create_clarified_entities(in_dir, out_draft_clarified_entities_csv_file, tmp_output_dir, input_files_quantity = 38)
        #
        # # === draft clarified entity file for manual filtering
        # validated_clarified_entities_file_path = out_draft_clarified_entities_csv_file
        #
        # cl.remove_bad_clarified_entities(tmp_output_dir, validated_clarified_entities_file_path, output_dir)
        #


    def test_single_get_relations(self):

        # sentence="""Со второй половины 2014 года в США набирает силу волна протестов, вызванная жестокими действиями
        # стражей порядка и убийствами безоружных людей из бедных районов сотрудниками полиции."""
        # ent=["полиции"]
        # ent=["действиями"]
        # ent=["набирает"]

        sentence = """Как сообщает The Guardian, во время демонстрации произошли столкновения участников акции с полицией."""
        ent = ["полицией"]

        sentence = """Практически готово соглашение о передаче в траст Rothschild моего пакета акций в Roshen», — сказал глава государства, отметив, что делает это во избежание разного рода спекуляций"""
        ent = ["акций"]

        sentence = '''10 ноября одна из компаний, входящих в «Онэксим», направила основному владельцу ГК «Связной» Максиму Ноготкову уведомление о дефолте по долгу, залогом по которому выступает контрольный пакет акций группы.'''
        ent = ["акций"]

        sentence = '''Адвокатская контора планирует также привлечь к ответственности главу финской 
        компании Йорму Оллилу (Jorma Ollila) и ее финансового директора Ричарда Симонсона (Richard Simonson).'''
        ent = ["главу"]

        sentence = '''Евросоюз является крупнейшим торговым партнером России, и достижение 
        договоренности с ним значительно приблизит момент принятия Москвы в ВТО.'''
        ent = ["России"]
        ent = ["России"]
        ent = ["является"]

        doc = self.nlp(sentence)
        parsed_sent = [sent for sent in doc.sents][0]

        print_tree(parsed_sent.root)

        cur_model_creator = model_creator.GensimModelCreator()
        model = cur_model_creator.load_model("/home/user/Downloads/lenta_set/lenta_model/data/lenta_model.bin")

        entity_tokens = enrhp._get_spacy_tokens_by_text(ent, parsed_sent)
        entity = entity_tokens[0]

        relationDTOs = get_all_relations(parsed_sent, entity, self.base_words, self.similarity_model_threshold, model)

        for r in relationDTOs:
            print(r)

    def test_hierarchy_evristic(self):

        sentence = """Как сообщает агентство Reuters, падение акций Nokia повлияло также на 
        стоимость ценных бумаг других компаний, задействованных в сфере высоких технологий."""

        # TODO ent - это должен быть головной токен ( parsed_sent.root ), а не сущность, тк мы анализируем все предложение
        # ent = ["повлияло"]

        doc = self.nlp(sentence)
        parsed_sent = [sent for sent in doc.sents][0]
        entity = parsed_sent.root
        print_tree(parsed_sent.root)

        # cur_model_creator = model_creator.GensimModelCreator()
        # model = cur_model_creator.load_model("/home/user/Downloads/lenta_set/lenta_model/data/lenta_model.bin")
        model = self.model

        # entity_tokens = enrhp._get_spacy_tokens_by_text(ent, parsed_sent)
        # entity = entity_tokens[0]

        relationDTOs = get_all_relations(parsed_sent, entity, self.base_words, self.similarity_model_threshold,
                                         model)

        for r in relationDTOs:
            print(r)

        relationDTOs = join_relations_by_verbs(relationDTOs)
        print("=== after join_relations_by_verbs(relationDTOs) ===")
        for r in relationDTOs:
            print(r)
            # print(f"A:{ get_upper_token(r.conceptA).text} {get_upper_token(r.conceptA).dep_} {get_upper_token(r.conceptA).pos_}")
            # print(f"B:{get_upper_token(r.conceptB).text} {get_upper_token(r.conceptB).dep_} {get_upper_token(r.conceptB).pos_}")

        r = try_to_assign_isKindOf_type_to_relations(relationDTOs)
        print("=== after try_to_assign_IS_KINS_OF_type_to_relations(relationDTOs) ===")
        for r in relationDTOs:
            print(r)

    def test_all_get_obj_properties_in_sentence(self):
        in_dir = "./data/"
        # in_file = os.path.join(in_dir, "section_0_100_lines.csv")
        in_file = os.path.join("/home/user/Downloads/lenta_set/lenta_trainset/experimental_sections", "section_2.csv")

        # === Read domain words ===
        domain_words = []
        for category, words in self.base_words.items():
            for word in words:
                domain_words.append(word)

        model = self.model

        for row in ut._rows_from_csv_file(in_file):
            sentence = row[0]
            doc = self.nlp(sentence)
            parsed_sent = [sent for sent in doc.sents][0]

            if parsed_sent is None:
                continue

            # _,relationDTOs = get_all_relations(parsed_sent, parsed_sent.root, self.base_words, self.similarity_model_threshold , model)
            _, relationDTOs = get_relations_recursively(parsed_sent, parsed_sent.root, self.base_words,
                                                        self.similarity_model_threshold, model, [])

            if len(relationDTOs) > 0:
                print(f"\nsentence >>> {sentence}")
                # print(f"\nentity >>> {entity}")

                print_tree(parsed_sent.root)

                for r in relationDTOs:
                    print(r)

                relationDTOs = join_relations_by_verbs(relationDTOs)
                print("=== after join_relations_by_verbs(relationDTOs) ===")
                for r in relationDTOs:
                    print(r)

    def test_single_get_relations_in_sentence(self):

        # sentence="""Со второй половины 2014 года в США набирает силу волна протестов, вызванная жестокими действиями
        # стражей порядка и убийствами безоружных людей из бедных районов сотрудниками полиции."""
        # ent=["полиции"]
        # ent=["действиями"]
        # ent=["набирает"]

        sentence = """Как сообщает The Guardian, во время демонстрации произошли столкновения участников акции с полицией."""
        ent = ["полицией"]

        sentence = """Практически готово соглашение о передаче в траст Rothschild моего пакета акций в Roshen», — сказал глава государства, отметив, что делает это во избежание разного рода спекуляций"""
        ent = ["акций"]

        sentence = '''10 ноября одна из компаний, входящих в «Онэксим», направила основному владельцу ГК «Связной» Максиму Ноготкову уведомление о дефолте по долгу, залогом по которому выступает контрольный пакет акций группы.'''
        ent = ["акций"]

        sentence = '''Адвокатская контора планирует также привлечь к ответственности главу финской компании Йорму Оллилу (Jorma Ollila) и ее финансового директора Ричарда Симонсона (Richard Simonson).'''
        ent = ["главу"]

        sentence = '''Евросоюз является крупнейшим торговым партнером России, и достижение договоренности с ним значительно приблизит момент принятия Москвы в ВТО.'''
        ent = ["России"]
        ent = ["России"]
        ent = ["является"]

        sentence = '''Напомним, что в 2002 году финансовая разведка контролировала только операции кредитных организаций на сумму свыше 600 тысяч рублей.'''
        ent = ["Напомним"]

        model = self.model

        doc = self.nlp(sentence)
        parsed_sent = [sent for sent in doc.sents][0]

        print_tree(parsed_sent.root)

        _, relationDTOs = get_relations_recursively(parsed_sent, parsed_sent.root, self.base_words,
                                                    self.similarity_model_threshold, model, [])

        # relationDTOs = join_by_root_verb(relationDTOs, parsed_sent.root)
        for r in relationDTOs:
            print(r)

        relationDTOs = join_relations_by_verbs(relationDTOs)
        print("=== after join_relations_by_verbs(relationDTOs) ===")
        for r in relationDTOs:
            print(r)

    # def test_relation_type(self):
    #     tp = RelationType()
    #     print(tp)

    # def test_single_path_based_relation_collection(self):
    #     doc = self.nlp(
    #         """10 ноября одна из компаний, входящих в «Онэксим», направила основному
    #         владельцу ГК «Связной» Максиму Ноготкову уведомление о дефолте по долгу,
    #         залогом по которому выступает контрольный пакет акций группы.""")
    #
    #     parsed_sentence = [sent for sent in doc.sents][0]
    #     print_tree(parsed_sentence.root)
    #     st_token_combs = stgraph.get_2_place_tokens_st_combinations(parsed_sentence.root, [(parsed_sentence.root,)])
    #
    #     all_paths_between_list_tokens = \
    #         stgraph.convert_dependence_tree_to_paths_between_list_tokens(parsed_sentence)
    #
    #     result_st_comb = st_token_combs
    #
    #     relations = set()
    #     for comb in result_st_comb:
    #         neighbour_combs = []
    #
    #         # взять все пути для одной комбинации...
    #         paths = _get_paths_containing_comb(all_paths_between_list_tokens, comb)
    #
    #         # из каждого пути выбирать ближние левые/правые/вложенные комбинации к целевой
    #         for path in paths:
    #             nested_neighbours, \
    #             right_neighbours_and_path_fragments, \
    #             left_neighbours_and_path_fragments = get_all_neighbour_combs_from_path(path, comb, result_st_comb)
    #
    #             for neighbours,path_fragment in right_neighbours_and_path_fragments:
    #                 if path_fragment is None:
    #                     print("R:", comb)
    #
    #             for neighbours,path_fragment in left_neighbours_and_path_fragments:
    #                 if path_fragment is None:
    #                     print("L:", comb)

    def test_path_based_relation_collection(self):
        doc = self.nlp(
            """10 ноября одна из компаний, входящих в «Онэксим», направила основному 
            владельцу ГК «Связной» Максиму Ноготкову уведомление о дефолте по долгу, 
            залогом по которому выступает контрольный пакет акций группы.""")

        parsed_sentence = [sent for sent in doc.sents][0]
        print_tree(parsed_sentence.root)

        st_token_combs = stgraph.get_2_place_tokens_st_combinations(parsed_sentence.root, [(parsed_sentence.root,)])

        # Выбираем сегменты с близостью > порога concept_combos = []
        comb_to_closeness = get_combinations_with_good_closeness(st_token_combs, parsed_sentence,
                                                                 self.base_words,
                                                                 self.similarity_model_threshold,
                                                                 self.model)

        closest_combs = set(comb_to_closeness.keys())

        print("=== result st combs")
        for comb in closest_combs:
            print(f"{comb}:={comb_to_closeness[comb]}")

        paths_between_list_tokens = stgraph.get_paths_between_list_tokens_of_dependence_tree(parsed_sentence)
        relations = get_relations_from_paths_and_token_combinations(closest_combs, paths_between_list_tokens)

        print('=== for r in relations: ==')
        for r in relations:
            print(r)

    def test_tree_to_graph(self):
        sentence = """Как сообщает The Guardian, во время демонстрации произошли столкновения участников акции с полицией."""
        doc = self.nlp(sentence)
        parsed_sentence = [sent for sent in doc.sents][0]
        print_tree(parsed_sentence.root)

        res_graph = stgraph._syntax_tree_to_graph(parsed_sentence.root)
        for node, neighbourns in res_graph.items():
            print(node.text, " : ", ", ".join([str.strip(t.text) for t in neighbourns]))
            # for n in neighbourns:
            #     print(n.text + ",")

    def test_get_graph_paths(self):
        sentence = """Как сообщает The Guardian, во время демонстрации произошли 
        столкновения участников акции с полицией."""
        doc = self.nlp(sentence)
        parsed_sentence = [sent for sent in doc.sents][0]
        print_tree(parsed_sentence.root)

        res_graph = stgraph._syntax_tree_to_graph(parsed_sentence.root)

        paths = stgraph._find_all_paths(res_graph, parsed_sentence[0], parsed_sentence[12])

        for path in paths:
            print(path)

    def test_child(self):
        doc = self.nlp(
            """10 ноября одна из компаний, входящих в «Онэксим», направила основному владельцу ГК 
            «Связной» Максиму Ноготкову уведомление о дефолте по долгу, залогом по которому 
            выступает контрольный пакет акций группы.""")
        word = doc[len(doc) - 4]
        # followers = word.ancestors
        followers = [t for t in word.children if t.head == word]
        print(word)
        # print(word.head)
        print([t.text for t in followers])

    def test_are_nighbours(self):
        sentence = """Как сообщает The Guardian, во время демонстрации произошли столкновения участников акции с полицией."""
        ent = ["полицией"]

        doc = self.nlp(sentence)
        parsed_sent = [sent for sent in doc.sents][0]
        token_a = parsed_sent[1]
        token_b1 = parsed_sent[0]
        token_b2 = parsed_sent[2]
        token_b3_not = parsed_sent[3]
        assert are_neighbour_tokens(token_a, token_b1) == True
        assert are_neighbour_tokens(token_a, token_b2) == True
        assert are_neighbour_tokens(token_a, token_b3_not) == False

        token_a = parsed_sent[0]
        token_b1 = parsed_sent[1]
        token_b3_not = parsed_sent[2]
        assert are_neighbour_tokens(token_a, token_b1) == True
        assert are_neighbour_tokens(token_a, token_b3_not) == False

        token_a = parsed_sent[13]
        token_b1 = parsed_sent[12]
        token_b3_not = parsed_sent[11]
        assert are_neighbour_tokens(token_a, token_b1) == True
        assert are_neighbour_tokens(token_a, token_b3_not) == False


def get_all_relations(parsed_sentence: Doc,
                      cur_token: Token,
                      domain_words,
                      threshold: float,
                      model: Model) -> List[RelationDTO]:
    # sometimes very first concept (initial entity) can be extended by up and/or down processing of syntax-tree
    # ... join these extensions in extended_initial_concept
    _, conceptA_of_first_relations_with_children, relations_with_children = get_relations_recursively(parsed_sentence,
                                                                                                      cur_token,
                                                                                                      domain_words,
                                                                                                      threshold,
                                                                                                      model, [],
                                                                                                      go_down=True)
    _, conceptA_of_first_relations_with_ancestors, relations_with_ancestors = get_relations_recursively(parsed_sentence,
                                                                                                        cur_token,
                                                                                                        domain_words,
                                                                                                        threshold,
                                                                                                        model, [],
                                                                                                        go_down=False)
    relationDTOs = []
    extended_initial_concept = set(conceptA_of_first_relations_with_children).union(
        set(conceptA_of_first_relations_with_ancestors))
    extended_initial_concept = [t for t in extended_initial_concept]

    sort_tokens_by_order_in_sentence(extended_initial_concept)

    if len(relations_with_children) > 0:
        relations_with_children[0].conceptA = extended_initial_concept
    if len(relations_with_ancestors) > 0:
        relations_with_ancestors[0].conceptA = extended_initial_concept

    relationDTOs.extend(relations_with_ancestors)
    relationDTOs.extend(relations_with_children)
    return relationDTOs


def get_relations_recursively(parsed_sentence: Doc,
                              cur_token: Token,
                              domain_words,
                              threshold: float,
                              model: Model,
                              collected_relations: List[RelationDTO],
                              go_down=True) -> Tuple[RelationDTO, Token, List[RelationDTO]]:
    '''
    @param parsed_sentence:
    @param cur_token:
    @param domain_words:
    @param threshold:
    @param model:
    @param collected_relations:
    @param go_down:
    @return: current relation, conceptA of current relation, relations gotten from previous levels
    TODO remove return_current_relation
    '''

    # ... define direction of processing - up or down by syntax-tree
    # next_level_tokens = [t for t in cur_token.children] if go_down else [t for t in cur_token.ancestors]

    if go_down:
        next_level_tokens = [t for t in cur_token.children]
    else:
        next_level_tokens = [cur_token.head] if cur_token.head != cur_token else []

    cur_concept_tokens = [cur_token]

    for next_level_token in next_level_tokens:

        token_is_domain_word, val = is_domain_concept(next_level_token.text, parsed_sentence.text, domain_words, model,
                                                      threshold)
        print(next_level_token.text + "=" + str(val) + "\n")

        if token_is_domain_word:
            # Create relation between current and next_level_concept  ...
            relation = RelationDTO()
            relation.conceptA = cur_concept_tokens

            direction = "- child" if go_down else " ancestor"
            relation.type = RelationType.unknown

            _, next_conceptA, collected_relations = get_relations_recursively(parsed_sentence, next_level_token,
                                                                              domain_words, threshold, model,
                                                                              collected_relations, go_down=go_down)
            # === relation.conceptB will not be None
            # ... conceptB of current relation is conceptA of next one
            relation.conceptB = next_conceptA
            collected_relations.append(relation)
            # return relation, relation.conceptA, collected_relations

        else:

            for token in cur_concept_tokens:
                if are_neighbour_tokens(token, next_level_token):
                    cur_concept_tokens.append(next_level_token)

    return None, cur_concept_tokens, collected_relations  # cur_concept_tokens = relation.subject


# def get_2_tokens_st_combinations(token: Token, combinations: List[Tuple[Token]]) -> (
#         List[Tuple[Token]]):
#     for child in token.children:
#         if not hp.is_suitable_token(child.text):
#             continue
#         combinations.append((child,))
#         combinations.append((token, child))
#         combinations.append((child, token))
#
#         combinations = get_2_tokens_st_combinations(child, combinations)
#     return combinations




# def join_similar_combination(comb_to_closeness: Dict[Tuple[str], float]) -> Set[Tuple]:
#     """
#     TODO deprecated not used
#     @param comb_to_closeness:
#     @return:
#     """
#     # Выбираем группы "сходных" между собой сегментов
#     result_combs = set()
#     for comb in comb_to_closeness.keys():
#         similar_cobms = set()
#         for combTemp in comb_to_closeness.keys():
#             if comb == combTemp:
#                 continue
#
#             if sum([1 for t in comb if t in combTemp]) > 0:
#                 similar_cobms.add(comb)
#                 similar_cobms.add(combTemp)
#
#         # ... из них оставляем сегмент с максимальной близостью
#         if len(similar_cobms) > 0:
#             closest_com = similar_cobms.pop()
#             for combTemp in similar_cobms:
#                 if comb_to_closeness[combTemp] > comb_to_closeness[closest_com]:
#                     closest_com = combTemp
#             result_combs.add(closest_com)
#
#     return result_combs




# def syntax_tree_to_graph(token: Token, ancestor=None, tree_graph={}):
#     """
#     {token : [ancestor, child1, child2]}
#     @param token:
#     @param ancestor:
#     @param tree_graph:
#     @return:
#     """
#     if ancestor is not None:
#         tree_graph[token] = [ancestor, ]
#
#     if token in tree_graph:
#         tree_graph[token].extend([t for t in token.children])
#     else:
#         tree_graph[token] = [t for t in token.children]
#
#     for child in token.children:
#         tree_graph = syntax_tree_to_graph(child, token, tree_graph)
#     return tree_graph


# graph = {'A': ['B', 'C'],
#          'B': ['C', 'D'],
#          'C': ['D'],
#          'D': ['C'],
#          'E': ['F'],
#          'F': ['C']}

# def find_all_paths(graph, start, end, path=[]):
#     path = path + [start]
#     if start == end:
#         return [path]
#     if start not in graph:
#         return []
#     paths = []
#     for node in graph[start]:
#         if node not in path:
#             newpaths = find_all_paths(graph, node, end, path)
#             for newpath in newpaths:
#                 paths.append(newpath)
#     return paths
#






# def get_neighbours_comb_and_path_fragment_to_it(path: List[Token],
#                                                 target_comb: Tuple[Token],
#                                                 all_combs: List[Tuple[Token]],
#                                                 seek_for_left_neighbour=True) -> (Tuple[Token], List[Token]):
#     neighbour_comb = target_comb
#     is_found = False
#     for comb in all_combs:
#         if not is_comb_in_path(path, comb):
#             continue
#         if comb == target_comb:
#             continue
#
#         if seek_for_left_neighbour:
#             if get_token_index_in_path(path, right_token(comb)) < get_token_index_in_path(path, left_token(neighbour_comb)):
#             # if left_token(comb).i < left_token(neighbour_comb).i:
#
#                 neighbour_comb = comb
#                 is_found = True
#
#                 path_fragment = get_path_fragment_between_tokens(
#                     left_token(target_comb),
#                     right_token(neighbour_comb), path)
#
#         else:  # seek_for_right_neighbour
#             if get_token_index_in_path(path, right_token(comb)) < get_token_index_in_path(path, left_token(neighbour_comb)):
#             # if right_token(comb).i > right_token(neighbour_comb).i:
#                 neighbour_comb = comb
#                 is_found = True
#
#                 path_fragment = get_path_fragment_between_tokens(
#                     right_token(target_comb),
#                     left_token(neighbour_comb), path)
#
#     return neighbour_comb, path_fragment if is_found else None


