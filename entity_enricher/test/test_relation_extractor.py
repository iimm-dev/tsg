import unittest

import spacy


import entity_enricher.st_graph as stgraph
from entity_enricher.relation_heuristics import join_relations_by_verbs
from entity_enricher.relation_heuristics import try_to_assign_isKindOf_type_to_relations
from entity_enricher.helpers import get_combinations_with_good_closeness
from entity_enricher.helpers import print_tree
from entity_enricher.st_graph import _syntax_tree_to_graph
from entity_enricher.st_graph import get_relations_from_paths_and_token_combinations
from entity_enricher.test.test_enricher_deprecated import get_all_relations, get_relations_recursively
from semantic_field import model_creator
import entity_enricher.relation_extractor as rext
from domain_words import domain_words
from utils.file_utils import _rows_to_csv_file


class EntityEnricherTestCase(unittest.TestCase):
    def setUp(self):
        self.init()

        self.domain_words = domain_words()
        self.similarity_model_threshold = 0.08
        return

    def tearDown(self):
        return

    def init(self):
        # init spacy model ...
        self.nlp = spacy.load("ru_core_news_sm")

        # cur_model_creator = model_creator.GensimModelCreator()
        cur_model_creator = model_creator.NgramGensimModelCreator()
        # TODO enadle
        self.model = cur_model_creator.load_model("/home/user/Downloads/lenta_set/lenta_model2/bigram_model_full.bin")

    def test_relation_extractor_get_relations(self):

        from utils.file_utils import _rows_from_csv_files

        path_to_dir_with_files_of_sentences_with_entities = "/home/user/Downloads/" \
                                                            "lenta_set/lenta_trainset/experimental_sections" \
                                                            "/result_by_model_no_aug"
        # path_to_dir_with_files_of_sentences_with_entities = "/home/user/PycharmProjects/tsg/entity_enricher/test/data/bug_ent/"

        # === load sentences
        sentences = []
        max_sentence_quantity = 50
        max_sentence_quantity = -1
        sentence_quantity = 0
        for row in _rows_from_csv_files(path_to_dir_with_files_of_sentences_with_entities):
            sentences.append(row[0])

            sentence_quantity += 1
            if sentence_quantity >= max_sentence_quantity and max_sentence_quantity > 0:
                break

        print(f"=== load sentences finished - {sentence_quantity}")

        # === extraction of relations
        relations = rext.get_relations(sentences,
                                       self.domain_words,
                                       self.nlp,
                                       self.model,
                                       self.similarity_model_threshold)

        # === write result
        out_filepath = "/home/user/Downloads/lenta_set/lenta_trainset/experimental_sections" \
                       "/relation_extracted_00.csv "
        # out_filepath = "/home/user/PycharmProjects/tsg/tmp/relation_extracted_00.csv"

        sorted(relations, key=lambda rl: rl.__hash__())
        for r in relations:
            # print(r.__hash__(),r)
            print(r)

        _rows_to_csv_file([r.as_csv_row() for r in relations], out_filepath)


    def test_relation_extractor_path_based_extration(self):
        doc = self.nlp(
            """10 ноября одна из компаний, входящих в «Онэксим», направила основному 
            владельцу ГК «Связной» Максиму Ноготкову уведомление о дефолте по долгу, 
            залогом по которому выступает контрольный пакет акций группы.""")

        parsed_sentence = [sent for sent in doc.sents][0]
        print_tree(parsed_sentence.root)

        st_token_combs = stgraph.get_2_place_tokens_st_combinations(parsed_sentence.root, [(parsed_sentence.root,)])

        # Выбираем сегменты с близостью > порога concept_combos = []



        comb_to_closeness = get_combinations_with_good_closeness(st_token_combs, parsed_sentence,
                                                                 self.domain_words,
                                                                 self.similarity_model_threshold,
                                                                 self.model)
        closest_combs = set(comb_to_closeness.keys())
        print("=== result st combs")
        for comb in closest_combs:
            print(f"{comb}:={comb_to_closeness[comb]}")

        paths_between_list_tokens = stgraph.get_paths_between_list_tokens_of_dependence_tree(parsed_sentence)
        relations = get_relations_from_paths_and_token_combinations(closest_combs, paths_between_list_tokens)

        print('=== for r in relations: ==')
        for r in relations:
            print(r)

    def test_tree_to_graph(self):
        sentence = """Как сообщает The Guardian, во время демонстрации произошли столкновения участников акции с полицией."""
        doc = self.nlp(sentence)
        parsed_sentence = [sent for sent in doc.sents][0]
        print_tree(parsed_sentence.root)

        res_graph = _syntax_tree_to_graph(parsed_sentence.root)
        for node, neighbourns in res_graph.items():
            print(node.text, " : ", ", ".join([str.strip(t.text) for t in neighbourns]))
            # for n in neighbourns:
            #     print(n.text + ",")

    def test_get_graph_paths(self):
        sentence = """Как сообщает The Guardian, во время демонстрации произошли 
        столкновения участников акции с полицией."""
        doc = self.nlp(sentence)
        parsed_sentence = [sent for sent in doc.sents][0]
        print_tree(parsed_sentence.root)

        res_graph = _syntax_tree_to_graph(parsed_sentence.root)
        paths = stgraph._find_all_paths(res_graph, parsed_sentence[0], parsed_sentence[12])

        for path in paths:
            print(path)



    def test_hierarchy_hevristic(self):

        sentence = """Как сообщает агентство Reuters, падение акций Nokia повлияло также на 
        стоимость ценных бумаг других компаний, задействованных в сфере высоких технологий."""

        # TODO ent - это должен быть головной токен ( parsed_sent.root ), а не сущность, тк мы анализируем все предложение
        # ent = ["повлияло"]

        doc = self.nlp(sentence)
        parsed_sent = [sent for sent in doc.sents][0]
        entity = parsed_sent.root
        print_tree(parsed_sent.root)

        # cur_model_creator = model_creator.GensimModelCreator()
        # model = cur_model_creator.load_model("/home/user/Downloads/lenta_set/lenta_model/data/lenta_model.bin")
        model = self.model

        # entity_tokens = enrhp._get_spacy_tokens_by_text(ent, parsed_sent)
        # entity = entity_tokens[0]

        relationDTOs = get_all_relations(parsed_sent, entity, self.domain_words, self.similarity_model_threshold,
                                         model)

        for r in relationDTOs:
            print(r)

        relationDTOs = join_relations_by_verbs(relationDTOs)
        print("=== after join_relations_by_verbs(relationDTOs) ===")
        for r in relationDTOs:
            print(r)
            # print(f"A:{ get_upper_token(r.conceptA).text} {get_upper_token(r.conceptA).dep_} {get_upper_token(r.conceptA).pos_}")
            # print(f"B:{get_upper_token(r.conceptB).text} {get_upper_token(r.conceptB).dep_} {get_upper_token(r.conceptB).pos_}")

        r = try_to_assign_isKindOf_type_to_relations(relationDTOs)
        print("=== after try_to_assign_IS_KINS_OF_type_to_relations(relationDTOs) ===")
        for r in relationDTOs:
            print(r)


    # def test_single_get_relations_in_sentence(self):
    #
    #     # sentence="""Со второй половины 2014 года в США набирает силу волна протестов, вызванная жестокими действиями
    #     # стражей порядка и убийствами безоружных людей из бедных районов сотрудниками полиции."""
    #     # ent=["полиции"]
    #     # ent=["действиями"]
    #     # ent=["набирает"]
    #
    #     sentence = """Как сообщает The Guardian, во время демонстрации произошли столкновения участников акции с полицией."""
    #     ent = ["полицией"]
    #
    #     sentence = """Практически готово соглашение о передаче в траст Rothschild моего пакета акций в Roshen», — сказал глава государства, отметив, что делает это во избежание разного рода спекуляций"""
    #     ent = ["акций"]
    #
    #     sentence = '''10 ноября одна из компаний, входящих в «Онэксим», направила основному владельцу ГК «Связной» Максиму Ноготкову уведомление о дефолте по долгу, залогом по которому выступает контрольный пакет акций группы.'''
    #     ent = ["акций"]
    #
    #     sentence = '''Адвокатская контора планирует также привлечь к ответственности главу финской компании Йорму Оллилу (Jorma Ollila) и ее финансового директора Ричарда Симонсона (Richard Simonson).'''
    #     ent = ["главу"]
    #
    #     sentence = '''Евросоюз является крупнейшим торговым партнером России, и достижение договоренности с ним значительно приблизит момент принятия Москвы в ВТО.'''
    #     ent = ["России"]
    #     ent = ["России"]
    #     ent = ["является"]
    #
    #     sentence = '''Напомним, что в 2002 году финансовая разведка контролировала только операции кредитных организаций на сумму свыше 600 тысяч рублей.'''
    #     ent = ["Напомним"]
    #
    #     model = self.model
    #
    #     doc = self.nlp(sentence)
    #     parsed_sent = [sent for sent in doc.sents][0]
    #
    #     print_tree(parsed_sent.root)
    #
    #     _, relationDTOs = get_relations_recursively(parsed_sent, parsed_sent.root, self.domain_words,
    #                                                 self.similarity_model_threshold, model, [])
    #
    #     # relationDTOs = join_by_root_verb(relationDTOs, parsed_sent.root)
    #     for r in relationDTOs:
    #         print(r)
    #
    #     relationDTOs = join_relations_by_verbs(relationDTOs)
    #     print("=== after join_relations_by_verbs(relationDTOs) ===")
    #     for r in relationDTOs:
    #         print(r)




