from typing import List, Tuple

import spacy

from entity_enricher.st_graph import get_relations_from_paths_and_token_combinations
from entity_enricher.st_graph import get_2_place_tokens_st_combinations
from entity_enricher.st_graph import is_combination_good_source_for_relation_concept
from entity_enricher.st_graph import get_paths_between_list_tokens_of_dependence_tree
from entity_enricher.helpers import get_combinations_with_good_closeness

from entity_enricher.class_relation import RelationDTO
import semantic_field.model
from entity_enricher.relation_heuristics import join_relations_by_verbs
from entity_enricher.relation_heuristics import try_to_assign_isKindOf_type_to_relations
from entity_enricher.relation_heuristics import remove_relation_containing_same_concepts

from multiprocessing import Process


from utils.file_utils import _rows_to_csv_file, create_if_not_exists
from os import path

# TODO consider to make extractor as class

# --- init logger ---
from text_corpora.utils.clogger import get_logger

logger = get_logger("./", "common")


def get_relations_from_sentence(sentence: str,
                                domain_entities_as_ngrams: List[Tuple[str]],
                                nlp: spacy.Language,
                                closeness_model: semantic_field.model.Model,
                                closeness_threshold: float,
                                ) -> List[RelationDTO]:
    doc = nlp(sentence)
    # ... assume that doc contains only one sentence
    doc_sents = [sent for sent in doc.sents]
    if len(doc_sents) == 0:
        return []

    parsed_sentence = doc_sents[0]

    token_combs = get_2_place_tokens_st_combinations(parsed_sentence.root, [(parsed_sentence.root,)])
    token_combs = [c for c in token_combs if is_combination_good_source_for_relation_concept(c)]

    comb_to_closeness = get_combinations_with_good_closeness(token_combs, parsed_sentence,
                                                             domain_entities_as_ngrams,
                                                             closeness_threshold,
                                                             closeness_model)
    closest_combs = set(comb_to_closeness.keys())

    paths_between_list_tokens = get_paths_between_list_tokens_of_dependence_tree(parsed_sentence)
    relations = get_relations_from_paths_and_token_combinations(closest_combs, paths_between_list_tokens)
    return list(set(relations))


def get_relations(sentences: List[str],
                  domain_entities_as_ngrams: List[Tuple[str]],
                  nlp: spacy.Language,
                  closeness_model: semantic_field.model.Model,
                  closeness_threshold: float,
                  ) -> List[RelationDTO]:
    relations = []
    all_sentence_quantity = len(sentences)
    processed_sentence_quantity = 0
    logger.info(f"=== Start collecting relations from [{all_sentence_quantity}] sentences ... ===")

    for sent in sentences:
        if all_sentence_quantity % 10 == 0:
            logger.info(f"... [{processed_sentence_quantity} of {all_sentence_quantity}] sentences have been processed")
        processed_sentence_quantity += 1

        relations_of_sentence = get_relations_from_sentence(sent, domain_entities_as_ngrams, nlp, closeness_model,
                                                            closeness_threshold)
        # === Apply heuristics =====
        logger.info(f"... apply  heuristics to  [{len(relations_of_sentence)}] relation of sentence ...")

        relations_of_sentence = join_relations_by_verbs(relations_of_sentence)
        relations_of_sentence = try_to_assign_isKindOf_type_to_relations(relations_of_sentence)
        relations_of_sentence = remove_relation_containing_same_concepts(relations_of_sentence)

        # ============================
        logger.info(f"... end of application heuristics - [{len(relations_of_sentence)}] relation remains.")
        relations.extend(relations_of_sentence)

    return relations


def get_relations_file_saving_for_multithreading_execution(sentences: List[str],
                                                           domain_entities_as_ngrams: List[Tuple[str]],
                                                           nlp: spacy.Language,
                                                           closeness_model: semantic_field.model.Model,
                                                           closeness_threshold: float,
                                                           out_file_path: str):

    relations = get_relations(sentences, domain_entities_as_ngrams, nlp, closeness_model, closeness_threshold)
    _rows_to_csv_file([r.as_csv_row() for r in relations], out_file_path)
    logger.info(f"========= Exiting process get_relations_file_saving_for_multithreading_execution - out file: [{out_file_path}]")


def multithreaded_get_relations(sentences: List[str],
                                sentence_batch_size: int,
                                domain_entities_as_ngrams: List[Tuple[str]],
                                nlp: spacy.Language,
                                closeness_model: semantic_field.model.Model,
                                closeness_threshold: float,
                                dir_for_output_files: str
                                ) -> List[RelationDTO]:
    create_if_not_exists([dir_for_output_files])

    # sentence_batch_size = 15000

    sentence_batches = []
    # ... split sentence list into batches with length = sentence_batch_size
    for sentence in range(0, len(sentences), sentence_batch_size):
        sentence_batches.append(sentences[sentence:sentence + sentence_batch_size])

    out_file_num = 0
    processes = []
    # ... create and start process for each batch of sentences
    for sentence_batch in sentence_batches:
        logger.info(f"========= Starting {out_file_num} get_relations_file_saving_for_multithreading_execution - len(sentence_batch)={len(sentence_batch)} ...")
        out_file_path = path.join(dir_for_output_files, f"extracted_relations_{out_file_num}.csv")
        out_file_num += 1
        process = Process(target=get_relations_file_saving_for_multithreading_execution,
                          args=(sentence_batch,
                                domain_entities_as_ngrams,
                                nlp,
                                closeness_model,
                                closeness_threshold,
                                out_file_path)
                          )
        processes.append(process)
        process.start()

    # ... waiting until each process is finished
    for process in processes:
        process.join()

