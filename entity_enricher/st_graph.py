from itertools import combinations
from typing import List, Tuple, Dict, Set

from spacy.tokens import Doc, Token

import augmentator.helpers as hp
from entity_enricher.class_relation import RelationDTO, RelationType
from entity_enricher.helpers import sort_tokens_by_order_in_sentence


def _syntax_tree_to_graph(token: Token, ancestor=None, tree_graph={}) -> Dict[Token, List[Token]]:
    """
    Convert spacy-syntax-dependence tree to dict {spacy.token : [ancestor, child1, child2]}
    @param token: root token of spacy.Doc i.e. root-token of sentence.
    @param ancestor: don't set this - it is for recursion
    @param tree_graph: don't set this - it is for recursion
    @return: tree_graph as dict = {spacy.token : [ancestor, child1, child2]}
    """
    if ancestor is not None:
        tree_graph[token] = [ancestor, ]

    if token in tree_graph:
        tree_graph[token].extend([t for t in token.children])
    else:
        tree_graph[token] = [t for t in token.children]

    for child in token.children:
        tree_graph = _syntax_tree_to_graph(child, token, tree_graph)
    return tree_graph


def _get_list_tokens(sentence: Doc, filter_token_pos_list=["PUNCT", "SPACE"]) -> List[Token]:
    '''
    From given sentence tokens chooses only those which have no
    children in sentence syntax-tree.
    @param sentence: sentence parsed by spacy
    @param filter_token_pos_list: tokens with part of spech from that
    list will not be considered. Default: ["PUNCT","SPACE"]
    @return:
    '''
    all_tokens = [t for t in sentence]
    list_tokens = []
    for token in [t for t in all_tokens if t.pos_ not in filter_token_pos_list]:
        filtered_children = [t for t in token.children if t.pos_ not in filter_token_pos_list]
        if len(filtered_children) == 0:
            list_tokens.append(token)
    return list_tokens


def _find_all_paths(graph, start, end, path=[]):
    '''
    For given graph returns all paths between start and end nodes.
    @param graph: {node : [neighbour_node_1 ... neighbour_node_n]}
    @param start: something considered as a node
    @param end: something considered as a node
    @param path: [something considered as a node]
    @return: ordered list of nodes
    '''
    path = path + [start]
    if start == end:
        return [path]
    if start not in graph:
        return []
    paths = []
    for node in graph[start]:
        if node not in path:
            newpaths = _find_all_paths(graph, node, end, path)
            for newpath in newpaths:
                paths.append(newpath)
    return paths


def get_paths_between_list_tokens_of_dependence_tree(sentence: Doc) -> List[List[Token]]:
    """
    Convert spacy.Doc sentence dependence tree in graph and returns all path
    between tokens with no children.
    @param sentence: spacy.Doc sentence
    @return:
    """
    # === convert tree to graph-dict
    res_graph = _syntax_tree_to_graph(sentence.root)
    list_tokens = _get_list_tokens(sentence)

    # === get all 2-place-combinations of list tokens
    list_token_combs = [i for i in combinations(list_tokens, 2)]

    # === get all path between list-tokens from 2-place-combinations of them
    all_paths_between_list_tokens = []
    for tk1, tk2 in list_token_combs:
        all_paths_between_list_tokens.extend(_find_all_paths(res_graph, tk1, tk2))

    return all_paths_between_list_tokens


def get_2_place_tokens_st_combinations(token: Token,
                                       combs=[]) -> (
        List[Tuple[Token]]):
    """
    Recursively construct 2-place-combinations of token and its children
    from spacy-dependence-tree.
    # TODO consider improve to generate N-place-token combinations
    @param token: initial token (usually root token of sentence)
    @param combs: if you want to have combination with single initial token
    e.g. (initital_token,) then set combs = [(initital_token,)] else -
    don't set this arg
    @return:
    """
    for child in token.children:
        if not hp.is_suitable_token(child.text):
            continue
        combs.append((child,))
        combs.append(tuple(sort_tokens_by_order_in_sentence([token, child])))
        combs = get_2_place_tokens_st_combinations(child, combs)

    return combs


def is_combination_good_source_for_relation_concept(comb: List[Token]) -> bool:

    # see https://universaldependencies.org/u/pos/
    allowed_pos_tags = {
        "ADJ": True,
        "NOUN": True,
        "VERB": True,
    }

    for token in comb:
        try:
            if not allowed_pos_tags[token.pos_]:
                return False
        # if token has unknown POS then refuse whole combination
        except KeyError as e:
            return False
    return True

def _get_paths_containing_comb(paths: List[List[Token]], comb: Tuple[Token]) -> List[List[Token]]:
    result_paths = []
    for path in paths:
        if _is_comb_in_path(path, comb):
            result_paths.append(path)
    return result_paths


def _is_comb_in_path(path: List[Token], comb: Tuple[Token]) -> bool:
    comb_token_indices = [t.i for t in comb]
    match_quantity = sum([1 for t in path if t.i in comb_token_indices])
    if match_quantity == len(comb):
        return True
    return False


def get_all_neighbour_combs_from_path(path: List[Token],
                                      target_comb: Tuple[Token],
                                      all_combs: List[Tuple[Token]]) -> Set[Tuple[Token]]:
    """
    Get all neighbour of target combs. from path.
    @param path:
    @param target_comb:
    @param all_combs:
    @return: all_nested_neighbours as [(tokens),(tokens)] ,
    right_neighbours_and_path_fragments [( (neighbour),[path = list of tokens] ) ],
    left_neighbours_and_path_fragments [( (neighbour),[path = list of tokens] ) ]
    """

    nested_neighbours = set()
    left_nested_neighbours = set()
    right_nested_neighbours = set()
    left_neighbours_and_path_fragments = []
    right_neighbours_and_path_fragments = []

    if not _is_comb_in_path(path, target_comb):
        return set(), [], []

    target_comb_token_indices = set([t.i for t in target_comb])

    # === Consider each comb as neighbours of target one ...
    for comb in all_combs:
        if not _is_comb_in_path(path, comb) or comb == target_comb:
            continue

        comb_token_indices = set([t.i for t in comb])
        # ... consider comb as nested neighbour
        if comb_token_indices.issubset(target_comb_token_indices):
            nested_neighbours.add(comb)
            continue

        different_tokens = comb_token_indices.difference(target_comb_token_indices)
        same_tokens = comb_token_indices.intersection(target_comb_token_indices)

        # ... consider comb as nested left/right neighbour
        if len(same_tokens) > 0 and len(different_tokens) > 0:
            if _left_token(comb).i < _left_token(target_comb).i:
                left_nested_neighbours.add(comb)
            if _right_token(comb).i > _right_token(target_comb).i:
                right_nested_neighbours.add(comb)
            continue

    # === Consider each comb as left/right not nested neighbour ...
    neighbour, path_fragment = _get_closest_neighbour_comb_and_path_fragment(target_comb, all_combs, path, True)
    if neighbour is not None:
        left_neighbours_and_path_fragments.append((neighbour, path_fragment))

    neighbour, path_fragment = _get_closest_neighbour_comb_and_path_fragment(target_comb, all_combs, path, False)
    if neighbour is not None:
        right_neighbours_and_path_fragments.append((neighbour, path_fragment))

    # if len(left_nested_neighbours) == 0:
    #     neighbour, path_fragment = _get_closest_neighbour_comb_and_path_fragment(target_comb, all_combs, path, True)
    #     if neighbour is not None:
    #         left_neighbours_and_path_fragments.append((neighbour, path_fragment))
    #
    # if len(right_nested_neighbours) == 0:
    #     neighbour, path_fragment = _get_closest_neighbour_comb_and_path_fragment(target_comb, all_combs, path, False)
    #     if neighbour is not None:
    #         right_neighbours_and_path_fragments.append((neighbour, path_fragment))

    all_nested_neighbours = set()
    all_nested_neighbours = all_nested_neighbours.union(nested_neighbours,
                                                        left_nested_neighbours,
                                                        right_nested_neighbours)

    return all_nested_neighbours, right_neighbours_and_path_fragments, left_neighbours_and_path_fragments


def _get_token_index_in_path(path: List[Token], token: Token) -> int:
    index = 0
    for t in path:
        if token.i == t.i:
            return index
        index += 1
    return -1


# def get_token_index_to_st_comb(combs: List[Tuple[Token]]) -> Dict[Token, Tuple[Token]]:
#     token_to_combo = {}
#     for comb in combs:
#         for token in comb:
#             token_to_combo[token.i] = comb
#     return token_to_combo


def _left_token(comb: Tuple[Token]) -> Token:
    # TODO consider to use spacy token.func
    l_token = comb[0]
    for t in comb:
        if t.i < l_token.i:
            l_token = t
    return l_token


def _right_token(comb: Tuple[Token]) -> Token:
    # TODO consider to use spacy token.func
    r_token = comb[0]
    for t in comb:
        if t.i > r_token.i:
            r_token = t
    return r_token


def _create_relation(comb_A: Tuple[Token], comb_B: Tuple[Token], path_fragment: List[Token]) -> RelationDTO:
    relation = RelationDTO()
    relation.type = RelationType.unknown
    relation.conceptA = sort_tokens_by_order_in_sentence(comb_A)
    relation.conceptB = sort_tokens_by_order_in_sentence(comb_B)
    # context = "Context:" ", ".join([t.text for t in path_fragment]) if path_fragment is not None else ""
    context = "Context:" ", ".join([t.text for t in path_fragment])
    relation.info = context if len(relation.info) == 0 else f"{relation.info}, {context}"
    return relation


def _get_closest_neighbour_comb_and_path_fragment(target_comb: Tuple[Token], all_combs: List[Tuple[Token]],
                                                  path: List[Token], seek_for_left_neighbour=True) -> (
        Tuple[Token], List[Token]):
    # TODO can be >1 of closest neighbour - e.g target_comb = (1, 2)
    #  and  2 closest right neighbours  (3, 4), (3)
    """
    Try to find closest neighbour of target_comb among all comb and given path.
    @param target_comb:
    @param all_combs:
    @param path:
    @param seek_for_left_neighbour:
    @return: res_comb, path_fragment
    """
    min_distance = None
    res_comb = None

    if not _is_comb_in_path(path, target_comb):
        return None, None

    for comb in all_combs:
        if not _is_comb_in_path(path, comb) or comb == target_comb:
            continue

        if seek_for_left_neighbour:
            target_comb_path_index = _get_token_index_in_path(path, _left_token(target_comb))
            comb_path_index = _get_token_index_in_path(path, _right_token(comb))
            distance = target_comb_path_index - comb_path_index  # 2:comb_path_index ...  4:target_comb_path_index
            if (distance > 0 and min_distance is None) or (distance > 0 and distance < min_distance):
                min_distance = distance
                res_comb = comb
                res_target_comb_token = _left_token(target_comb)
                res_comb_path_token = _right_token(comb)

        else:  # seek_for_right_neighbour
            target_comb_path_index = _get_token_index_in_path(path, _right_token(target_comb))
            comb_path_index = _get_token_index_in_path(path, _left_token(comb))
            distance = comb_path_index - target_comb_path_index  # 2:target_comb_path_index ...  4:comb_path_index
            if (distance > 0 and min_distance is None) or (distance > 0 and distance < min_distance):
                min_distance = distance
                res_comb = comb
                res_target_comb_token = _right_token(target_comb)
                res_comb_path_token = _left_token(comb)

    if res_comb is not None:
        path_fragment = _get_path_fragment_between_tokens(res_target_comb_token, res_comb_path_token, path)
        return res_comb, path_fragment
    else:
        return None, None


def get_relations_from_paths_and_token_combinations(combinations: List[Tuple[Token]],
                                                    paths: List[List[Token]]) -> Set[RelationDTO]:
    """
    Returns relations created from dependence-tree-paths and token-combinations.
    Relations are constructed from each combination and nested and not-nested
    left/right neighbour combinations.

    Note: left/right is considered in regard to place in path (i.e. dependence tree) and
    not in sentence token number.
    @param combinations: combinations of tokens from single sentence - [(token,..), (token,..)]
    @param paths: paths between list tokens from dependence-tree of the sentence.
    @return: [RelationDTO]
    """
    relations = set()

    # === For each combination create relation with all its neighbours from paths
    for comb in combinations:
        neighbour_combs = []
        paths_with_combinations = _get_paths_containing_comb(paths, comb)

        # Try to get neighbours of comb from all-combinations and paths ...
        for path in paths_with_combinations:
            nested_neighbours, \
            right_neighbours_and_path_fragments, \
            left_neighbours_and_path_fragments = get_all_neighbour_combs_from_path(path, comb, combinations)

            # ... create relations from comb and its neighbours ...
            for neighbour_comb in nested_neighbours:
                relations.add(_create_relation(comb, neighbour_comb, []))

            # ... for not-nested neighbours add path_fragment to relation
            for neighbour_comb, path_fragment in right_neighbours_and_path_fragments:
                relations.add(_create_relation(comb, neighbour_comb, path_fragment))

            for neighbour_comb, path_fragment in left_neighbours_and_path_fragments:
                relations.add(_create_relation(comb, neighbour_comb, path_fragment))

    return relations


def _get_path_fragment_between_tokens(tokenA: Token, tokenB: Token, path: List[Token]) -> List[Token]:
    path_fragment = []
    meet_first_token = False
    # meet_second_token = False
    for token in path:
        if (token.i == tokenA.i or token.i == tokenB.i) and not meet_first_token:
            meet_first_token = True
            continue
        if (token.i == tokenA.i or token.i == tokenB.i) and meet_first_token:
            # path_segment.append(token)
            return path_fragment
        if meet_first_token:
            path_fragment.append(token)

    return []
