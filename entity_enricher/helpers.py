import string

from spacy.tokens import Doc, Token
from typing import List, Tuple, Dict
from semantic_field.model import Model
from numpy import mean

def _get_spacy_tokens_by_text(entity: List[str], parsed_text: Doc)-> List[str]:
    # TODO if entity contains more than 1 word
    # if parsed_text.text.contains(entity)
    # res=List[Token]
    for token in parsed_text:
        if token.text == entity[0]:
            return [token]


def is_domain_concept(ngram: string, sentence: string, domain_entities_as_ngrams, model: Model, threshold=None) -> (bool, int):
    """
    For given ngram (i.e. string containing of n-words) from sentence returns true if the ngram is close enough
    to domain words in context of given model.
    @param ngram:
    @param sentence:
    @param domain_entities_as_ngrams: List[Tuple[str]]
    @param threshold:
    @param model:
    @return: is domain concept, closeness measure
    """
    ngram_to_similarity = model.get_similarities(sentence, domain_entities_as_ngrams, make_lemmatization=False)

    threshold = mean(list(ngram_to_similarity.values())) if threshold is None else threshold

    if ngram in ngram_to_similarity:
        ngram_similarity = ngram_to_similarity[ngram]
    else:
        return False, 0

    if ngram_similarity is not None and ngram_similarity >= threshold:
        return True, ngram_similarity
    else:
        return False, ngram_similarity


def to_nltk_tree(node):
    from nltk import Tree
    if node.n_lefts + node.n_rights > 0:
        return Tree(node.orth_, [to_nltk_tree(child) for child in node.children])
    else:
        return node.orth_


def are_neighbour_tokens(token_A: Token, token_B: Token) -> bool:
    if len(token_A.sent) < 2:
        return False

    if len(token_A.sent)-1 == token_A.i: # if A - last token
        if token_A.nbor(i=-1) == token_B:
            return True
        return False

    if token_A.i==0: # if A - first token
        if token_A.nbor(i=1) == token_B:
            return True
        return False

    if len(token_A.sent)>1 and (token_A.nbor(i=1) == token_B or token_A.nbor(i=-1) == token_B):
        return True
    return False


def print_tree(spacy_doc):
    nltk_tree = to_nltk_tree(spacy_doc)
    nltk_tree.pretty_print()


def tokens_to_string(tokens: List[Token]) -> string:
    if tokens is None:
        return ""
    tokens = sort_tokens_by_order_in_sentence(tokens)
    str = ""
    if tokens is not None or len(tokens) > 0:
        str = " ".join([t.text for t in tokens])
    return str


def sort_tokens_by_order_in_sentence(tokens: List[Token]):
    '''
    Sort entity tokens by order in sentence
    @param tokens:
    @return: sorted list of tokens
    '''

    return sorted(tokens, key=lambda token: token.i)


def get_combinations_with_good_closeness(tokens_combinations: List[Tuple[Token]],
                                         sentence: Doc,
                                         domain_entities_as_ngrams: List[Tuple[str]],
                                         threshold: float, model: Model) -> Dict[Tuple[str], float]:

    """
    For each token combination defines closeness
    to domain words, returns only ones having closeness > threshold
    @param tokens_combinations: [(token,)]
    @param sentence: spacy.doc sentence
    @param domain_entities_as_ngrams: e.g. List[("national", "leader"), ...}
    @param threshold: goog_closeness threshold
    @param model: gensim n-gram model
    @return: {good_comb : closeness_float}
    """
    comb_to_closeness = {}
    for comb in tokens_combinations:
        ngram = (" ".join([t.text for t in comb])).strip()
        is_close_enough, closeness = is_domain_concept(ngram, sentence.text, domain_entities_as_ngrams, model,
                                                       threshold)
        if is_close_enough:
            comb_to_closeness[comb] = closeness

    return comb_to_closeness

