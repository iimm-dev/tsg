import logging
import unittest
import os

import spacy


from semantic_field import model_creator
import entity_enricher.relation_extractor as rext
from entity_enricher.test.domain_words import domain_words
from utils.file_utils import create_if_not_exists, recreate_dirs



from utils.file_utils import _rows_from_csv_files

# --- init logger ---
from text_corpora.utils.clogger import get_logger

logger = get_logger("./", "common")
logger.setLevel(logging.INFO)


path_to_dir_with_files_of_sentences_with_entities = "/home/user/PycharmProjects/tsg/entity_enricher/test/data/bug_ent/"

# === init closeness model ...
# NOTE: closeness model is generated by semantic_field/model_creator.py
cur_model_creator = model_creator.NgramGensimModelCreator()
# model = cur_model_creator.load_model("/model_file_path/bigram_model_full.bin")
model = cur_model_creator.load_model("/home/user/Downloads/lenta_set/lenta_model2/bigram_model_full.bin")
closeness_model_threshold = None
domain_words = domain_words()

# === init spacy model ...
# NOTE: spacy model should be installed and loaded here
nlp = spacy.load("ru_core_news_sm")

# === load sentences
sentences = set()
# all sentences
max_sentence_quantity = -1

for row in _rows_from_csv_files(path_to_dir_with_files_of_sentences_with_entities):
    sentences.add(row[0])

    if len(sentences) >= max_sentence_quantity > 0:
        break

print(f"=== load sentences finished - {len(sentences)}")
sentences=list(sentences)

# === extraction and saving of relations
dir_for_output_files = "./test_output_dir"
recreate_dirs([dir_for_output_files])

sentence_batch_size = 3000
relations = rext.multithreaded_get_relations(sentences,
                                             sentence_batch_size,
                                             domain_words,
                                             nlp,
                                             model,
                                             closeness_model_threshold,
                                             dir_for_output_files)

