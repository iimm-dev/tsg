from spacy.tokens import Doc, Token
import augmentator.helpers as hp


# https://universaldependencies.org/u/pos/

# TODO deprecated not used now

def _get_token_with_no_children(sentense: Doc):
    childless_tokens = []
    for token in sentense:
        if len([child for child in token.children]) == 0 and _is_suitable_token(token):
            childless_tokens.append(token)
    return childless_tokens


def _is_suitable_token(token: Token) -> bool:
    if token.pos_ not in ["PUNCT", "CCONJ", "ADP", "DET", "SCONJ", "ADV", "NUM", "AUX", "PART", "X"] \
            and not hp.is_adjective(token) \
            and not hp.is_single_char(token.text):
        return True
    else:
        return False


def _get_verb_token(sentense: Doc):
    tokens = []
    for token in sentense:
        if token.pos_ == "VERB":
            tokens.append(token)
    return tokens


def _get_left_and_right_noun_children(verb_token: Token):
    left_children = []
    right_children = []
    for child in verb_token.children:
        if _is_suitable_token(child) and child.pos_ != "VERB":
            if _is_left_neighbour(verb_token, child):
                left_children.append(child)
            else:
                right_children.append(child)
    return left_children, right_children


def _is_left_neighbour(token: Token, token_for_check: Token) -> bool:
    if token_for_check.idx > token.idx:
        return True
    return False


