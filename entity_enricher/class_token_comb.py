import string
import timeit
from typing import List, Tuple, Dict

from spacy.tokens import Doc, Token

from entity_enricher.helpers import tokens_to_string
from entity_enricher.helpers import sort_tokens_by_order_in_sentence

import enum
import augmentator.helpers as hp

# TODO Deprecated not used now - consider as substitution of combination as tuple[spacy.Token]

class TokenSTCombination:
    def __init__(self, tokens: Tuple):
        self.tokens = tokens
        # self.conceptB = []  # List[Token]
        # self.type = RelationType.unknown
        # self.info = ""

    def __str__(self):
        str = ",".join([t.text for t in self.tokens])
        return f"({str})"

    def __hash__(self):
        return hash(self.tokens)

    def __eq__(self, other):
        return isinstance(other, TokenSTCombination) and self.__hash__() == other.__hash__()



def get_token_st_combinations(token: Token, current_combinations: List[TokenSTCombination])-> ( List[TokenSTCombination]):

    for child in token.children:
        if not hp.is_suitable_token(child.text):
            continue
        # TODO deperacted
        current_combinations.append(TokenSTCombination(()))
        current_combinations.append((token, child))
        current_combinations.append((child, token))

    return get_token_st_combinations(child, current_combinations)
