import string
from typing import List, Tuple

from spacy.tokens import Doc, Token

from entity_enricher.helpers import tokens_to_string
from entity_enricher.helpers import sort_tokens_by_order_in_sentence
from entity_enricher.class_relation import RelationDTO
from entity_enricher.class_relation import is_relation_with_verb
from entity_enricher.class_relation import is_contains_verb
from entity_enricher.class_relation import RelationType
from entity_enricher.class_relation import get_upper_token



'''
===========================================
=== post processing relation heuristics ===
===========================================
'''

def join_relations_by_verbs(relations: List[RelationDTO]) -> List[RelationDTO]:
    '''
    Join relations by same concept containing verb.
    For example:
    (2002 году)--[SOME_REL]--(контролировала)
    (разведка)--[SOME_REL]--(контролировала)
    ... returns
    (2002 году)--[REL контролировала]--(разведка)

    @param relations:
    @return:
    '''
    relations = set(relations)
    relations_with_verb = [r for r in relations if is_relation_with_verb(r)[0]]

    relations_with_verb = set(relations_with_verb)
    replaced_relations = set()
    joined_relations = set()
    # === Consider relation1 for joining ...
    for relation1 in relations_with_verb:
        _, verb_token, verb_concept = is_relation_with_verb(relation1)
        rel1_concept_for_replacement = verb_concept

        # ... and get relation2 for joining ...
        for relation2 in relations_with_verb:
            if relation2 == relation1:
                continue
            # ... skip already replaced PAIRS of relations
            if relation2 in replaced_relations and relation1 in replaced_relations:
                continue
            _, verb_token, verb_concept = is_relation_with_verb(relation2)
            rel2_concept_for_replacement = verb_concept

            # === Create joined_relation ...
            joined_relation = RelationDTO()
            # TODO it seems that rel1_concept_for_replacement and rel2_concept_for_replacement should be equal
            if relation1.conceptA == rel1_concept_for_replacement:
                joined_relation.conceptA = relation1.conceptB
                joined_relation.type = RelationType.unknown
                joined_relation.info = " ".join([t.text for t in rel1_concept_for_replacement])
            else:
                joined_relation.conceptA = relation1.conceptA
                joined_relation.type = RelationType.unknown
                # joined_relation.type = "REL " + " ".join([t.text for t in rel1_concept_for_replacement])
                joined_relation.info = " ".join([t.text for t in rel1_concept_for_replacement])

            if relation2.conceptB == rel2_concept_for_replacement:
                joined_relation.conceptB = relation2.conceptA
                joined_relation.type = RelationType.unknown
                # joined_relation.type = "REL " + " ".join([t.text for t in rel2_concept_for_replacement])
                joined_relation.info = " ".join([t.text for t in rel1_concept_for_replacement])
            else:
                joined_relation.conceptB = relation2.conceptB
                joined_relation.type = RelationType.unknown
                # joined_relation.type = "REL " + " ".join([t.text for t in rel2_concept_for_replacement])
                joined_relation.info = " ".join([t.text for t in rel1_concept_for_replacement])

            replaced_relations.add(relation1)
            replaced_relations.add(relation2)

            # ... if some of the concept is VERB than not create relationship
            if is_contains_verb(joined_relation.conceptA) or is_contains_verb(joined_relation.conceptB):
                continue

            joined_relations.add(joined_relation)

    relations = relations - replaced_relations
    # relations_with_verb = relations_with_verb.difference(replaced_relations)
    relations = relations.union(joined_relations)
    return [r for r in relations]

def remove_relation_containing_same_concepts(relations: List[RelationDTO]) -> List[RelationDTO]:
    res_relations = []

    for relation in relations:

        conceptA_hash = tokens_to_string(relation.conceptA).__hash__()
        conceptB_hash = tokens_to_string(relation.conceptB).__hash__()

        if conceptA_hash != conceptB_hash:
            res_relations.append(relation)



    return res_relations



def try_to_assign_isKindOf_type_to_relations(relations: List[RelationDTO]) -> List[RelationDTO]:

    for relation in relations:
        tokenA = get_upper_token(relation.conceptA)
        tokenB = get_upper_token(relation.conceptB)
        # relation.conceptA -[parent-of]-> relation.conceptB


        # case: (оперативная группа) - -[UNK] - -(группа)
        if tokens_to_string([tokenA])== tokens_to_string([tokenB]):
            relation.type = RelationType.kind_of
            parent_concept = relation.conceptA if len(relation.conceptA) < len(relation.conceptB) else relation.conceptB
            parent_concept_text = " ".join([t.text for t in sort_tokens_by_order_in_sentence(parent_concept)])

            child_concept = relation.conceptA if parent_concept == relation.conceptB else relation.conceptB
            child_concept_text = " ".join([t.text for t in sort_tokens_by_order_in_sentence(child_concept)])

            relation.info = f"Parent: {parent_concept_text} Child:{child_concept_text}"

        # case: (группа) - -[UNK] - -(оперативная)
        # relation.conceptA -[parent-of]-> relation.conceptB
        if tokenA.pos_ == "NOUN" and tokenA.dep_ in ["nmod", "'nsubj'"]:
            if tokenB.pos_ == "ADJ" and tokenB.dep_ == "amod":
                relation.type = RelationType.kind_of
                child_concept_text = " ".join([t.text for t in sort_tokens_by_order_in_sentence(relation.conceptB)])
                relation.info = f"Parent: {tokenA.text} Child:{child_concept_text}"

        # case: (оперативная) - -[UNK] - -(группа)
        # relation.conceptB -[parent-of]-> relation.conceptA
        if tokenA.pos_ == "ADJ" and tokenA.dep_ == "amod":
            if tokenB.pos_ == "NOUN" and tokenB.dep_ in ["nmod", "'nsubj'"]:
                relation.type = RelationType.kind_of
                child_concept_text = " ".join([t.text for t in sort_tokens_by_order_in_sentence(relation.conceptA)])
                relation.info = f"Parent: {tokenB.text} Child:{child_concept_text}"

    return relations
