export PYTHONPATH=${PYTHONPATH}:/home/user/PycharmProjects/ru-gpts/

CUDA_VISIBLE_DEVICES=0 python ../../ru-gpts/pretrain_transformers.py \
    --output_dir=/home/user/Downloads/rugpt2small_habr/ \
    --model_type=gpt2 \
    --model_name_or_path=/home/user/Downloads/rugpt2small_habr/checkpoint-184500 \
    --do_train \
    --train_data_file=/home/user/Downloads/habr_post/habr_gpt_train_data.txt \
    --do_eval \
    --eval_data_file=/home/user/Downloads/habr_post/habr_gpt_valid_data.txt \
    --per_gpu_train_batch_size 1 \
    --gradient_accumulation_steps 1 \
    --num_train_epochs 1 \
    --block_size 512 \
    --overwrite_output_dir \
    --save_total_limit 3