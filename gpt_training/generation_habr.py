from _ast import mod

from transformers import GPT2LMHeadModel, GPT2Tokenizer
import numpy as np
import torch

# model_rep_path = "/home/user/Downloads/rugpt2small_habr/"
model_rep_path = "/home/user/Downloads/rugpt2medium_habr"
# model_rep_path = "/home/user/Downloads/rugpt2small_habr/checkpoint-184500"

model_name_or_path = model_rep_path
tokenizer = GPT2Tokenizer.from_pretrained(model_name_or_path)
model = GPT2LMHeadModel.from_pretrained(model_name_or_path).cuda()
# text = "Александр Сергеевич Пушкин родился в "
# text = """
# <s>sent: Apple нормально принимает Qt приложения, саму Qt нужно немного пропатчить.
# ent: Apple
# """
# [Предложение] Атаки на уровне приложений Как можно понять из названия, атаки направлены на уязвимости в приложениях и операционных системах Apache, Windows, OpenBSD и т.п. .
# [Сущность] Windows [Конец]
# text = """ [Предложение] Apple нормально принимает Qt приложения, саму Qt нужно немного пропатчить.
# [Сущность] ''' [Конец]
# """
text = "  #### нормально принимает Qt приложения, саму Qt нужно немного пропатчить. "
text = "####  семейство коммерческих операционных систем корпорации Microsoft, ориентированных на управление с помощью графического интерфейса. "
text = "В этом выпуске мы поговорим о том, как мы использовали Microsoft"
text = "я думал, что мы отдаем как есть и взаимодействие с нашим бэком идет только нашим фронтом и на внешку ничего не отдаем в части API"

input_ids = tokenizer.encode(text, return_tensors="pt").cuda()
# out = model.generate(input_ids.cuda())
out = model.generate(input_ids.cuda(), max_length=420, repetition_penalty=1.0, do_sample=True, top_k=5, top_p=0.95, temperature=1.)
generated_text = list(map(tokenizer.decode, out))[0]
print(generated_text)
print("----------------------------------------------")

input_ids = tokenizer.encode(text, return_tensors="pt").cuda()
# out = model.generate(input_ids.cuda())
out = model.generate(input_ids.cuda(), max_length=420, repetition_penalty=1.0, do_sample=True, top_k=4, top_p=0.95, temperature=1.)
generated_text = list(map(tokenizer.decode, out))[0]
print(generated_text)
print("----------------------------------------------")

input_ids = tokenizer.encode(text, return_tensors="pt").cuda()
# out = model.generate(input_ids.cuda())
out = model.generate(input_ids.cuda(), max_length=420, repetition_penalty=1.0, do_sample=True, top_k=3, top_p=0.95, temperature=1.)
generated_text = list(map(tokenizer.decode, out))[0]
print(generated_text)
print("----------------------------------------------")

input_ids = tokenizer.encode(text, return_tensors="pt").cuda()
# out = model.generate(input_ids.cuda())
out = model.generate(input_ids.cuda(), max_length=420, repetition_penalty=1.0, do_sample=True, top_k=2, top_p=0.95, temperature=1.)
generated_text = list(map(tokenizer.decode, out))[0]
print(generated_text)
print("----------------------------------------------")



# ===========================

# input_ids = tokenizer.encode(text, return_tensors="pt").cuda()
# # out = model.generate(input_ids.cuda())
# out = model.generate(input_ids.cuda(), max_length=220, repetition_penalty=5.0, do_sample=True, top_k=5, top_p=0.95, temperature=2.)
# generated_text = list(map(tokenizer.decode, out))[0]
# print(generated_text)
# === temp ===
# print("=== temp ===")
# input_ids = tokenizer.encode(text, return_tensors="pt").cuda()
# # out = model.generate(input_ids.cuda())
# out = model.generate(input_ids.cuda(), max_length=220, repetition_penalty=5.0, do_sample=True, top_k=5, top_p=0.95, temperature=2.)
# generated_text = list(map(tokenizer.decode, out))[0]
# print(generated_text)
# print("-------------------------")
#
# input_ids = tokenizer.encode(text, return_tensors="pt").cuda()
# # out = model.generate(input_ids.cuda())
# out = model.generate(input_ids.cuda(), max_length=220, repetition_penalty=5.0, do_sample=True, top_k=5, top_p=0.95, temperature=3.)
# generated_text = list(map(tokenizer.decode, out))[0]
# print(generated_text)
# print("-------------------------")
# input_ids = tokenizer.encode(text, return_tensors="pt").cuda()
# # out = model.generate(input_ids.cuda())
# out = model.generate(input_ids.cuda(), max_length=220, repetition_penalty=5.0, do_sample=True, top_k=5, top_p=0.95, temperature=4.)
# generated_text = list(map(tokenizer.decode, out))[0]
# print(generated_text)
# print("-------------------------")
# input_ids = tokenizer.encode(text, return_tensors="pt").cuda()
# # out = model.generate(input_ids.cuda())
# out = model.generate(input_ids.cuda(), max_length=220, repetition_penalty=5.0, do_sample=True, top_k=5, top_p=0.95, temperature=5.)
# generated_text = list(map(tokenizer.decode, out))[0]
# print(generated_text)
#
# print("=== penalty ===")
# input_ids = tokenizer.encode(text, return_tensors="pt").cuda()
# # out = model.generate(input_ids.cuda())
# out = model.generate(input_ids.cuda(), max_length=220, repetition_penalty=1.0, do_sample=True, top_k=5, top_p=0.95, temperature=1.)
# generated_text = list(map(tokenizer.decode, out))[0]
# print(generated_text)
# print("-------------------------")
#
# input_ids = tokenizer.encode(text, return_tensors="pt").cuda()
# # out = model.generate(input_ids.cuda())
# out = model.generate(input_ids.cuda(), max_length=220, repetition_penalty=2.0, do_sample=True, top_k=5, top_p=0.95, temperature=1.)
# generated_text = list(map(tokenizer.decode, out))[0]
# print(generated_text)
# print("-------------------------")
#
# input_ids = tokenizer.encode(text, return_tensors="pt").cuda()
# # out = model.generate(input_ids.cuda())
# out = model.generate(input_ids.cuda(), max_length=220, repetition_penalty=3.0, do_sample=True, top_k=5, top_p=0.95, temperature=1.)
# generated_text = list(map(tokenizer.decode, out))[0]
# print(generated_text)
# print("-------------------------")
#
# input_ids = tokenizer.encode(text, return_tensors="pt").cuda()
# # out = model.generate(input_ids.cuda())
# out = model.generate(input_ids.cuda(), max_length=220, repetition_penalty=4.0, do_sample=True, top_k=5, top_p=0.95, temperature=1.)
# generated_text = list(map(tokenizer.decode, out))[0]
# print(generated_text)
# print("-------------------------")

# AttributeError: module 'torch' has no attribute '__version__'

# from torch.optim.lr_scheduler import SAVE_STATE_WARNING

# np.random.seed(42)
# torch.manual_seed(42)
#
# model_rep_path = "/home/user/PycharmProjects/rugpt2large"
#
# tok = GPT2Tokenizer.from_pretrained(model_rep_path)
# model = GPT2LMHeadModel.from_pretrained(model_rep_path)
# model.cuda()
#
# text = "<s>Тема: «Создает человека природа, но развивает и образует его общество». (В.Т. Белинский)\nСочинение: "
# inpt = tok.encode(text, return_tensors="pt")
#
# out = model.generate(inpt.cuda(), max_length=500, repetition_penalty=5.0, do_sample=True, top_k=5, top_p=0.95, temperature=1)
#
# tok.decode(out[0])