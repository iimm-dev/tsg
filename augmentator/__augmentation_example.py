import logging
import os
import re

import augmentator.helpers as hp
import spacy

from augmentator.augmentation import generate_augmented_csv_files
from setgen.final_trainset_generation import join_csv_files_with_borders_into_spacy_trainset_files

# --- init logger ---
from text_corpora.utils.clogger import get_logger
logger = get_logger("./", "common")

# ===================
# === Init models ===
# ===================
model_path = "/home/user/Downloads/arctic/rubert_cased_L-12_H-768_A-12_pt/"
nlp = spacy.load("ru_core_news_sm")
tokenizer, model = hp.initTokeniserAndModel(model_path)

# =================
# === Init path ===
# =================
in_dir_of_csv_files_with_borders = "./test/data/test_border_csv"

augmented_file_dir = "./test/data/out/out_augmented_files"
output_dir_for_files_with_boundaries = "./test/data/out/tmp_output_dir_for_files_with_boundaries"
output_dir_for_entity_with_wrong_boundaries = "./test/data/out/tmp_dir_for_entity_with_wrong_boundaries"
output_dir_with_train_files = "./test/data/out/output_dir_with_train_files"


# ==================================================================================================
# === get augmented files from csv-files [sentence, entity, category, left_border, right_border] ===
# ==================================================================================================
augmented_file_dir = generate_augmented_csv_files(in_dir_of_csv_files_with_borders, augmented_file_dir,
                                                     nlp, tokenizer, model,
                                                     sentences_per_out_file=300000,
                                                     max_sentence_size_in_chars=400,
                                                     max_mask_sentence_variations_quantity=30)

# =========================================================
# === join augmented csv files into training json-files ===
# =========================================================
spacy_json_train_file_dir = join_csv_files_with_borders_into_spacy_trainset_files(augmented_file_dir,
                                                                                  output_dir_for_files_with_boundaries,
                                                                                  output_dir_for_entity_with_wrong_boundaries,
                                                                                  output_dir_with_train_files,
                                                                                  row_quantity_per_train_file=25)

files = [f for f in os.listdir(output_dir_with_train_files) if
         os.path.isfile(os.path.join(output_dir_with_train_files, f))]

logging.info(f"=== Got {len(files)} augmented files for training in {output_dir_with_train_files}")
# for n in files:
#     logging.info(n)


