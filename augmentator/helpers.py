import random
import re
import string
from typing import List, Tuple

import spacy.tokens
from transformers import AutoModelWithLMHead, AutoTokenizer, AutoModelForMaskedLM
import torch
from spacy.tokens import Doc, Token
from itertools import permutations, combinations, product


def initTokeniserAndModel(model_path: str) -> Tuple[AutoTokenizer, AutoModelForMaskedLM]:
    tokenizer = AutoTokenizer.from_pretrained(model_path)
    model = AutoModelForMaskedLM.from_pretrained(model_path)
    return tokenizer, model

# def initTokeniserAndModel(model_path: str) -> Tuple[AutoTokenizer, AutoModelForMaskedLM]:
#     from transformers import GPT2LMHeadModel, GPT2Tokenizer
#     model_path ="/home/user/Downloads/rugpt2medium_habr"
#     tokenizer = GPT2Tokenizer.from_pretrained(model_path)
#     model = GPT2LMHeadModel.from_pretrained(model_path)
#     return tokenizer, model


def _get_alternatives_for_masked_tokens(tokenizer: AutoTokenizer, model: AutoModelForMaskedLM,
                                        masked_sentence: str) -> List[List[str]]:
    """
    For each mask-token of sentence generate alternatives and put them into list.
    @param tokenizer:
    @param model:
    @param masked_sentence:
    @return: list of alternatives for each mask-token - i.e. [[alternatives for 1 token]... [alternatives for N token]]
    """
    each_token_alternatives = []
    input = tokenizer.encode(masked_sentence, return_tensors="pt")
    mask_token_index = torch.where(input == tokenizer.mask_token_id)[1]
    token_logits = model(input)[0]
    mask_token_logits = token_logits[0, mask_token_index, :]
    indices = torch.topk(mask_token_logits, 5, dim=1).indices

    for index in indices:
        top_5_tokens = index.tolist()
        alternatives = []
        for token in top_5_tokens:
            alternative_token = tokenizer.decode([token])
            # print(alternative)
            if is_suitable_token(alternative_token):
                # print(alternative_token)
                alternatives.append(alternative_token)

        each_token_alternatives.append(alternatives)

    return each_token_alternatives


def _get_entity_tokens(sentense: Doc, left_ent_border: int, right_ent_border: int) -> List[Token]:
    '''
    Collect ordered tokens of given entity presented by its left/right borders
    @param sentense: sentence as spacy.token.Doc
    @param left_ent_border:
    @param right_ent_border:
    @return: ordered entity tokens [spacy.tokens.Token]
    '''
    entity_tokens = []
    for token in sentense:
        token_left_border = token.idx
        if token_left_border >= left_ent_border and token_left_border < right_ent_border:
            entity_tokens.append(token)
    return entity_tokens


def _get_head_tokens(tokens: [spacy.tokens.Token]) -> List[Token]:
    '''
    Find head tokens in provided ones. head token != punct, stop_word, adjective.
    Note: Assume that head_token will be only one
    @param tokens:
    @return: head tokens
    '''
    heads = []
    for t in tokens:
        if (not t.is_punct) and (not t.is_stop) and (t.pos_ not in ["PUNCT", "CCONJ"]):
            if (t.head not in tokens) or (t.head == t):
                heads.append(t)

    return heads


def is_last(token: Token) -> bool:
    if len(token.doc) == token.i + 1:
        return True
    return False


def is_first(token: Token) -> bool:
    if token.i == 0:
        return True
    return False


def next(token: Token) -> Token:
    if not is_last(token):
        return token.doc[token.i + 1]
    else:
        raise Exception(f"[{token.text}] is last token")


def prev(token: Token) -> Token:
    if not is_first(token):
        return token.doc[token.i - 1]
    else:
        raise Exception(f"[{token.text}] is first token")


def _clarify_entity_tokens_by_nearest_childs_of_head_token(entity_tokens: [Token], head_entity_token: Token) -> List[Token]:
    entity_tokens = set(entity_tokens)
    new_entity_tokens = set()

    while entity_tokens != new_entity_tokens:
        new_entity_tokens = set(entity_tokens)

        # For each entity_token from CURRENT list of entity tokens ...
        for ent_token in entity_tokens:
            # ... and each child-token of head_token of entity ...
            for child in head_entity_token.children:
                # ... if child_token is before some entity_token...
                if not is_last(child) and next(child) == ent_token and child not in entity_tokens:
                    new_entity_tokens.add(child)
                    continue

                # ... or child_token is after some entity_token...
                if not is_first(child) and prev(child) == ent_token and child not in entity_tokens:
                    new_entity_tokens.add(child)
                    continue

                # ... if between child AND ent_token IS child_of_child - add both = child AND child_of_child ...
                for child_of_child in child.children:
                    if not is_last(child_of_child) and not is_first(child_of_child):
                        if next(child_of_child) == ent_token and prev(child_of_child) == child \
                                or (next(child_of_child) == child and prev(child_of_child) == ent_token):
                            if (child not in entity_tokens) or (child_of_child not in entity_tokens):
                                new_entity_tokens.add(child)
                                new_entity_tokens.add(child_of_child)
                                continue

        entity_tokens = set(new_entity_tokens)
    # Sort entity tokens by order in sentence
    entity_tokens = sorted(entity_tokens, key=lambda token: token.i)
    entity_tokens = [t for t in entity_tokens if t.pos_ not in ["PUNCT", "CCONJ"]]
    return entity_tokens


def _get_nearest_left_tokens(entity_tokens: [Token])-> Tuple[List[Token], Token]:
    if len(entity_tokens) == 0:
        return [],None
    # find initial nearest token ...
    nearest_token = None
    cursor = entity_tokens[0]
    while nearest_token is None:
        try:
            cursor = prev(cursor)
            nearest_token = cursor if cursor.pos_ not in ["ADP", "PUNCT", "CCONJ"] else None
        except Exception:
                return [],None

    # ... find children of initial nearest token ...
    nearest_tokens = [nearest_token]
    for n_token in nearest_token.children:
        if n_token.i < entity_tokens[0].i:
            nearest_tokens.append(n_token)

    # ... nearest_tokens shouldn't contain entity_tokens ...
    nearest_tokens = set(nearest_tokens) - set(entity_tokens)

    # Sort tokens by order in sentence and cleaning them ...
    nearest_tokens = sorted(nearest_tokens, key=lambda token: token.i)
    nearest_tokens = [t for t in nearest_tokens if t.pos_ not in ["PUNCT", "CCONJ"]]

    return nearest_tokens, nearest_token


def _get_nearest_right_tokens(entity_tokens: [Token]) -> Tuple[List[Token], Token]:
    if len(entity_tokens) == 0:
        return [],None

    # find initial nearest token ...
    nearest_token = None
    # ... cursor = rihgt-most token ...
    # cursor = entity_tokens[0]
    cursor = sorted(entity_tokens, key=lambda token: token.i)[-1]
    while nearest_token is None:
        try:
            cursor = next(cursor)
            nearest_token = cursor if cursor.pos_ not in ["ADP", "PUNCT", "CCONJ"] else None
        except Exception:
                return [],None

    # ... find children of initial nearest token ...
    nearest_tokens = [nearest_token]
    for n_token in nearest_token.children:
        if n_token.i > entity_tokens[0].i:
            nearest_tokens.append(n_token)

    # ... nearest_tokens shouldn't contain entity_tokens ...
    nearest_tokens = set(nearest_tokens) - set(entity_tokens)

    # Sort tokens by order in sentence and cleaning them ...
    nearest_tokens = sorted(nearest_tokens, key=lambda token: token.i)
    nearest_tokens = [t for t in nearest_tokens if t.pos_ not in ["PUNCT", "CCONJ"]]

    return nearest_tokens, nearest_token


def get_copy_of_spacy_doc(doc: Doc):
    """
    Copy-constructor - see https://spacy.io/api/doc#from_bytes
    @param doc:
    @return:
    """
    doc_bytes = doc.to_bytes()
    doc2 = Doc(doc.vocab).from_bytes(doc_bytes)
    return doc2


def is_adjective(token: Token) -> bool:
    # TODO use spacy custom attr
    if token.pos_=="ADJ":
        return True
    return False


def is_noun(token: Token) -> bool:
    # TODO use spacy custom attr
    if token.pos_=="NOUN":
        return True
    return False


def _set_spacy_attribute_masked_extension():
    """
    Add custom masking attr. to spacy.doc.
    @return:
    """

    if not Token.has_extension("is_masked"):
        Token.set_extension("is_masked", default=False)
    if not Token.has_extension("is_removed"):
        Token.set_extension("is_removed", default=False)


def _apply_mask_to_tokens(tokens: List[Token], exceptions: List[Token]) -> List[Token]:
    _set_spacy_attribute_masked_extension()

    head_tokens = _get_head_tokens(tokens)

    for head in tokens:
        if head in exceptions:
            continue
        if not is_adjective(head):
            head._.is_masked = True

    for token in list(set(tokens) - set(head_tokens)):
        if token in exceptions:
            continue
        if is_adjective(token):
            token._.is_masked = True
            token._.is_removed = True
            continue
        if is_noun(token):
            token._.is_masked = True
            continue

    return tokens


def _get_masked_sentences(parsed_sent: Doc, mask: str, masked_token_id_combination, removed_token_id_combination) -> List[str]:
    """
    Generate sentence (as string) with mask-tokens and with removing of tokens.
    @param parsed_sent: 
    @param mask: 
    @param masked_token_id_combination: indices of tokens (i.e. toke.i) for masking
    @param removed_token_id_combination: indices of tokens (i.e. toke.i) for removing
    @return: [sentence_as_string with mask_tokens and without some tokens]
    """
    masked_sent = ""
    for token in parsed_sent:

        if token.i in removed_token_id_combination:
            continue

        if token.i in masked_token_id_combination:
            masked_sent += " " + mask if token.pos_ != "PUNCT" else mask
            continue

        masked_sent += " " + token.text if token.pos_ != "PUNCT" else token.text

    return masked_sent.strip()


def get_combinations(numbers: List[int])-> List[Tuple[int]]:
    """
    Returns combinations with different length (including empty ones) of numbers.
    E.g. for [2,7] returns [(),(0, 4),(7),(2,7)] or for [,] returns [()]
    @param numbers: 
    @return: 
    """
    combs = []
    for quantity_numbers_in_combination in range(0, len(numbers)+1):
        all_combs = [i for i in combinations(numbers, quantity_numbers_in_combination)]
        for combination in all_combs:
            combs.append(combination)
    return combs if len(combs) > 0 else [()]


def get_masked_sentence_variations(parsed_sent: Doc, left_ent_border: int, right_ent_border: int,
                                   token_mask: str,
                                   max_masked_token_combinations=5,
                                   max_removed_token_combinations=2) -> List[str]:
    """
    For given sentence performs masking of tokens related with entity_tokens
    (entity_tokens defined as left-and-right borders).
    Then perform generation of any masked sentence variations (i.e. different combination of masked tokens)
    and return list of variations.
    @param parsed_sent:
    @param left_ent_border: number of character - beginning of entity
    @param right_ent_border: number of character - ending of entity
    @param token_mask: some string for masking of tokens
    @param max_masked_token_combinations limit sentence_variations_quantity - huge amount of them takes a lot of time
    @param max_removed_token_combinations
    @return: [alternative_sentence_as_string]
    """
    # === Define group of tokens for masking ...
    entity_tokens = _get_entity_tokens(parsed_sent, left_ent_border, right_ent_border)
    head_entity_tokens = _get_head_tokens(entity_tokens)
    # ... assume only one head token ...
    if len(head_entity_tokens) == 0:
        print(f">>{parsed_sent.text}\n")
        head_entity_token = None
    else:
        head_entity_token = head_entity_tokens[0]

    nearest_right_tokens, nearest_right_token = _get_nearest_right_tokens(entity_tokens)
    nearest_left_tokens, nearest_left_token = _get_nearest_left_tokens(entity_tokens)

    # === Application of mask - setting is_masked and is_removed attrs...
    _apply_mask_to_tokens(nearest_right_tokens, [])
    _apply_mask_to_tokens(nearest_left_tokens, [])

    # ---  assume that entity_tokens shouldn't be masked ...
    #  TODO consider masking of entity_tokens - in such case entity and is border may change
    #   - e.g. Полярная сова --> Белая сова
    #
    # _apply_mask_to_tokens(entity_tokens, [head_entity_token])

    # === Get combinations of removing and masking token ids
    # (i.e. token_ids = number of token in sentence = token.i) ...
    masked_token_ids = [t.i for t in parsed_sent if t._.is_masked]
    # === ... if there are very many combinations - randomly get only some of them ...
    if len(masked_token_ids) > max_masked_token_combinations:
        masked_token_ids = random.sample(masked_token_ids, max_masked_token_combinations)
    masked_token_combinations = get_combinations(masked_token_ids)

    removed_token_ids = [t.i for t in parsed_sent if t._.is_removed]
    # === ... if there are very many combinations - randomly get only some of them ...
    if len(removed_token_ids) > max_removed_token_combinations:
        removed_token_ids = random.sample(removed_token_ids, max_removed_token_combinations)
    removed_token_combinations = get_combinations(removed_token_ids)

    masked_token_combinations = remove_combinations_with_masked_token_following_successively(masked_token_combinations)
    removed_token_combinations = remove_combinations_with_masked_token_following_successively(removed_token_combinations)

    # === Generate sentences = strings with all combinations of masked and removed tokens...
    sentence_variations = []
    for remove_comb in removed_token_combinations:
        for masked_comb in masked_token_combinations:
            # ... skip empty mask combination ...
            if len(masked_comb) == 0:
                continue
            # ... parsed_sent == spacy.Doc
            masked_sent = _get_masked_sentences(parsed_sent, token_mask, masked_comb, removed_token_combinations)
            # print(masked_sent)
            sentence_variations.append(masked_sent)

    return sentence_variations


def remove_combinations_with_masked_token_following_successively(masked_token_combinations: List[List[int]]) -> List[int]:
    filtered_masked_token_combinations = []

    for combination in masked_token_combinations:
        warning_tokens_ids = [combination[id_index] for id_index in range(0, len(combination) - 1)
                            if combination[id_index] + 1 == combination[id_index + 1]]
        if len(warning_tokens_ids) == 0:
            filtered_masked_token_combinations.append(combination)
        else:
            continue

    return filtered_masked_token_combinations


def get_demasked_sentence_variations(mask_sentence_variations: List[str],
                                     tokenizer: AutoTokenizer, model: AutoModelForMaskedLM, max_sentence_variations=0):
    """
    For given masked_sentences create demasked variations containing all combinations of tokens, generated by model.
    @param mask_sentence_variations: sentences containing mask-tokens
    @param tokenizer:
    @param model:
    @param max_sentence_variations:
    @return: [sentences without mask-tokens]
    """
    demasked_sentences = []
    for masked_sentence in mask_sentence_variations:
        # token_alternatives = list of [alternatives_of_N_token]
        token_alternatives = _get_alternatives_for_masked_tokens(tokenizer, model, masked_sentence)
        # ... product(*token_alternatives) - e.g. from [ [1,2], [9] ] gives [[1, 9], [2, 9]]
        for combination_of_alternative_tokens in product(*token_alternatives):
            demasked_sentence = masked_sentence
            for alternative_token in combination_of_alternative_tokens:
                demasked_sentence = demasked_sentence.replace(tokenizer.mask_token, alternative_token, 1)

            demasked_sentences.append(demasked_sentence)
            # print(demasked_sentence)
            demasked_sentences = random.sample(demasked_sentences, max_sentence_variations) \
                if max_sentence_variations > 0 and max_sentence_variations<len(demasked_sentences) else demasked_sentences
    return demasked_sentences


def is_suitable_token(word: str) -> bool:
    """
    Naive checking that given word is good.
    @param word:
    @return:
    """

    if is_contain_special_characters(word):
        return False

    if word.islower() and is_single_char(word):
        return False

    return True


def is_single_char(word: str)-> bool:
    return len(word.strip()) == 1


def is_contain_special_characters(word: str)-> bool:
    """
    Are special characters in given word.
    """
    non_spec_char_pattern = r"[^А-Я-а-я-0-9A-Z-a-z\-\+]+"
    spec_chars = re.findall(non_spec_char_pattern, word)
    return len(spec_chars) > 0


