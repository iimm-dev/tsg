import csv
import os
import random
from typing import List

import spacy
from transformers import AutoTokenizer, AutoModelForMaskedLM
from setgen.boundaries_defenition import get_trainset_with_boundaries
from setgen.final_trainset_generation import get_final_SPACY_trainsets
import augmentator.helpers as hp
import utils.file_utils as ut

# --- init logger ---
from text_corpora.utils.clogger import get_logger

logger = get_logger("./", "common")


def generate_augmented_csv_files(in_dir_of_csv_files_with_borders: str, output_dir: str, nlp: spacy.Language,
                                 tokenizer: AutoTokenizer, model: AutoModelForMaskedLM,
                                 sentences_per_out_file=300, max_sentence_size_in_chars=400,
                                 max_masked_token_combinations=4,
                                 max_removed_token_combinations=2,
                                 max_mask_sentence_variations_quantity=30) -> str:
    '''
    Generate augmented_csv_files from csv-files (containing entity borders) in giver dir .
    @param in_dir_of_csv_files_with_borders:
    @param output_dir:
    @param nlp: spacy model - i.e. nlp = spacy.load("ru_core_news_sm")
    @param tokenizer: bert-tokenizer
    @param model: bert-model
    @param sentences_per_out_file: max quantity of sentences in output csv-files
    @param max_sentence_size_in_chars:
    @param max_masked_token_combinations
    @param max_removed_token_combinations
    @param max_mask_sentence_variations_quantity: max quantity of masked sentences variation
    @return:
    '''
    ut.recreate_dirs([output_dir])

    logger.info(f"=== Start generation augmented csv-files for files in [{in_dir_of_csv_files_with_borders}] ===\n")

    augmented_file_paths = []
    out_file = None
    base_name = "augment_train_data_draft.csv"
    out_sent_number = 0

    # For each sentence in initial file ...
    for n, line in enumerate(ut._rows_from_csv_files(in_dir_of_csv_files_with_borders)):

        sentence, lb, rb, entity, cat = _get_info_from(line)
        logger.info(f"sentence: {n}:{sentence} - [{entity}, {lb}, {rb}]")
        # ... skip big sentences - it may take a long time
        if len(sentence) > max_sentence_size_in_chars:
            continue

        # === For each sentence get sentence variations ...
        logger.debug(f"=== Sentence num: {n}, len: {len(sentence)} === \n{sentence}")
        parsed_sent = nlp(sentence)

        mask_sentence_variations = hp.get_masked_sentence_variations(parsed_sent, lb, rb,
                                                                     token_mask=tokenizer.mask_token,
                                                                     max_masked_token_combinations=max_masked_token_combinations,
                                                                     max_removed_token_combinations=max_removed_token_combinations)
        logger.debug(f" Mask sentence variations quantity: {len(mask_sentence_variations)}")

        logger.info(f"... mask_sentence_variations: {len(mask_sentence_variations)}")
        # ... if mask_sentence_variations > allowed resulted max_mask_sentence_variations = get only some mask_sentence_variations
        if len(mask_sentence_variations) > max_mask_sentence_variations_quantity:
            mask_sentence_variations = random.sample(mask_sentence_variations, max_mask_sentence_variations_quantity)
            logger.info(f"... limit mask_sentence_variations to: {len(mask_sentence_variations)}")

        demasked_sentence_variations = hp.get_demasked_sentence_variations(
            mask_sentence_variations, tokenizer, model, max_sentence_variations=max_mask_sentence_variations_quantity)
        logger.debug(f" Demasked sentence variation quantity: {len(demasked_sentence_variations)}")

        # === Write sentence variations in csv-file...
        is_original_sentence_written = False
        for varied_sentence in demasked_sentence_variations:
            logger.debug(f"in_sent_number={n}  out_sent_number={out_sent_number} ")
            logger.info(f"in_sent_number={n}  out_sent_number={out_sent_number}  ")
            # ... for each M sentences create new file with augmented sentences...
            if out_sent_number >= sentences_per_out_file or out_sent_number == 0:
                _close_file_if_opened(out_file)
                # ... create new name for next out file ...
                file_num = len(augmented_file_paths)
                filepath = os.path.join(output_dir, str(file_num) + "_" + base_name)
                out_file = open(filepath, mode="w", encoding="utf8")
                augmented_file_paths.append(filepath)
                out_sent_number = 0
                logger.info(f"[{n}] sentences have been processed... ")
                logger.info(
                    f"[{file_num}] files with [{sentences_per_out_file * file_num}] augmented sentences have been generated ... ")
            # ... write original sentence to file
            if not is_original_sentence_written:
                writer = csv.writer(out_file, delimiter=',')
                writer.writerow([sentence, entity, cat])
                out_sent_number += 1
                is_original_sentence_written = True

            # ... write aug. sentence to file
            writer = csv.writer(out_file, delimiter=',')
            writer.writerow([varied_sentence, entity, cat])
            out_sent_number += 1

    _close_file_if_opened(out_file)

    logger.info(f"=== End generation augmented csv-files for files in dir [{in_dir_of_csv_files_with_borders}]\n"
                f"=== Got {len(augmented_file_paths)} files in {output_dir}\n")
    return output_dir


# TODO consider to move method to sep_prep package
# def join_csv_files_without_borderds_into_spacy_trainset_files(input_dir_with_augmented_cvs_files,
#                                                               temp_out_dir_for_csv_files_with_good_boundaries,
#                                                               temp_out_dir_for_csv_files_with_wrong_boundaries,
#                                                               output_dir_with_train_files,
#                                                               row_quantity_per_train_file=600000,
#                                                               output_base_filename="trainset_part_") -> List[str]:
#     ut.recreate_dirs([temp_out_dir_for_csv_files_with_wrong_boundaries,
#                       temp_out_dir_for_csv_files_with_good_boundaries,
#                       output_dir_with_train_files])
#
#     logger.info(f"=== Start joining csv-files from [{input_dir_with_augmented_cvs_files} "
#                 f"into spacy json training files ===\n")
#     dir_for_files_with_boundaries = get_trainset_with_boundaries(input_dir_with_augmented_cvs_files,
#                                                                  temp_out_dir_for_csv_files_with_good_boundaries,
#                                                                  temp_out_dir_for_csv_files_with_wrong_boundaries)
#
#     trainset_file_paths = get_final_SPACY_trainsets(dir_for_files_with_boundaries, output_dir_with_train_files,
#                                                     row_quantity_per_file=row_quantity_per_train_file,
#                                                     out_file_prefix=output_base_filename)
#
#     logger.info(f"=== End joining augmented csv-files from [{input_dir_with_augmented_cvs_files} "
#                 f"got {len(trainset_file_paths)} spacy json training files ===\n")


def _close_file_if_opened(file):
    try:
        file.close()
    except (IOError, UnboundLocalError, AttributeError):
        pass


def _get_info_from(line):
    sentence = line[0]
    entity = line[1]
    cat = line[2]
    lb = int(line[3])
    rb = int(line[4])
    return sentence, lb, rb, entity, cat
