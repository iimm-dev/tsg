import csv
import json
import os
import random

import augmentator.helpers as hp
import spacy
import unittest
from spacy.tokens import Doc, Token

# ================================
# === 2. Add entity boundaries ===
# ================================
from setgen.boundaries_defenition import get_trainset_with_boundaries

# --- Create temporary dirs ---
input_dir = output_dir_raw_trainsets = "/home/user/Downloads/lenta_set/lenta_trainset/dir_raw_testsets_with_redefined_entities"
main_output_test_path =  "/home/user/Downloads/lenta_set/lenta_trainset/"
output_dir_raw_trainsets_with_boundaries = os.path.join(main_output_test_path, "files_with_boundaries")
output_dir_for_entity_with_wrong_boundaries = os.path.join(main_output_test_path, "files_with_wrong_boundaries")
# os.makedirs(output_dir_raw_trainsets_with_boundaries)
# os.makedirs(output_dir_for_entity_with_wrong_boundaries)

get_trainset_with_boundaries(input_dir,
                             output_dir_raw_trainsets_with_boundaries,
                             output_dir_for_entity_with_wrong_boundaries)


# =========================================
# === 3 Create final training json file ===
# =========================================
from setgen.final_trainset_generation import get_final_SPACY_trainset, get_final_SPACY_trainsets

input_dir = output_dir_raw_trainsets_with_boundaries

# ===== Save train_data =====
# train_data_path = "/home/lomov/drv/tsg/training/spacy_train_data/train_data.json"
trainset_dir = os.path.join(main_output_test_path, "trainsets")

# get_final_SPACY_trainset(input_dir, final_trainset)
get_final_SPACY_trainsets(input_dir, trainset_dir, row_quantity_per_file=600000 )

