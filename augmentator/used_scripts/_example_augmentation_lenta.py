import logging
import os
import re

import augmentator.helpers as hp
import spacy
import unittest
from spacy.tokens import Doc, Token
import augmentator.augmentation as aa
import setgen.final_trainset_generation as fg
import utils.file_utils as ut

# ===================
# === Init models ===
# ===================
model_path = "/home/user/Downloads/arctic/rubert_cased_L-12_H-768_A-12_pt"
nlp = spacy.load("ru_core_news_sm")
tokenizer, model = hp.initTokeniserAndModel(model_path)

# =================
# === Init path ===
# =================
base_dir="/home/user/Downloads/lenta_set/lenta_trainset/"
in_dir_of_csv_files_with_borders = base_dir + "files_with_boundaries"

augmented_file_dir = base_dir + "out_augm_tmp/aug_files"
# ut.force_remove_dir(augmented_file_dir)

output_dir_for_files_with_boundaries = base_dir + "out_augm_tmp/tmp_output_dir_for_files_with_boundaries"
# ut.force_remove_dir(output_dir_for_files_with_boundaries)

output_dir_for_entity_with_wrong_boundaries = base_dir + "out_augm_tmp/tmp_dir_for_entity_with_wrong_boundaries"
# ut.force_remove_dir(output_dir_for_entity_with_wrong_boundaries)

output_dir_with_train_files = base_dir + "out_augm_tmp/output_dir_with_train_files"
# ut.force_remove_dir(output_dir_with_train_files)

# ===================================================================================
# === get augmented files [sentence, entity, category, left_border, right_border] ===
# ===================================================================================
augmented_file_dir = aa.generate_augmented_csv_files(in_dir_of_csv_files_with_borders, augmented_file_dir,
                                                     nlp, tokenizer, model,
                                                     sentences_per_out_file=600000,
                                                     max_sentence_size_in_chars=400,
                                                     max_mask_sentence_variations_quantity=5)

# =========================================================
# === join augmented csv files into training json-files ===
# =========================================================
spacy_json_train_file_dir = fg.join_csv_files_with_borders_into_spacy_trainset_files(augmented_file_dir,
                                                                                     output_dir_for_files_with_boundaries,
                                                                                     output_dir_for_entity_with_wrong_boundaries,
                                                                                     output_dir_with_train_files,
                                                                                     row_quantity_per_train_file=600000)

files = [f for f in os.listdir(output_dir_with_train_files) if
         os.path.isfile(os.path.join(output_dir_with_train_files, f))]

logging.info(f"=== Got {len(files)} augmented files for training in {output_dir_with_train_files}")
# for n in files:
#     logging.info(n)


