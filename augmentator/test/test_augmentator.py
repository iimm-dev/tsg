import os
import augmentator.helpers as hp
import spacy
import unittest

import augmentator.augmentation as aa
import utils.file_utils as ut

class AugmentatorTestCase(unittest.TestCase):
    def setUp(self):
        # init spacy model ...
        self.nlp = spacy.load("ru_core_news_sm")
        self.token_mask = "{mask}"
        self.initModelAndTokenizer()

    def tearDown(self):
        return

    def initModelAndTokenizer(self):
        # init betr model ...
        self.model_path = "/home/user/Downloads/arctic/rubert_cased_L-12_H-768_A-12_pt/"
        self.tokenizer, self.model = hp.initTokeniserAndModel(self.model_path)

    def test_generate_augmented_files_and_join_them_into_spacy_trainset_files(self):
        in_dir = "./data/test_border_csv"

        augmented_file_dir = "./data/out/out_augmented_files"
        output_dir_for_files_with_boundaries = "./data/out/tmp_output_dir_for_files_with_boundaries"
        output_dir_for_entity_with_wrong_boundaries = "./data/out/tmp_dir_for_entity_with_wrong_boundaries"
        output_dir_with_train_files = "./data/out/output_dir_with_train_files"

        # get augmented files ...
        augmented_file_dir = aa.generate_augmented_csv_files(in_dir, augmented_file_dir,
                                                             self.nlp, self.tokenizer, self.model,
                                                             sentences_per_out_file=1,
                                                             max_sentence_size_in_chars=400,
                                                             max_mask_sentence_variations_quantity=30)

        files = [f for f in os.listdir(augmented_file_dir) if os.path.isfile(os.path.join(augmented_file_dir, f))]
        assert len(files) > 0

        # join augmented csv files into training json-files ...
        spacy_json_train_file_dir = aa.join_csv_files_without_borderds_into_spacy_trainset_files(augmented_file_dir,
                                                                                                 output_dir_for_files_with_boundaries,
                                                                                                 output_dir_for_entity_with_wrong_boundaries,
                                                                                                 output_dir_with_train_files,
                                                                                                 row_quantity_per_train_file=25)

        files = [f for f in os.listdir(output_dir_with_train_files) if
                 os.path.isfile(os.path.join(output_dir_with_train_files, f))]
        assert len(files) > 0


    # TODO check augmentation for
    # В 1929 г. известный полярный исследователь В. Визе выдвинул идею создания первой полярной научной дрейфующей станции.', 20, 116, 'ИОПД'
    # Впоследствии вектор на международное сотрудничество и экономическое освоение региона был подтвержден в новой Стратегии развития Арктической зоны Российской Федерации и обеспечения национальной безопасности на период до 2020 года, подписанной 20 февраля 2013 г. Президентом РФ В.В. Путиным.
