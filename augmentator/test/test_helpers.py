import re

import augmentator.helpers as hp
import spacy
import unittest
from spacy.tokens import Doc, Token

class HelpersTestCase(unittest.TestCase):
    def setUp(self):
        # init spacy model ...
        self.nlp = spacy.load("ru_core_news_sm")
        self.token_mask="{mask}"

    def tearDown(self):
        return

    def initModelAndTokenizer(self):
        # init betr model ...
        self.model_path = "/home/user/Downloads/arctic/rubert_cased_L-12_H-768_A-12_pt/"
        self.tokenizer, self.model = hp.initTokeniserAndModel(self.model_path)

    def test_get_alternatives_for_masked_tokens(self):
        '''
        Test for checking possibilities of masking language model.
        @return:
        '''
        self.initModelAndTokenizer()
        # model_path = "/home/user/Downloads/arctic/rubert_cased_L-12_H-768_A-12_pt/"
        # tokenizer, model = hp.initTokeniserAndModel(model_path)

        # sentence = f"Ледокол {self.tokenizer.mask_token}  самым большим и мощным в {self.tokenizer.mask_token}."
        sentence = f"По {self.tokenizer.mask_token} в {self.tokenizer.mask_token} {self.tokenizer.mask_token} система инициализации {self.tokenizer.mask_token} и файловая система ZFS."

        alternatives = hp._get_alternatives_for_masked_tokens(self.tokenizer, self.model, sentence)

        for n, token_alternatives in enumerate(alternatives):
            print(f"=== {n} token alternatives:")
            for alternative in token_alternatives:
                print(f" {alternative : <20} ")

        assert len(alternatives) == 2
        assert len(alternatives[0]) > 0
        assert len(alternatives[1]) > 0


    def test_get_entity_tokens(self):
        # nlp = spacy.load("ru_core_news_sm")

        sentence = "Читайте последние новости на тему в ленте новостей на сайте РИА Новости."
        parsed_sent = self.nlp(sentence)

        # --- Читайте
        ent, lb, rb ="Читайте", 0, 7
        # e = parsed_sent[2]
        # idx = parsed_sent[2].idx
        tokens = hp._get_entity_tokens(parsed_sent, lb, rb)
        assert ent in [t.text for t in tokens]

        # ---
        ent, lb, rb ="РИА Новости", 60, 71
        # lb, rb = 8, 17
        tokens = hp._get_entity_tokens(parsed_sent, lb, rb)
        assert set(ent.split()).issubset([t.text for t in tokens])

        # --- ленте новостей
        ent, lb, rb ="ленте новостей", 36, 50
        hp._get_entity_tokens(parsed_sent, lb, rb)
        tokens = hp._get_entity_tokens(parsed_sent, lb, rb)
        assert set(ent.split()).issubset([t.text for t in tokens])


    def test_get_head_tokens(self):
        # nlp = spacy.load("ru_core_news_sm")

        sentence = "Читайте последние новости на тему в ленте новостей на сайте РИА Новости."
        parsed_sent = self.nlp(sentence)
        tokens = parsed_sent[0:3]  # Читайте последние новости
        heads = hp._get_head_tokens(tokens)
        print([t.text for t in heads])
        assert len(heads) == 1
        assert heads[0].text == "Читайте"

        sentence = "США величайшая морская держава современности имеет всего лишь два ледокола и ни одного атомного."
        parsed_sent = self.nlp(sentence)
        tokens = parsed_sent[9:10]  # .. ледокола ..
        heads = hp._get_head_tokens(tokens)
        print([t.text for t in heads])
        assert len(heads) == 1
        assert heads[0].text == "ледокола"

        tokens = parsed_sent[10:14]  # .. и ни одного атомного.
        heads = hp._get_head_tokens(tokens)
        print([t.text for t in heads])
        assert len(heads) == 0
        # assert "атомного" in [t.text for t in heads]


    def test_extend_entity_tokens_by_nearest_child_of_head_token(self):
        # nlp = spacy.load("ru_core_news_sm")
        sentence = "США величайшая морская держава современности имеет всего лишь два ледокола и ни одного атомного."
        ent, lb, rb = "держава", 23, 30
        parsed_sent = self.nlp(sentence)

        entity_tokens = hp._get_entity_tokens(parsed_sent, lb, rb)
        head_tokens = hp._get_head_tokens(entity_tokens)
        head_entity_token = head_tokens[0]

        entity_tokens = hp._extend_entity_tokens_by_nearest_childs_of_head_token(entity_tokens, head_entity_token)
        assert set(["величайшая", "морская", "держава", "современности"]) & set([t.text for t in entity_tokens])

        sentence = "ВМС являются для Штатов средством решения основных проблем, связанных c Арктикой."
        ent, lb, rb = "решения", 34, 41
        parsed_sent = self.nlp(sentence)

        entity_tokens = hp._get_entity_tokens(parsed_sent, lb, rb)
        head_tokens = hp._get_head_tokens(entity_tokens)
        head_entity_token = head_tokens[0]

        entity_tokens = hp._extend_entity_tokens_by_nearest_childs_of_head_token(entity_tokens, head_entity_token)
        print([t.text for t in entity_tokens])
        assert set(["решения","основных","проблем"]) == set([t.text for t in entity_tokens])
        # assert set(["решения"]) == set([t.text for t in entity_tokens])

        sentence = "ВМС являются для Штатов средством решения основных проблем, связанных c Арктикой."
        ent, lb, rb = "Арктикой", 72, 80
        parsed_sent = self.nlp(sentence)

        entity_tokens = hp._get_entity_tokens(parsed_sent, lb, rb)
        head_tokens = hp._get_head_tokens(entity_tokens)
        head_entity_token = head_tokens[0]

        entity_tokens = hp._extend_entity_tokens_by_nearest_childs_of_head_token(entity_tokens, head_entity_token)
        print([t.text for t in entity_tokens])
        assert set(["c", "Арктикой"])== set([t.text for t in entity_tokens])

        sentence = "По умолчанию в GhostBSD применяется система инициализации OpenRC и файловая система ZFS."
        ent, lb, rb = "система", 36, 43
        parsed_sent = self.nlp(sentence)

        entity_tokens = hp._get_entity_tokens(parsed_sent, lb, rb)
        head_tokens = hp._get_head_tokens(entity_tokens)
        head_entity_token = head_tokens[0]

        entity_tokens = hp._extend_entity_tokens_by_nearest_childs_of_head_token(entity_tokens, head_entity_token)
        print([t.text for t in entity_tokens])
        assert set(["система", "инициализации"]) == set([t.text for t in entity_tokens])

        sentence = "По умолчанию в GhostBSD применяется система инициализации OpenRC и файловая система ZFS."
        ent, lb, rb = "применяется", 24, 36
        parsed_sent = self.nlp(sentence)

        entity_tokens = hp._get_entity_tokens(parsed_sent, lb, rb)
        head_tokens = hp._get_head_tokens(entity_tokens)
        head_entity_token = head_tokens[0]

        entity_tokens = hp._extend_entity_tokens_by_nearest_childs_of_head_token(entity_tokens, head_entity_token)
        print([t.text for t in entity_tokens])
        assert set(['GhostBSD', 'применяется', 'система']) == set([t.text for t in entity_tokens])

    def test_get_nearest_left_tokens(self):
        # nlp = spacy.load("ru_core_news_sm")
        sentence = "По умолчанию в GhostBSD применяется система инициализации OpenRC и файловая система ZFS."
        ent, lb, rb = "система", 36, 43
        parsed_sent = self.nlp(sentence)

        entity_tokens = hp._get_entity_tokens(parsed_sent, lb, rb)

        nearest_tokens, nearest_token = hp._get_nearest_left_tokens(entity_tokens)
        print([t.text for t in nearest_tokens])
        assert nearest_token.text == 'применяется'
        assert set(['умолчанию','GhostBSD', 'применяется']) == set([t.text for t in nearest_tokens])


    def test_get_nearest_right_tokens(self):
        # nlp = spacy.load("ru_core_news_sm")
        sentence = "По умолчанию в GhostBSD применяется система инициализации OpenRC и файловая система ZFS."
        ent, lb, rb = "система", 36, 43
        parsed_sent = self.nlp(sentence)

        # sentence = "Международный аэропорт Шереметьево в Санкт-Петербурге является четвертым по размерам в России."
        # ent, lb, rb = "аэропорт Пулково", 14, 29
        # parsed_sent = self.nlp(sentence)
        #
        # for token in parsed_sent:
        #     print(token,"|",token.pos_, token.dep_, token.head)

        entity_tokens = hp._get_entity_tokens(parsed_sent, lb, rb)

        nearest_tokens, nearest_token = hp._get_nearest_right_tokens(entity_tokens)
        print([t.text for t in nearest_tokens])
        assert nearest_token.text == 'инициализации'
        assert set(['инициализации', 'OpenRC']) == set([t.text for t in nearest_tokens])

    def test_spacy_token_custom_attr(self):
        Token.set_extension("is_masked", default=False)
        Token.set_extension("is_removed", default=False)

        sentence = "По умолчанию в GhostBSD применяется система инициализации OpenRC, а также и файловая система ZFS."
        ent, lb, rb = "система", 36, 43
        parsed_sent = self.nlp(sentence)

        parsed_sent[1]._.is_masked = True
        parsed_sent[3]._.is_removed = True

        # sentence = "ВМС являются для Штатов средством решения основных проблем, связанных c Арктикой."
        # ent, lb, rb = "Арктикой", 72, 80
        # parsed_sent = self.nlp(sentence)
        #
        # parsed_sent[3]._.is_masked = True
        #
        # parsed_sent[3]._.is_removed = True


        masked_sent=hp.get_masked_sentence(parsed_sent, self.token_mask)

        print(parsed_sent.text)
        print(masked_sent)

    def test_get_masked_sentence_variations(self):
        # sentence = "По умолчанию в GhostBSD применяется система инициализации OpenRC и файловая система ZFS."
        # ent, lb, rb = "система", 36, 43
        # parsed_sent = self.nlp(sentence)

        # sentence = "ВМС являются для Штатов средством решения основных проблем, связанных c Арктикой."
        # ent, lb, rb = "Арктикой", 72, 80
        # parsed_sent = self.nlp(sentence)

        sentence = "Наряду с Финляндией, Дания выступает за открытие Арктического окна в Северном измерении, поскольку это позволяет использовать организационные возможности, политический вес и привлекать финансовые средства ЕС для решения в Арктике разных вопросов, в которых заинтересована Дания как самостоятельная держава и как член ЕС."
        ent, lb, rb = "Арктике", 222, 229
        parsed_sent = self.nlp(sentence)


        variations_with_masks = hp.get_masked_sentence_variations(parsed_sent, lb, rb, token_mask=self.token_mask)
        assert len(variations_with_masks) > 0

        for variation in variations_with_masks:
            print(variation)
            mask_quantity=sum([1 for _ in re.findall(f"{self.token_mask}",variation)])
            assert mask_quantity > 0



    def test_get_alternative_sentences_(self):
        self.initModelAndTokenizer()

        sentence = "По умолчанию в GhostBSD применяется система инициализации OpenRC и файловая система ZFS."
        ent, lb, rb = "система", 36, 43
        parsed_sent = self.nlp(sentence)

        mask_sentence_variations = hp.get_masked_sentence_variations(parsed_sent, lb, rb, token_mask=self.tokenizer.mask_token)
        demasked_sentence_variations = hp.get_demasked_sentence_variations(mask_sentence_variations, self.tokenizer, self.model)

        assert len(demasked_sentence_variations) > 0

        for demasked_sentence_variation in demasked_sentence_variations:
            print(demasked_sentence_variation)
            assert self.tokenizer.mask_token not in demasked_sentence_variation.split(" ")

    def test_is_suitable_token(self):
        tokens = [",", "paper", "auto-moto", "к", "n", "V", "Б", "А", "Я", "A", "Z"]
        expected_tokens = ["paper", "auto-moto", "V","Б", "А", "Я", "A", "Z"]

        for t in tokens:
            if hp.is_suitable_token(t):
                print(t)
                assert t in expected_tokens

    def test_remove_combinations_with_masked_token_following_successively(self):
        bad_combo = [1,2,5,6,7,8]
        good_combo = [1, 4, 6, 8]
        combos = [bad_combo, good_combo]
        got=hp.remove_combinations_with_masked_token_following_successively(combos)
        assert len(got) == 1

        good_combos = [[1, 4, 6, 8], [1, 4, 6, 9], [1]]
        got = hp.remove_combinations_with_masked_token_following_successively(good_combos)
        assert len(got) == len(good_combos)

        bad_combos = [[1,2,5,6,7,8], [1, 3, 5, 6, 9, 10]]
        got = hp.remove_combinations_with_masked_token_following_successively(bad_combos)
        assert len(got) == 0

    def test_draft(self):
        self.initModelAndTokenizer()

        sentence = "СССР с самого начала сделал ставку на атомные ледоколы и на развитие ледокольного флота в целом."
        ent, lb, rb = "ледокола", 66, 74
        parsed_sent = self.nlp(sentence)

        sentence = "США величайшая морская держава современности имеет всего лишь два ледокола и ни одного атомного."
        ent, lb, rb = "ледокола", 66, 74
        parsed_sent = self.nlp(sentence)


# "СССР с самого начала сделал ставку на атомные ледоколы и [MASK] [MASK] ледокольного флота в целом."
# "СССР с самого начала сделал ставку на атомные ледоколы и развитие развитие ледокольного флота в целом."
# "СССР с самого начала сделал ставку на атомные ледоколы и развитие развития ледокольного флота в целом."


        variations_with_masks = hp.get_masked_sentence_variations(parsed_sent, lb, rb, token_mask=self.tokenizer.mask_token)
        demasked_sentence_variations = hp.get_demasked_sentence_variations(variations_with_masks, self.tokenizer, self.model)

        for variation in variations_with_masks:
            print(variation)

        print("==============================")
        for variation in demasked_sentence_variations:
            print(variation)


