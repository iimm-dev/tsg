import json
import os
import random

json_file="/home/user/Downloads/arctic/train_data.json"
out_train_file="/home/user/Downloads/arctic/dp_trainset/train.txt"
out_valid_file="/home/user/Downloads/arctic/dp_trainset/valid.txt"
out_test_file="/home/user/Downloads/arctic/dp_trainset/test.txt"

# ==================
# === deprecated ===
# ==================

# , valid.txt, and test.txt

with open(json_file, mode="r", encoding="utf8") as file:
    json_data = json.load(file)

random.shuffle(json_data)

with open(out_train_file, mode="w", encoding="utf8") as file_train:
    with open(out_valid_file, mode="w", encoding="utf8") as file_valid:
        with open(out_test_file, mode="w", encoding="utf8") as file_test:
            for n,line in enumerate(json_data):
                str = line[0]
                val = line[1]
                lb=val["entities"][0][0]
                rb = val["entities"][0][1]
                cat = val["entities"][0][2]
                entity = str[lb:rb]

                # Collect token and their borders ...
                tokens = str.split(" ")
                token_left_borders = []
                current_left_border = 0
                for token in tokens:
                    lbt = str.find(token, current_left_border)
                    if lbt == -1:
                        raise Exception(f"token [{token}] doesn't found in [{str}]")
                    token_left_borders.append(lbt)
                    current_left_border = lbt

                # Assign token tags ...
                tags = []
                is_first_token = True
                for i,token in enumerate(tokens):
                    if token_left_borders[i]==lb: # is token in entity ?
                        if is_first_token:
                            tags.append("B-"+cat)
                            is_first_token=False
                        else:
                            tags.append("I-" + cat)
                    else:
                        tags.append("O")

                # Write token and tag ...
                tagged_str = ""
                for i,token in enumerate(tokens):
                    token_and_tag=f"{token} {tags[i]} {os.linesep}"
                    tagged_str += token_and_tag
                tagged_str += os.linesep

                if n < 90:
                    file_train.write(tagged_str)
                elif n > 90 and n < 100:
                    file_valid.write(tagged_str)
                else:
                    file_test.write(tagged_str)

                if n > 120:
                    break

                # entity = str[lb:rb]
                # newStr = "[Предложение] "+str+ os.linesep+"[Сущность] "+entity+ " [Конец]"+ os.linesep
                # if n<300000:
                #     file_train.write(newStr)
                # else:
                #     file_valid.write(newStr)


