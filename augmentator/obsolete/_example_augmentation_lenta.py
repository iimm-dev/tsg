import csv
import json
import os
import random

import augmentator.helpers as hp
import spacy
import unittest
from spacy.tokens import Doc, Token
from transformers import AutoModelWithLMHead, AutoTokenizer, AutoModelForMaskedLM

# --- init logger ---
from text_corpora.utils.clogger import get_logger

logger = get_logger("./", "common")

input_json_file = "/home/user/Downloads/arctic/train_data.json"
# new_csv_path = "/home/user/Downloads/arctic/augm_sentences/dir_raw_trainsets/augment_train_data_draft.csv"
main_output_test_path = "/home/user/Downloads/arctic/augm_sentences/"

# init spacy model ...
nlp = spacy.load("ru_core_news_sm")




if not os.path.exists(main_output_test_path):
    os.makedirs(main_output_test_path)

output_dir_raw_trainsets = os.path.join(main_output_test_path, "dir_raw_trainsets")
if not os.path.exists(output_dir_raw_trainsets):
    os.makedirs(output_dir_raw_trainsets)

augmen_out_csv_file = os.path.join(output_dir_raw_trainsets, "augment_train_data_draft.csv")

# random.shuffle(json_data)


# init bert model ...
model_path = "/home/user/Downloads/arctic/rubert_cased_L-12_H-768_A-12_pt/"
tokenizer, model = hp.initTokeniserAndModel(model_path)

out_json_data = {}

num = 0
filename = "augment_train_data_draft.csv"
augmen_out_csv_file = os.path.join(output_dir_raw_trainsets, str(num) + "_" + filename)

# fl = open(augmen_out_csv_file, mode="w", encoding="utf8")
# # for n,line in enumerate(json_data[:100]):
# for n,line in enumerate(json_data):
#
#     #  for each M sentences create new file with augmented sentences...
#     if n % 300 == 0:
#         try:
#             fl.close()
#         except IOError:
#             pass
#         # fl = open(augmen_out_csv_file, mode="w", encoding="utf8")
#         # create new name for outtput file
#         augmen_out_csv_file = os.path.join(output_dir_raw_trainsets, str(num) + "_" + filename)
#         while os.path.exists(augmen_out_csv_file):
#             num += 1
#             augmen_out_csv_file = os.path.join(output_dir_raw_trainsets, str(num) + "_" + filename)
#         fl = open(augmen_out_csv_file, mode="w", encoding="utf8")
#
#
#     snt = line[0]
#     val = line[1]
#     lb=val["entities"][0][0]
#     rb = val["entities"][0][1]
#     cat = val["entities"][0][2]
#     entity = snt[lb:rb]
#
#     # ---------------------
#     # target_sent =  "США величайшая морская держава современности имеет всего лишь два ледокола и ни одного атомного."
#     # if target_sent in snt:
#     #     print(f"{snt} {entity}, {lb}, {rb} ")
#     #     break
#     # else:
#     #     continue
#
#     # ---------------------
#
#
#     sentence = snt
#     if len(sentence)>400:
#         # skip big sentences
#         continue
#
#     print(f"=== sentence num: {n}, len: {len(snt)} \n {snt}")
#     parsed_sent = nlp(sentence)
#
#     # === For each sentence get sentence_variations
#     mask_sentence_variations = hp.get_masked_sentence_variations(parsed_sent, lb, rb, token_mask=tokenizer.mask_token)
#     print(f" mask_sentence_variations: {len(mask_sentence_variations)}")
#
#     if len(mask_sentence_variations)>30:
#         continue
#
#     demasked_sentence_variations = hp.get_demasked_sentence_variations(mask_sentence_variations, tokenizer, model)
#     print(f" demasked_sentence_variations: {len(demasked_sentence_variations)}")
#     # for sent in demasked_sentence_variations:
#     # fl.writelines((f"{s} ent:{entity : >20} \n" for s in demasked_sentence_variations))
#
#
#     # === Write sentence variations if csv-file
#     # with open(new_csv_path, "w") as fw:
#     for sentence in demasked_sentence_variations:
#         writer = csv.writer(fl, delimiter=',')
#         writer.writerow([sentence, entity, cat])
#
#     # --- stop ---
#     # if n > 0:
#     #     break
#
# # os._exit(0)
# ================================
# === 2. Add entity boundaries ===
# ================================
from setgen.boundaries_defenition import get_trainset_with_boundaries

# --- Create temporary dirs ---
input_dir = output_dir_raw_trainsets
output_dir_raw_trainsets_with_boundaries = os.path.join(main_output_test_path, "files_with_boundaries")
output_dir_for_entity_with_wrong_boundaries = os.path.join(main_output_test_path, "files_with_wrong_boundaries")
# os.makedirs(output_dir_raw_trainsets_with_boundaries)
# os.makedirs(output_dir_for_entity_with_wrong_boundaries)

# get_trainset_with_boundaries(input_dir,
#                              output_dir_raw_trainsets_with_boundaries,
#                              output_dir_for_entity_with_wrong_boundaries)


# =========================================
# === 3 Create final training json file ===
# =========================================
from setgen.final_trainset_generation import get_final_SPACY_trainset, get_final_SPACY_trainsets

input_dir = output_dir_raw_trainsets_with_boundaries

# ===== Save train_data =====
# train_data_path = "/home/lomov/drv/tsg/training/spacy_train_data/train_data.json"
trainset_dir = os.path.join(main_output_test_path, "trainsets")

# get_final_SPACY_trainset(input_dir, final_trainset)
get_final_SPACY_trainsets(input_dir, trainset_dir, row_quantity_per_file=600000)
