import os
from transformers import AutoModelWithLMHead, AutoTokenizer, AutoModelForMaskedLM
import torch
import json
import spacy
import deeppavlov.helpers as hp

json_file="/home/user/Downloads/arctic/train_data.json"
model_path="/home/user/Downloads/arctic/rubert_cased_L-12_H-768_A-12_pt/"

with open(json_file, mode="r", encoding="utf8") as file:
    json_data = json.load(file)
print("=== Reading json_data is done")

sentToEntPairs =[]
for n, line in enumerate(json_data):
    # TODO
    if n>20:
        break

    str = line[0]
    val = line[1]
    lb = val["entities"][0][0]
    rb = val["entities"][0][1]
    cat = val["entities"][0][2]
    entity = str[lb:rb]
    sentToEntPairs.append((str,entity,lb,rb))

# ============================================
# === Init Spacy model for syntax analysis ...
# !spacy download ru_core_news_lg
# nlp = spacy.load("ru_core_news_lg")
print("=== Load SPACY model ...")
nlp = spacy.load("ru_core_news_sm")
spacy.require_gpu()


# ====================
# === Augmentation ===
# ====================
# TODO
tokenizer, model = hp._get_alternatives_for_masked_tokens(model_path)


print("=== Start augmentation ...")
n=0
for sentence,entity,lb,rb in sentToEntPairs:
    if len(sentence)>100:
        continue

    # === Define token for replacement ...
    doc = nlp(sentence)
    for token in doc:
        result = f" {token.text : <10}   {'Rel: '+token.dep_ : <10}  {'Part of speech: '+token.pos_ : <10}  {'Head: '+token.head.text : <10}   {'Head_pos: '+token.head.pos_ : <10}    "
        result += f"{'Children: '+' '.join([child.text for child in token.children]) : <50}"
        print(result)

        # print(token.text, "relt:" + token.dep_, "head:" + token.head.text, "head_posi:" + token.head.pos_, "chil:",
        #       [child for child in token.children])
    print(os.linesep)




    # TODO
    n+=1
    if n>20:
        break
    else:
        continue


    # === Replace some word with mask ...
    left_part = sentence[:lb]
    right_part = sentence[rb:]
    sentence = left_part + tokenizer.mask_token + right_part

    # === Processing masked sentence to get tokens for substitution ...
    alternatives = hp._get_alternatives_for_masked_tokens(tokenizer, model, sentence)

    # === Get proposed substitution tokens ...
    for token_alternatives in alternatives:
        for alternative in token_alternatives:
            print(alternative)



def get_augmented_sentences(sentence: str):
    return None