# ===============
# === pytorch ===
# ===============

# from transformers import AutoTokenizer, AutoModel
#
# tokenizer = AutoTokenizer.from_pretrained("bert-base-uncased")
# model = AutoModel.from_pretrained("bert-base-uncased")
#
# inputs = tokenizer("Hello world!", return_tensors="pt")
# outputs = model(**inputs)

# ===================
# === tensor flow ===
# ===================
from transformers import AutoTokenizer, TFAutoModel
import tensorflow as tf

# model_path="/home/user/Downloads/arctic/rubert_cased_L-12_H-768_A-12_v2/"
model_path="/home/user/Downloads/arctic/rubert_cased_L-12_H-768_A-12_pt/"


# new_model = tf.saved_model.load(model_path)
# with tf.compat.v1.Session() as sess:
    # saver = tf.compat.v1.train.import_meta_graph(model_path+"model.ckpt.data-00000-of-00001")
    # saver.restore(sess, model_path+"model.ckpt.data-00000-of-00001")
# sss


# from transformers import *
# from transformers import BertTokenizer, TFBertModel,BertConfig

# bert_tokenizer = BertTokenizer.from_pretrained(model_path)
# bert_model = TFBertForSequenceClassification.from_pretrained('bert-base-uncased',num_labels=num_classes)

# sent= 'Кто звонил на горячую линию Ростелекома тот в цирке не смеется, я решил еще и написать им в ВК.'
# tokens=bert_tokenizer.tokenize(sent)
# print(tokens)

# =================
# from transformers import TFAutoModelWithLMHead, AutoTokenizer
# from transformers import TFBertForMaskedLM
# import tensorflow as tf
# tokenizer = AutoTokenizer.from_pretrained(model_path)
# model = TFAutoModelWithLMHead.from_pretrained(model_path)
# sequence= f"Кто звонил на горячую линию {tokenizer.mask_token} тот в цирке не смеется, я решил еще и написать им в ВК."
# input = tokenizer.encode(sequence, return_tensors="tf")
# mask_token_index = tf.where(input == tokenizer.mask_token_id)[0, 1]
# token_logits = model(input)[0]
# mask_token_logits = token_logits[0, mask_token_index, :]
# top_5_tokens = tf.math.top_k(mask_token_logits, 5).indices.numpy()
#
# for token in top_5_tokens:
#     print(sequence.replace(tokenizer.mask_token, tokenizer.decode([token])))

#     ====================
model_path="/home/user/Downloads/arctic/rubert_cased_L-12_H-768_A-12_pt/"
from transformers import AutoModelWithLMHead, AutoTokenizer, AutoModelForMaskedLM
import torch

tokenizer = AutoTokenizer.from_pretrained(model_path)
# model = AutoModelWithLMHead.from_pretrained(model_path)
model = AutoModelForMaskedLM.from_pretrained(model_path)

sequence= f"Кто звонил в {tokenizer.mask_token}."
sequence=f"Можно отметить несколько важных историко-правовых документов, определивших статус {tokenizer.mask_token}, политику России в отношении Арктики в ХХ ХХ веках и е геополитическое положение в мире Нота Министерства иностранных дел России от 4 сентября 1916 года к правительствам союзных и дружественных государств о принадлежности ей всех открытых земель и островов, расположенных к северу от азиатского побережья Российской Империи."
sequence=f"Ледокол {tokenizer.mask_token}  самым большим и мощным в {tokenizer.mask_token}."
# sequence=f"Ледокол {tokenizer.mask_token}  самым {tokenizer.mask_token} и мощным в {tokenizer.mask_token}."
# sequence=f"Ледокол {tokenizer.mask_token}  самым большим и мощным в мире."

input = tokenizer.encode(sequence, return_tensors="pt")
mask_token_index = torch.where(input == tokenizer.mask_token_id)[1]

token_logits = model(input)[0]
mask_token_logits = token_logits[0, mask_token_index, :]


# top_5_tokens = torch.topk(mask_token_logits, 5, dim=1).indices[0].tolist()

indices = torch.topk(mask_token_logits, 5, dim=1).indices
# top_5_tokens = indices[0].tolist()

for index in indices:
    top_5_tokens = index.tolist()
    print("===")
    # for token in top_5_tokens:
    #     print(sequence.replace(tokenizer.mask_token, tokenizer.decode([token])))

    for token in top_5_tokens:
        print(tokenizer.decode([token]))




