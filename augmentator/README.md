## Augmentator module

Requirements:

* [Russian spaCy model](https://spacy.io/models/ru#ru_core_news_sm)
* [RuBERT](http://docs.deeppavlov.ai/en/master/features/pretrained_vectors.html#bert) - pre-trained embeddings 

Use `__augmentator_example.py` test script as augmentator template. 
