import csv
import json
import os
from random import shuffle

# --- init logger ---
from typing import List

from setgen.boundaries_defenition import get_trainset_with_boundaries
from text_corpora.utils.clogger import get_logger
import utils.file_utils as ut

logger = get_logger("./", "common")


def get_final_SPACY_trainsets(input_dir_with_files_with_boundaries, output_dir_with_train_files,
                              row_quantity_per_file=600000, out_file_prefix="trainset_part_",
                              shuffle_rows=False) -> List[str]:
    """
    Combine input csv-files with_boundaries into JSON files containing some rows for training SPACY model.
    @param input_dir_with_files_with_boundaries:
    @param output_dir_with_train_files:
    @param row_quantity_per_file: row quantity per each trainfile
    @param out_file_prefix: base for creation of N trainset file - i.e. filename = base + _N
    @param shuffle_rows:  gets row from random input files and write it to output
    @return: out_file_paths
    """
    ut.recreate_dirs([output_dir_with_train_files])

    # Open all files - get csv.readers(...) for them ...
    readers = []
    ended_readers_num = []
    for filepath in ut._file_paths(input_dir_with_files_with_boundaries):
        fl = open(filepath, "r")
        readers.append(csv.reader(fl))

    train_data = []
    trainset_file_paths = []
    while len(readers) > 0:
        if shuffle_rows:
            shuffle(readers)

        for rn, reader in enumerate(readers):
            row = next(reader, None)
            if row is None:
                ended_readers_num.append(rn)
                continue
            data = (row[0], {"entities": [(int(row[3]), int(row[4]), row[2])]})
            train_data.append(data)
            n = len(train_data)

            # ===== for each Q rows - save train_data_part ...
            if n % int(row_quantity_per_file) == 0 and n > 0:
                outpath = os.path.join(output_dir_with_train_files,
                                       out_file_prefix + str(len(trainset_file_paths)) + ".json")
                with open(outpath, "w") as file:
                    logger.info("=== Saving [{}] part train data to [{}] ==="
                                .format(len(trainset_file_paths), outpath))
                    file.write(json.dumps(train_data, ensure_ascii=False))
                train_data = []
                trainset_file_paths.append(outpath)

        readers = [r for rn, r in enumerate(readers) if rn not in ended_readers_num]

    # save remain part of rows ...
    if len(train_data) > 0:
        outpath = os.path.join(output_dir_with_train_files, out_file_prefix + str(len(trainset_file_paths)) + ".json")
        with open(outpath, "w") as file:
            logger.info("=== Saving [{}] part train data to [{}] ==="
                        .format(len(trainset_file_paths), outpath))
            file.write(json.dumps(train_data, ensure_ascii=False))
        trainset_file_paths.append(outpath)

    return trainset_file_paths


# TODO consider replace with get_final_SPACY_trainsets(...)
def get_final_SPACY_trainset(input_dir_with_files_with_boundaries, output_path_to_file_with_final_trainset):
    """
    Combine input csv-files with_boundaries into single JSON file for training SPACY model.
    @param input_dir_with_files_with_boundaries:
    @param output_path_to_file_with_final_trainset:
    @return: output path to JSON file for training SPACY model.
    """

    filenames = os.listdir(input_dir_with_files_with_boundaries)
    train_data = []
    for file_name in filenames:
        csv_path = os.path.join(input_dir_with_files_with_boundaries, file_name)
        logger.info("=== Processing file - [{}] ===".format(csv_path))
        with open(csv_path, "r") as fr:
            for row in csv.reader(fr):
                data = (row[0], {"entities": [(int(row[3]), int(row[4]), row[2])]})
                train_data.append(data)
    logger.info("=== Train data consists of [{}] sentences ===".format(len(train_data)))
    # ===== Save train_data =====
    with open(output_path_to_file_with_final_trainset, "w") as file:
        logger.info("=== Saving train data to [{}] ===".format(output_path_to_file_with_final_trainset))
        file.write(json.dumps(train_data, ensure_ascii=False))
    return output_path_to_file_with_final_trainset


def join_csv_files_with_borders_into_spacy_trainset_files(input_dir_with_cvs_files,
                                                             temp_out_dir_for_csv_files_with_good_boundaries,
                                                             temp_out_dir_for_csv_files_with_wrong_boundaries,
                                                             output_dir_with_train_files,
                                                             row_quantity_per_train_file=600000,
                                                             output_base_filename="trainset_part_") -> List[str]:
    logger.info(f"=== Start joining csv-files from [{input_dir_with_cvs_files} "
                f"into spacy json training files ===\n")
    dir_for_files_with_boundaries = get_trainset_with_boundaries(input_dir_with_cvs_files,
                                                                 temp_out_dir_for_csv_files_with_good_boundaries,
                                                                 temp_out_dir_for_csv_files_with_wrong_boundaries)

    trainset_file_paths = get_final_SPACY_trainsets(dir_for_files_with_boundaries, output_dir_with_train_files,
                                                    row_quantity_per_file=row_quantity_per_train_file,
                                                    out_file_prefix=output_base_filename,
                                                    shuffle_rows=True)

    logger.info(f"=== End joining augmented csv-files from [{input_dir_with_cvs_files} "
                f"got {len(trainset_file_paths)} spacy json training files ===\n")
