from setgen.multiproc_raw_trainset_generation import multiproc_form_raw_trainset
from text_corpora.utils.corpus_readers import RussianPlainTextCorpusReader
from multiprocessing import Process
import shutil
import json
import os

# ======================================================================
# === 1. Create raw set of sentences with entities(multiprocessing)=====
# ====== by multiproc_raw_trainset_generation.py =======================
# ======= {[sentence, sent_entity, category]} ==========================
# ======================================================================

# --- Create temporary dirs ---
# habr_csv_dir_path = "/media/_hit/_habr_post"
input_dir_path = "/home/user/Downloads/lenta_set"
output_path = os.path.abspath(os.path.join(input_dir_path, "lenta_trainset"))
# if os.path.exists(main_output_test_path):
#     shutil.rmtree(main_output_test_path)
# os.makedirs(main_output_test_path)

# output_dir_raw_trainsets = os.path.join(main_output_test_path, "dir_raw_trainsets")
output_dir_raw_testsets = os.path.join(output_path, "dir_raw_testsets")
# os.makedirs(output_dir_raw_testsets)

# === Chosen words ===
entitiesDict_file = os.path.abspath(os.path.join(input_dir_path, "base_words.txt"))
# entitiesDict_file = os.path.abspath(os.path.join(habr_csv_dir_path, "import_word_auto_corrected_100.txt"))

# :param categoryToEntitiesDict: {category1: [[entity1] ... [entity2]]}
# chosenEntities=set()
with open(entitiesDict_file, mode="r", encoding="utf8") as file:
    category2words = json.load(file)
    # words = file.readlines()
categoryToEntitiesDict = category2words
# for cat, words in category2words.items():
#     print(cat,words)
#     # chosenEntities.add(word.strip())
#
# os.exit(0)

# --- Init text corpus reader ---

# corpus_root = "/home/user/Downloads/habr_post/" # 1 480 files
# DOC_PATTERN = r"tmp/\w+.txt"

corpus_root = input_dir_path
DOC_PATTERN = r"docs/\w+.txt"
corpusReader = RussianPlainTextCorpusReader(corpus_root, fileids=DOC_PATTERN, encoding="utf-8")
# 204 000 docs

# ... split/resplit corpus into sections (can be done single time for new text corpus) ---
section_size = 5
corpusReader.split_to_sections(section_size) # 232 128 files # 204 - last section
section_quantity = len(corpusReader._sections.keys())
# section_range = range(0, section_quantity - 1)

# sectionquantity[400] == =
section_range = range(0, 50)


# section_range = range(0, 100)

# section_range = range(0, 10)
# section_range = range(10, 30)
# section_range = range(30, 50)
# section_range = range(50, 70)
# section_range = range(70, 90)
# section_range = range(110, 130)
# section_range = range(130, 150)
# section_range = range(150, 180)
# section_range = range(180, 205)
# section_range = range(205, 220)


# for section in range(0, section_quantity):
#     for num, doc in enumerate(corpusReader.section_docs(section)):
#         print("=== section [{}] doc [{}] ===".format(section,num))

# os._exit(0)

if __name__ == '__main__':  # to separate processes in Windows
    procs = []

    # ... create and start process for each section
    for section in section_range:
        proc = Process(target=multiproc_form_raw_trainset,
                       args=(corpusReader, categoryToEntitiesDict, output_dir_raw_testsets, section))
        procs.append(proc)
        proc.start()

    # ... waiting until each process is finished
    for proc in procs:
        proc.join()
