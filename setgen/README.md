## Train set generation module

Generation of train set consists of the following steps:
1. Create raw sets of sentences with entities by `raw_trainset_generation.py`  
   
   Multiprocessing way to create raw sets of sentences with entities by `multiproc_raw_trainset_generation.py`
2. Add entities boundaries to raw sets by `boundaries_defenition.py`
3. Form final JSON for training by `final_trainset_generation.py`

Use `__set_generation_example.py` test script as template for train set generation. 

Use `__multiproc_raw_trainset_example.py` test script as template for multiprocessing raw sets generation. 
