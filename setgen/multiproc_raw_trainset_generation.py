from text_corpora.utils.corpus_readers import RussianPlainTextCorpusReader
import os
import csv
from multiprocessing import current_process
from setgen.raw_trainset_generation import get_entities, find_entity_inside_sentence, get_key
import text_corpora.utils.helpers as hp


# --- init logger ---
from text_corpora.utils.corpus_readers import get_logger
logger = get_logger("./", "common")


def multiproc_form_raw_trainset(corpusReader:RussianPlainTextCorpusReader, categoryToEntitiesDict, out_dir, section_number):
    """
    Form raw trainset {sentence, sent_entity, category} as csv file for each section of text corpus
    :param corpusReader: initialised corpusReader
    :param categoryToEntitiesDict: {category1: [["polar circle"] ... ["car"]]}
    :param out_dir: folder to store
    :param section_number: trainset will be formed for given section
    :return:
    """

    result_path = os.path.join(out_dir, "section_" + str(section_number) + ".csv")

    proc_name = current_process().name
    logger.info('========= Starting process %s...' % proc_name)

    with open(result_path, "w", newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        entities = get_entities(categoryToEntitiesDict)

        for num, doc in enumerate(corpusReader.section_docs(section_number)):
            logger.info("=== Processing section:[{}], doc:[{}]/[{}] ===".format(section_number,num,corpusReader.section_size))
            # ... Convert Doc to set of normalised words ...
            sentences = hp.sentences(doc)
            doc_set = set([hp.get_lemma(w) for sentence in sentences for w in hp.get_words(sentence)])
            # ... check if entity (any of its words) is in doc_set ...
            for entity in entities:
                norm_entity = hp.get_ngram_lemmas(entity)
                entity_set = set(hp.get_words(norm_entity))
                if entity_set.intersection(doc_set) is not None:
                    # ... if YES then find sentence with that entity in this doc
                    for sentence in hp.sentences(doc):
                        sent_entity = find_entity_inside_sentence(sentence, norm_entity)
                        if sentence is not None and sent_entity is not None:
                            category = get_key(categoryToEntitiesDict, entity)  # use not normalised entity
                            if category is not None:
                                writer.writerow([sentence, sent_entity, category])  # form result csv file
    logger.info('========= Exiting process %s...' % proc_name)
