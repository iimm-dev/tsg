
# =================================================================================
# === 1. Create raw set of sentences with entities by set_generation.py ===========
# === {[sentence, sent_entity, category]} =========================================
# =================================================================================

import os
import shutil
from text_corpora.utils.corpus_readers import RussianPlainTextCorpusReader
from setgen.raw_trainset_generation import form_raw_trainsets_for_sections

# --- Create output dirs ---
main_output_test_path = os.path.abspath(os.path.join(".", "test_output_dir"))

# --- remove main outputdir if exist ---
# if os.path.exists(main_output_test_path):
#     shutil.rmtree(main_output_test_path, ignore_errors=True) # remove if exist

if not os.path.exists(main_output_test_path):
    os.makedirs(main_output_test_path)

output_dir_raw_trainsets = os.path.join(main_output_test_path, "dir_raw_trainsets")
if not os.path.exists(output_dir_raw_trainsets):
    os.makedirs(output_dir_raw_trainsets)


# --- Init text corpus reader ---
corpus_root = os.path.abspath(os.path.join("..","text_corpora","test_text_corpus"))
DOC_PATTERN = r"docs/\w+.txt"
corpusReader = RussianPlainTextCorpusReader(corpus_root, fileids=DOC_PATTERN, encoding="utf-8")

# ... split/resplit corpus into sections (can be done single time for new text corpus) ---
corpusReader.split_to_sections(2)

# --- Get category to entities dictionary ---
# ... entities_dict can be loader from file
# import json
# categoryToEntitiesDict_file = os.path.abspath(os.path.join("..","text_corpora","test_text_corpus", "resources", "category_to_entities_dict.json"))
# with open(categoryToEntitiesDict_file, mode="r", encoding="utf8") as file:
#     categoryToEntitiesDict = json.loads(file.read())

entities_dict = {"ТСДПД": ["полярное исполнение", "ледокол", "белая сова"]}


form_raw_trainsets_for_sections(corpusReader,
                                entities_dict,
                                output_dir_raw_trainsets,
                                0,1)


# ================================
# === 2. Add entity boundaries ===
# ================================
from setgen.boundaries_defenition import get_trainset_with_boundaries
# --- Create temporary dirs ---
input_dir = output_dir_raw_trainsets
output_dir_raw_trainsets_with_boundaries = os.path.join(main_output_test_path, "files_with_boundaries")
output_dir_for_entity_with_wrong_boundaries = os.path.join(main_output_test_path, "files_with_wrong_boundaries")
os.makedirs(output_dir_raw_trainsets_with_boundaries)
os.makedirs(output_dir_for_entity_with_wrong_boundaries)

get_trainset_with_boundaries(input_dir,
                             output_dir_raw_trainsets_with_boundaries,
                             output_dir_for_entity_with_wrong_boundaries)

# ==========================================================
# === OPTION make augmentation and/or entity clarification:
# augmentator/__augmentation_example.py
# entity_clarifier/_example_entity_clarification.py
# ==========================================================


# =========================================
# === 3 Create final training json file ===
# =========================================
from setgen.final_trainset_generation import get_final_SPACY_trainsets
input_dir = output_dir_raw_trainsets_with_boundaries

# ===== Save train_data =====
# train_data_path = "/home/lomov/drv/tsg/training/spacy_train_data/train_data.json"
# final_trainset = output_dir_raw_trainsets_with_boundaries = \
#     os.path.join(main_output_test_path, "finaltrain_data.json")

output_dir_for_final_trainsets = os.path.join(main_output_test_path, "final_trainsets")
get_final_SPACY_trainsets(input_dir, output_dir_for_final_trainsets)

