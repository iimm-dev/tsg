from setgen.multiproc_raw_trainset_generation import multiproc_form_raw_trainset
from text_corpora.utils.corpus_readers import RussianPlainTextCorpusReader
from multiprocessing import Process
import shutil
import json
import os

# ======================================================================
# === 1. Create raw set of sentences with entities(multiprocessing)=====
# ====== by multiproc_raw_trainset_generation.py =======================
# ======= {[sentence, sent_entity, category]} ==========================
# ======================================================================

# --- Create output dirs ---
main_output_test_path = os.path.abspath(os.path.join(".", "test_output_dir"))

# --- remove main outputdir if exist ---
# if os.path.exists(main_output_test_path):
#     shutil.rmtree(main_output_test_path, ignore_errors=True) # remove if exist

if not os.path.exists(main_output_test_path):
    os.makedirs(main_output_test_path)

output_dir_raw_trainsets = os.path.join(main_output_test_path, "dir_raw_trainsets")
if not os.path.exists(output_dir_raw_trainsets):
    os.makedirs(output_dir_raw_trainsets)

# ----------------------------------
# --- Get categoryToEntitiesDict ---
# ----------------------------------
# categoryToEntitiesDict + {category1: [[entity1] ... [entity2]]}
# categoryToEntitiesDict_file = os.path.abspath(os.path.join("..","text_corpora","test_text_corpus", "resources", "category_to_entities_dict.json"))
# with open(categoryToEntitiesDict_file, mode="r", encoding="utf8") as file:
#     categoryToEntitiesDict = json.loads(file.read())

categoryToEntitiesDict = {"ТСДПД": ["полярное исполнение", "исполнение", "ледокол", "полярная сова"]}

# -------------------------------
# --- Init text corpus reader ---
# -------------------------------
corpus_root = os.path.abspath(os.path.join("..","text_corpora","test_text_corpus"))
DOC_PATTERN = r"docs/\w+.txt"
corpusReader = RussianPlainTextCorpusReader(corpus_root, fileids=DOC_PATTERN, encoding="utf-8")

# ... split/resplit corpus into sections (can be done single time for new text corpus) ---
# set section size
section_size = 2
corpusReader.split_to_sections(section_size)
section_quantity = len(corpusReader._sections.keys())

# ... by-default - process all sections
processing_section_range = range(0, section_quantity - 1)
# processing_section_range = range(0, 20)

# -------------------------------------------------------------------------------------------
# --- Form sectionN.txt files with [sentence, entity, category] for each section in range ---
# -------------------------------------------------------------------------------------------
if __name__ == '__main__':  # to separate processes in Windows
    procs = []

    # ... create and start process for each section
    for section in processing_section_range:
        proc = Process(target=multiproc_form_raw_trainset,
                       args=(corpusReader, categoryToEntitiesDict, output_dir_raw_trainsets, section))
        procs.append(proc)
        proc.start()

    # ... waiting until each process is finished
    for proc in procs:
        proc.join()


# ================================
# === 2. Add entity boundaries ===
# ================================
from setgen.boundaries_defenition import get_trainset_with_boundaries
# --- Create temporary dirs ---
input_dir = output_dir_raw_trainsets
output_dir_raw_trainsets_with_boundaries = os.path.join(main_output_test_path, "files_with_boundaries")
output_dir_for_entity_with_wrong_boundaries = os.path.join(main_output_test_path, "files_with_wrong_boundaries")
os.makedirs(output_dir_raw_trainsets_with_boundaries)
os.makedirs(output_dir_for_entity_with_wrong_boundaries)

get_trainset_with_boundaries(input_dir,
                             output_dir_raw_trainsets_with_boundaries,
                             output_dir_for_entity_with_wrong_boundaries)

# ================================
# === OPTION entity clarification OR augmentation can be done
# ================================

# =========================================
# === 3 Create final training json file ===
# =========================================
from setgen.final_trainset_generation import get_final_SPACY_trainsets
input_dir = output_dir_raw_trainsets_with_boundaries

# ===== Save train_data =====
# train_data_path = "/home/lomov/drv/tsg/training/spacy_train_data/train_data.json"
# final_trainset = output_dir_raw_trainsets_with_boundaries = \
#     os.path.join(main_output_test_path, "finaltrain_data.json")

output_dir_for_final_trainsets = os.path.join(main_output_test_path, "final_trainsets")
get_final_SPACY_trainsets(input_dir, output_dir_for_final_trainsets)

