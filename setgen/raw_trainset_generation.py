import csv
import os
from text_corpora.utils.corpus_readers import RussianPlainTextCorpusReader
import text_corpora.utils.helpers as hp

# --- init logger ---
from text_corpora.utils.corpus_readers import get_logger
logger = get_logger("./", "common")


def get_entity_from_sentence(sentence, norm_entity):
    """
    Get entity from sentence as a slice between the min and max entity index
    :param sentence: string with some words (e.g. "angry cats")
    :param norm_entity: entity in normal form (e.g. "cat")
    :return: entity as a string or None
    """
    sent_words = hp.get_words(sentence)
    entity_words_to_lemmas = [(e, hp.get_ngram_lemmas(e)) for e in hp.get_words(norm_entity)]
    words_indexes = []
    for e, eb in entity_words_to_lemmas:
        for word_index, word_lemma in enumerate(((w, hp.get_ngram_lemmas(w)) for w in sent_words)):
            if word_lemma[1].startswith(eb):
                words_indexes.append(word_index)
                break   # to avoid of repeatable finding of the same word entity
    if len(words_indexes):
        return " ".join(sent_words[min(words_indexes):max(words_indexes) + 1])
    else:
        return None


def find_entity_inside_sentence(sentence, norm_entity):
    """
    Find entity inside given sentence
    :param sentence: string with some words (e.g. "angry cats")
    :param norm_entity: entity in normal form (e.g. "cat")
    :return: entity as a string or None
    """
    # ... convert sentence to word set ...
    norm_sent_words = [hp.get_lemma(w) for w in hp.get_words(sentence)]
    norm_sent_ngrams = hp.get_ngrams(norm_sent_words, 2)
    norm_sent_ngrams.extend(norm_sent_words)  # to not loose single words
    sentence_set = set(norm_sent_ngrams)
    if norm_entity in sentence_set:
        sent_entity = get_entity_from_sentence(sentence, norm_entity)
        if sent_entity is not None:
            sent_entity = sent_entity.replace("ё", "е")
            return sent_entity
    return None


def get_key(dict, val):
    """
    Return key by value from dictionary.
    :param dict: dictionary
    :param val: its value
    :return: string OR None if key was not found
    """
    for key, val_lists in dict.items():
        # cat_words = [e for l in val_lists for e in l]
        cat_words = [l for l in val_lists]
        if val in cat_words:
            return key
    return None


def get_entities(categoryToEntitiesDict):
    """
    :param categoryToEntitiesDict: {category1: ["polar night", "polar day"]}
    :return: set{"polar night","polar day"}
    """
    entSet = set()
    for entities in categoryToEntitiesDict.values():
        for entity in entities:
            entSet.add(entity)
    return entSet


def form_raw_trainset(corpusReader:RussianPlainTextCorpusReader, categoryToEntitiesDict, out_dir, section_number):
    """
    Form raw trainset {sentence, sent_entity, category} as csv file for each section of text corpus.
    @param corpusReader: initialised corpusReader 
    @param categoryToEntitiesDict: {category1: ["entity one", ... "entity two"]}
    @param out_dir: 
    @param section_number: trainset will be formed for given section
    @return: path to result csv-file.
    """
    result_path = os.path.join(out_dir, "section_" + str(section_number) + ".csv")

    with open(result_path, "w", newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        entities = get_entities(categoryToEntitiesDict)

        for num, doc in enumerate(corpusReader.section_docs(section_number)):
            logger.info("=== Processing [{}] doc ===".format(num))
            # === Convert Doc to set of normalised words ...
            sentences = hp.sentences(doc)
            doc_set = set([hp.get_lemma(w) for sentence in sentences for w in hp.get_words(sentence)])
            # ... check if entity (any of its words) is in doc_set ...
            for entity in entities:
                norm_entity = hp.get_ngram_lemmas(entity)
                entity_word_set = set(hp.get_words(norm_entity))
                if entity_word_set.intersection(doc_set) is not None:
                    # ... if YES then find sentence with that entity in this doc
                    for sentence in hp.sentences(doc):
                        sent_entity = find_entity_inside_sentence(sentence, norm_entity)
                        if sentence is not None and sent_entity is not None:
                            category = get_key(categoryToEntitiesDict, entity)  # use not normalised entity
                            if category is not None:
                                writer.writerow([sentence, sent_entity, category])  # form result csv file
    return result_path


def form_raw_trainsets_for_sections(corpusReader: RussianPlainTextCorpusReader, category_to_entities_dict,
                                    out_dir, start_section_number=0, end_section_number=0):
    """
    Form raw trainset {sentence, sent_entity, category} as csv file for each section of text corpus.
    @param corpusReader:
    @param category_to_entities_dict:
    @param out_dir:
    @param start_section_number: trainset will be formed for given range of section
    @param end_section_number:  trainset will be formed for given range of section
    @return: path to dir with result csv-files.
    """
    # TODO make getter for corpusReader._sections
    end_section_number = len(corpusReader._sections)-1 if end_section_number < 1 else end_section_number
    if start_section_number > end_section_number:
        raise ValueError('start_section_number cant be more than end_section_number')

    for section in range(start_section_number, end_section_number):
        logger.info("=== Start processing section [{}] ===".format(section))
        form_raw_trainset(corpusReader, category_to_entities_dict, out_dir, section)
        logger.info("=== Finish processing section [{}] ===".format(section))
    return out_dir
