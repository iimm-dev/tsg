import os
import augmentator.helpers as hp
import spacy
import unittest

import setgen.final_trainset_generation as fg


class FinalTrainsetGenerationCase(unittest.TestCase):
    def setUp(self):
        return

    def tearDown(self):
        return

    def test_get_final_SPACY_trainsets(self):
        in_dir = "./data/test_border_csv"
        out_dir = "./data/out/shuffled_train_files"

        fg.get_final_SPACY_trainsets(in_dir, out_dir, row_quantity_per_file = 5, shuffle_rows=True)

        files = [f for f in os.listdir(out_dir) if os.path.isfile(os.path.join(out_dir, f))]
        assert len(files) == 5
