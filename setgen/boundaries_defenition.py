import csv
import os
import re
import string
import utils.file_utils as ut

# --- init logger ---
from text_corpora.utils.clogger import get_logger
logger = get_logger("./", "common")


def __get_corrected_russian_sentence(sentence):
    """
    Remove all special characters from sentence.
    """
    s = re.sub(r"[^А-Я-а-я-0-9,A-Z-a-z,\\,\/,\+,\-,\,,\.]+", ' ', sentence)
    return s


def __get_correct_entity(entity):
    """
    Remove spaces before punctuation marks
    :param entity: str
    :return: str
    """
    entity_list = list(entity)
    punct = [e for e in entity_list if e in string.punctuation]
    for item in punct:
        entity_list.pop(entity_list.index(item) - 1)
    return "".join(entity_list)


def __get_entity_boundaries(sentence, entity):
    indexes = re.finditer(entity, sentence)
    indexes = list(indexes)
    for match in indexes:
        # if there is a space before entity (separate entity) then its boundaries are returned <-- unclear why not just return match.span()
        if sentence[match.span()[0] - 1] == " ":
            return match.span()
        return match.span() #TODO without this line doesn't work for "Навальный заявил, что рассчитывает на успех Демократической коалиции в Новосибирской области." "Навальный"


def get_trainset_with_boundaries(input_dir, output_dir, output_dir_for_entity_with_wrong_boundaries) -> str:
    """
    For each csv-file with {sentence, sent_entity, category} create cvs-file with entity boundaries:
    {sentence, category, left_border, right_border}
    @param input_dir: dir with input csv-files
    @param output_dir: dir with output csv-files with borders
    @param output_dir_for_entity_with_wrong_boundaries: dir for entity_with_wrong_boundaries.csv log-file with wrong borders
    @return: dir with output csv-files with borders
    """
    ut.recreate_dirs([output_dir, output_dir_for_entity_with_wrong_boundaries])
    filenames = os.listdir(input_dir)
    wrong_boundaries_quantity = 0
    good_boundaries_quantity = 0

    if not os.path.exists(output_dir_for_entity_with_wrong_boundaries):
        os.makedirs(output_dir_for_entity_with_wrong_boundaries)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    wrong_boundaries_csv_file = open(
        os.path.join(output_dir_for_entity_with_wrong_boundaries, "entity_with_wrong_boundaries.csv"), "w")
    for file_name in filenames: #TODO consider to use utils.file_utils._rows_from_csv_files(...) generator
        logger.info("=== Processing file - [{}] ===".format(file_name))
        csv_path = os.path.join(input_dir, file_name)
        new_csv_path = os.path.join(output_dir, file_name)

        with open(csv_path, "r") as fr:
            with open(new_csv_path, "w") as fw:
                writer = csv.writer(fw, delimiter=',')
                error_writer = csv.writer(wrong_boundaries_csv_file, delimiter=',')
                for row in csv.reader(fr):  # row = [sentence, entity, category]
                    sentence = __get_corrected_russian_sentence(row[0])
                    entity = __get_correct_entity(row[1])
                    row[0] = sentence

                    # === try to get boundaries ===
                    try:
                        start_entity_boundary, end_entity_boundary = __get_entity_boundaries(sentence, entity)
                    except Exception:
                        error_writer.writerow(row)
                        wrong_boundaries_quantity += 1
                        logger.warn("Skip row - wrong_entity_boundaries quantity:{}".format(wrong_boundaries_quantity))
                        continue

                    good_boundaries_quantity += 1
                    row.append(start_entity_boundary)
                    row.append(end_entity_boundary)
                    writer.writerow(row)  # row = [sentence, entity, category, start_boundary, end_boundary]
    logger.info("=== Processed finished ===")
    logger.info("Total wrong_entity_boundaries quantity:{}".format(wrong_boundaries_quantity))
    logger.info("Total good_entity_boundaries quantity:{}".format(good_boundaries_quantity))
    return output_dir
