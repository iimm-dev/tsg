Ward Hunt Island Observatory Research Station
The Ward Hunt Island Observatory research station is owned and run by CEN in collaboration with Parks
Canada. Scientists have been working at the station since the 1950s. Access is from late May to mid August
(contact the Park Manager in advance to confirm opening and closing dates). Parks Canada has three
Weatherhaven shelters with oil burner furnaces, each can sleep twelve people. CEN operates a laboratory
made of insulated fiberglass and powered by solar panels since 2010. Three automated climate stations of
SILA Network in the region, and these are in operation year-round.

Name
Primary Contact
Secondary Contact

Nunavut Field Unit (for
logistics)
Dr. Warwick Vincent (for
information regarding
research and facilities)

Email

Phone Number

nunavut.info@pc.gc.ca

(867) 975-4673

warwick.vincent@bio.ulaval.ca

(418) 656-3340

Owner
Centre d’études nordiques (CEN)/ Centre for Northern Studies
Membership
Regular Member
Website
www.cen.ulaval.ca/en/page.aspx?lien=stationwardhunt
Latitude
83.068144
Longitude
-74.213103
Location
Ward Hunt Island is located at the most northern tip of Canada, off the coast of northern Ellesmere Island
and is part of Quttinirpaaq National Park, Nunavut, Canada (74° 10' W, 83° 6' N). Quttinirpaaq means "top
of the world” in Inuktitut and reflects this station's location, situated about 750 km from the North Pole.
Nearest Community
Grise Fiord (Aujuittuq)

Territory/ Province
Nunavut
Aboriginal Government/ Homeland
www.gov.nu.ca
Facility Type
Seasonally-Operated Field Camp
Research Hinterland
Atmosphere, Continuous Permafrost, Glacier, Lake, Polar Desert
Main Research Disciplines
Biochemistry, Climatology, Environmental Sciences, Geology and Sedimentology, Geophysics,
Geocryology, Geomorphology, Glaciology, Hydrology, Isotopic Chemistry, Limnology, Microbiology,
Terrestrial Biology/Ecology
Research History
Scientists have been working at the station since the 1950s. Structure and functioning of lake and river
ecosystems at high latitudes; dynamics of northern ice shelves; microbial ecology; geomorphology of polar
desert landscapes; impacts of UV radiation and climate change on aquatic ecosystems. Extensive climate
data records are available at www.cen.ulaval.ca/nordicanad and upon request: cen@cen.ulaval.ca. For
requests concerning ecological monitoring data, please contact Warwick Vincent.
Current Projects
Structure and functioning of lake and river ecosystems at high latitudes; geomorphology (permafrost);
dynamics of northern ice shelves; cyanobacteria ecology; impacts of UV radiation and climate change on
aquatic ecosystems.
Power
Generator, Solar, Wind (all structures are powered by solar panels which are inverted to 110 AC)
Communications
Satellite phone
Local Transportation
Zodiac, helicopter
Equipment Storage
N/A
Dormitory/Sleeping Facilities
A total of 3 rooms (8 beds) are available. Shelters include 1 living area, 1 kitchen, 1 laboratory. No staff
member is present at the station. Can accommodate 8 to 9 visitors at the time.
Dining/Kitchen Facilities
1 kitchen
Laboratory Facilities
Dry lab
Fuel Availability
Both Parks Canada and Polar Continental Shelf Program maintain fuel caches for operational needs only,
except in the case of an emergency. Arrangements for fuel can be made through PCSP, but may require
additional permitting by Parks Canada. Please see www.polar.nrcan.gc.ca for more details.

Research Requirements
Contact the Research Coordinator for the Nunavut Field Unit of Parks Canada at (867) 975-4762 or
Nunavut.Research@pc.gc.ca
Special Rules and Regulations
Contact the Research Coordinator for the Nunavut Field Unit of Parks Canada at (867) 975-4762 or
Nunavut.Research@pc.gc.ca
Local External Resources
N/A
Nearest Medical Service
Health Center in Grise Fiord and Hospital in Iqaluit (2615 km, via Resolute)
Safety Considerations
A high degree of self-sufficiency is required.
Cost
www.cen.ulaval.ca/en/page.aspx?lien=stationwardhunt#reservation
Other Information
This island in the High Arctic is 6.5 km long, east to west, and 3.3 km wide. The climate regime is typical
of polar deserts, with dry and extremely cold temperatures (annual mean temperature of -17.3çC). The
natural environment features lakes, ice shelves, fjords, epishelf lakes, ice caps and glaciers, sea ice,
mountains, and valleys. The desert terrain has a low plant and animal diversity, but the region contains
diverse microbial communities such as cyanobacterial mats that survive in these extreme environments. An
overview of past studies in this region is given in: Vincent, W.F.et al. 2011. Ecoscience 18: 236-261.
The first known sighting was in 1876 by Pelham Aldrich, a lieutenant with the George Nares expedition,
and named for George Ward Hunt, First Lord of the Admiralty (1874-1877). Ward Hunt Island was briefly
used as a weather station during the International Geophysical Year of 1957-58, and since then it has been
used as the starting point for a number of attempts to reach the North Pole, beginning with Ralph Plaisted
in 1968. No communities live on Ward Hunt Island. The nearest community is Grise Fiord, located 800 km
away on southern Ellesmere Island. Grise Fiord, (Inuktitut: Aujuittuq, "place that never thaws") is a small
Inuit hamlet in the Qikiqtaaluk Region in the territory of Nunavut, Canada. With a population of 141
residents (as of the Canada 2006 Census), it is the only Inuit community on Ellesmere Island. It is also one
of the coldest inhabited places in the world, with an average yearly temperature of -16.5çC. Grise Fiord lies
1,160 km (720 mi) north of the Arctic Circle and lies in the Arctic Cordillera mountain range which is the
only major mountain system east of the Canadian Rockies. The Canadian military base Alert lies 170 km to
the east and slightly to the south of Ward Hunt Island.
Given this is an extremely isolated station in a national park, all research activities must be planned and
proposed at least one year in advance. Contact CEN (cen@cen.ulaval.ca) for more information. For
information on access and permits, contact Quttinirpaaq Park Manager (www.pc.gc.ca/pnnp/nu/quttinirpaaq/plan.aspx) and the Polar Continental Shelf Project (PCSP) for appropriate application
forms to access the site via chartered flights (polar.nrcan.gc.ca).
Last Updated
2015-05-30

