Север России
в военно-морском
и экономическом отношениях

ТРУДЫ
научно-исследовательского отдела
Института военной истории
Том 6
Книга 2

Brev om Norden
Nordområdet av landet vårt tiltrekker seg stadig mer allmenn oppmekrsomhet: på den
ene siden er det – av ulike grunner – av stor interesse både for samfunnet vårt, mediene
og forskjellige forskningsinstitusjoner, noe som selvsagt er veldig fint, og det er å
ønske at denne interessen bare fortsetter å øke. På den andre siden har det russiske
nordområdet vakt våre naboers – engelskmenns, tyskeres, nordmenns – interesse, og
de har begynt å utforske det med en iver som tyder på at det ikke bare handler om
vitenskapelige interesser, men også om å få praktisk fordel av vår eiendom, noe som
de herrer utlendingene er mestere i.
Det er denne siste omstendigheten som får meg til – så nøye som mulig – å forsøke å
gjøre rede for spørsmålet om det russiske nordområdet, slik at man kan diskutere om vi
skal overlate den arven vi har fått fra våre fedre og forfedre, til skjebnens og
utlendingenes vilkårlighet, slik som det ofte er tilfelle i dag, eller om vi, før det er for
sent, skal ta oss selv kraftig sammen, si til utlendingene "Hands off!" og begynne å
utnytte fornuftig ressursene i det enorme området og forsøke å gjøre det til det russiske
veldets bolverk ved havet, noe nordområdet har alle ressurser til.

Breve om Norden
Vores Norden tiltrækker sig i stadig stigende grad almen interesse: på denne ene side
er vores samfund, presse og diverse videnskabelige institutioner – af forskellige grunde
– interesseret i Norden, hvilket naturligvis er udmærket, og det er ønskeligt, at denne
interesse heller ikke i fremtiden bliver mindre; på den anden side har vores naboer –
englændere, tyskere og nordmænd – fattet interesse for vores Norden, og de er begyndt
at undersøge den med stor ihærdighed, hvilket viser, at de ikke udelukkende nærer
videnskabelige interesser, men også ønsker at udnytte området i praktisk og
egennyttigt øjemed, hvilket de herrer udlændinge er store mestre i.
Netop denne sidste omstændighed ansporer mig til at forsøge at klarlægge problemet
med vores Norden så detaljeret som muligt, så man vil kunne bedømme, om vi skal
overlade denne vor arv, som vi fra gammel tid har fra vore forfædre og fædre, til
skæbnens og udlændingenes vilkårlighed, som det er tilfældet i dag, – eller om vi skal
tænke selv, mens tid er, og efter at have sagt Hands off! til udlændingene begynde på
rette vis at udnytte rigdommene i disse enorme områder og bestræbe os på gøre dem til
Ruslands bolværk ved havet, hvortil der er alle ressourcer i Norden.

Научно-исследовательский отдел
(военной истории Северо-западного региона РФ)
Научно-исследовательского института (военной истории)
Военной академии Генерального штаба ВС РФ
191055, Санкт-Петербург, Дворцовая пл., д. 10
Тел.: (812)-494-24-86
himhistory@yandex.ru

