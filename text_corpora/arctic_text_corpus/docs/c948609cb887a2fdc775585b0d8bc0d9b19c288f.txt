ЭКОНОМИКА И ФИНАНСЫ
ПРОИСШЕСТВИЯ

ПОЛИТИКА

ОФИЦИАЛЬНАЯ ХРОНИКА

ЗДОРОВЬЕ И СРЕДА

HI-TECH

АКТУАЛЬНАЯ УРБАНИСТИКА ТАТАРСТАНА
ХРОНИКА
БИЗНЕС

ОБЩЕСТВО
ЛЮДИ

КУЛЬТУРА

СПОРТ

ЖИЗНЬ

ПРОИСШЕСТВИЯ

HI-TECH

УРБАНИСТИКА ТАТАРСТАНА
ОБЩЕСТВО

ЗДОРОВЬЕ И СРЕДА

ПОЛИТИКА

КУЛЬТУРА

ПРЕСС-РЕЛИЗЫ

ЗДОРОВЬЕ И СРЕДА

АКТУАЛЬНАЯ УРБАНИСТИКА ТАТАРСТАНА
TATAR-INFORM.RU

HI-TECH

УРБАНИСТИКА ТАТАРСТАНА

СЕЛЬСКОЕ ХОЗЯЙСТВО

И ОБРАЗОВАНИЕ

ДУХОВНАЯ ЖИЗНЬ

HI-TECH

КУЛЬТУРА

СПОРТ

ПРЕСС-РЕЛИЗЫ

HI-TECH

СОБЫТ

СЕЛЬСКОЕ ХОЗЯЙСТВ

НАУКА И ОБРАЗОВАНИЕ

ДУХОВНАЯ

ИНТЕРТАТ

ЭКОНОМИКА И ФИНАН

ФАКТЫ

КУЛЬТУРА

СПОРТ

ДУХОВНАЯ ЖИЗНЬ

ШОУ-БИЗНЕС

ПОЛИТИКА

HI-TECH

ШОУ-БИЗНЕС

СОБЫТИЯ НЕДЕЛИ

ИНТ

СОБЫТИЯ

КУЛ

НАУКА И ОБРАЗОВАНИЕ

ДУХОВНАЯ ЖИЗНЬ

ЭКОНОМИКА И ФИНАНСЫ

СЕЛЬСКОЕ ХОЗЯЙСТВО

КУЛЬТУРА

ДУХОВНАЯ ЖИЗНЬ

ЭКОНОМИКА И ФИНАНСЫ
ПРОИСШЕСТВИЯ

АКТУАЛЬНАЯ

ОФИЦИАЛЬНАЯ ХРОН

СЕЛЬСКОЕ ХОЗЯЙСТВО

ИНТЕРТАТ

ПРОИСШЕСТ

ПРЕСС-РЕЛИЗЫ

ЗДОРОВЬЕ И СРЕДА

ОБЩЕСТВО

СОБЫТИЯ НЕДЕЛИ

ИНТЕРТАТ

ОФИЦИ

СОБЫТИЯ НЕДЕЛИ

НАУКА И ОБРАЗОВАНИЕ

СОБЫТИЯ НЕДЕЛИ

ШОУ-БИЗНЕС

АКТУАЛЬНАЯ УРБАНИСТИКА ТАТАРСТАНА

ОБЩЕСТВО

ШОУ-БИЗНЕС

ОБЩЕСТВО

ЭКОНОМИКА И ФИНАНСЫ

ОФИЦИАЛЬНАЯ ХРОНИКА

РЕЛИЗЫ

ОБЩЕСТВО

ОФИЦИАЛЬНАЯ ХРОНИКА
HI-TECH

ПОЛИТИКА

ЗДОРОВЬЕ И СРЕДА

СПО

ПРЕСС-РЕЛ

АКТУАЛЬНАЯ УРБАНИСТИКА ТАТАРСТАНА

СПОРТ ПРОИСШЕСТВИЯ

ПРОИСШЕСТВИЯ

ОФИЦИАЛЬНАЯ ХРОНИКА

ПРОИСШЕСТВИЯ

КУЛЬТУРА

ДУХОВНАЯ ЖИЗНЬ

ЭКОНОМИКА И ФИНАНСЫ

СЕЛЬСКОЕ ХОЗЯЙСТВО

ИНТЕРТАТ

СПОРТ

ЗДОРОВЬЕ И СРЕДА

СПОРТ

НАУКА И ОБРАЗОВАНИЕ

СОБЫТИЯ НЕДЕЛИ

ДУХОВНАЯ ЖИЗНЬ

ЭКОНОМИКА И ФИНАНСЫ

ОБЩЕСТВО

ШОУ-БИЗНЕС

СЕЛЬСКОЕ ХОЗЯЙСТВО

И ОБРАЗОВАНИЕ

ИНТЕРТАТ

ПРЕСС-РЕЛИЗЫ

ПОЛИТИКА

СЕЛЬСКОЕ ХОЗЯЙСТВО

НАУКА И ОБРАЗОВАНИЕ

АКТУАЛЬНАЯ УРБАНИСТИКА ТАТАРСТАНА

ОФИЦИАЛЬНАЯ ХРОНИКА

ЗДОРОВЬЕ И СРЕДА

КУЛЬТУРА

ДУХОВНАЯ ЖИЗНЬ

ЭКОНОМИКА И ФИНАНСЫ

ПРЕСС-РЕЛИЗЫ

ПОЛИТИКА

СОБЫТИЯ НЕДЕЛИ

СЕЛЬСКОЕ ХОЗЯЙСТВО

НАУКА И ОБРАЗОВАНИЕ

ИНТЕРТАТ

ШОУ-БИЗНЕС

ОБЩЕСТВО

ПОЛИТИКА

СПОРТ ПРОИСШЕСТВИЯ

ПРЕСС-РЕЛИЗЫ

ПОЛИТИКА

ЗДОРОВЬЕ И СРЕДА

АКТУАЛЬНАЯ УРБАНИСТИКА ТАТАРСТАНА

П

АКТУАЛЬНАЯ

ОФИЦИАЛЬНАЯ ХРОН
HI-TECH

ШОУ-БИЗНЕ

СОБЫТИЯ НЕДЕЛИ

ИНТ

НАША ОСНОВНАЯ МИССИЯ

НАША МИССИЯ
ПОЛНАЯ И ДОСТОВЕРНАЯ
ИНФОРМАЦИЯ
О РЕСПУБЛИКЕ ТАТАРСТАН

3

О НАС:

О НАС

ОПЕРАТИВНОСТЬ, ДОСТОВЕРНОСТЬ

наши главные преимущества

ЕЖЕДНЕВНО БОЛЕЕ 250 НОВОСТЕЙ

на русском языке

ЕЖЕДНЕВНО БОЛЕЕ 60 НОВОСТЕЙ

на татарском языке
Лента новостей

ОБНОВЛЯЕТСЯ КАЖДЫЕ 7 - 10 МИНУТ
НАМ ДОВЕРЯЮТ

крупнейшие СМИ Татарстана, России, СНГ и дальнего зарубежья,
государственные органы власти, пресс-службы ведомств, крупнейшие
компании, предприятия республики и России

5

В СОЦИАЛЬНЫХ СЕТЯХ
FACEBOOK.COM/TATARINFORM
VK.COM/TATARINFORM
INSTAGRAM.COM/TATAR_INFORM
TWITTER.COM/TATAR_INFORM
OK.RU/TATARINFORM
@ti_news_bot
YOUTUBE.COM/USER/
TATARINFORM
FACEBOOK.COM/SNTATRU
VK.COM/SNTATRU
INSTAGRAM.COM/SNTATRU
INSTAGRAM.COM/SNTATRU
TWITTER.COM/SNTATRU

6

FACEBOOK.COM/TAT.TATARINFORM
VK.COM/TATARINFORMTAT
INSTAGRAM.COM/TATAR_INFORM_TATAR
TWITTER.COM/TAT_TATARINFORM
OK.RU/TATARINFORM.TATAR
@tatarinform_tat_bot
FACEBOOK.COM/INTERTAT
VK.COM/INTERTATGAZETA
INSTAGRAM.COM/INTERTAT.TATAR
TWITTER.COM/INTERTATOV
OK.RU/INTERTAT.TATAR

ОСНОВНЫЕ ФАКТЫ

ОСНОВНЫЕ ФАКТЫ

1 МИЛЛИОН

уникальных посетителей*

1-е МЕСТО СРЕДИ ПЕЧАТНЫХ СМИ ТАТАРСТАНА

по подписному тиражу: газеты «Атна вакыйгалары» и «События недели»**

1-е МЕСТО ПО ВЕРСИИ «МЕДИАЛОГИИ»

в рейтинге самых цитируемых СМИ Татарстана

1-е МЕСТО ПО ПОСЕЩАЕМОСТИ

среди сайтов на татарском языке: электронная газета «Интертат»***

* Данные «Яндекс.Каталог», 2018 г.
** Данные ФГУП «УПС «Татарстан Почтасы», I квартал 2018 г.
*** Данные LiveInternet, 2018 г.

8

СТРУКТУРА АГЕНТСТВА

СТРУКТУРА АГЕНТСТВА

БИЛЬДРЕДАКТОРЫ

ФАБРИКА НОВОСТЕЙ
ИА «ТАТАР-ИНФОРМ»

РЕДАКТОРЫ
ПЕЧАТНЫХ
ГАЗЕТ

IT-ОТДЕЛ
SMMОТДЕЛ
ОТДЕЛ
ПРОДАКШН

ЖУРНАЛИСТЫ
РЕДАКТОРЫ
НОВОСТНОЙ
ЛЕНТЫ
ОТДЕЛ
ПРОДАЖ

10

СТРУКТУРА АГЕНТСТВА

РЕДАКЦИЯ

НОВОСТИ
ФОТО, ВИДЕО
ИНФОГРАФИКА
МУЛЬТИМЕДИЙНЫЕ СТАТЬИ ДЛЯ ЭЛЕКТРОННОЙ ГАЗЕТЫ «СОБЫТИЯ»
СТАТЬИ В ПЕЧАТНОЙ ГАЗЕТЕ «СОБЫТИЯ НЕДЕЛИ»
ВИДЕОИНТЕРВЬЮ ДЛЯ САЙТА «СОБЫТИЯ»
НОВОСТИ И ВИДЕО ДЛЯ СОЦИАЛЬНЫХ СЕТЕЙ
11

СТРУКТУРА АГЕНТСТВА

PR-ОТДЕЛ

ОНЛАЙН-ТРАНСЛЯЦИИ
МЕРОПРИЯТИЙ
ФОТО- И ВИДЕОРЕПОРТАЖИ
РОЛИКИ ДЛЯ СОЦИАЛЬНЫХ СЕТЕЙ
ВИДЕОИНФОГРАФИКА
ЛОНГРИДЫ ДЛЯ СПЕЦИАЛЬНЫХ ПРОЕКТОВ
GIF-БАННЕРЫ

12

СТРУКТУРА АГЕНТСТВА

ПРЕСС-ЦЕНТР

ПРЕСС-КОНФЕРЕНЦИИ
БРИФИНГИ
ИНТЕРВЬЮ
СЕМИНАРЫ
ПРОВЕДЕНИЕ ВИДЕОМОСТОВ

13

НАШИ ВОЗМОЖНОСТИ

НАШИ ВОЗМОЖНОСТИ

ТАТАR-INFORM.RU
РУБРИКИ
ГЛАВНЫЕ
НОВОСТИ
ДНЯ

АКТУАЛЬНЫЕ
НОВОСТИ ДНЯ

ЛЕНТА
НОВОСТЕЙ
15

ИНТЕРВЬЮ

ОБЗОРНЫЕ
СТАТЬИ,
ИНФОТЕЙНМЕНТ

НАШИ ВОЗМОЖНОСТИ

ТАТАR-INFORM.TATAR
РУБРИКИ

ИНТЕРВЬЮ
ГЛАВНЫЕ
НОВОСТИ
ДНЯ

ЛЕНТА
НОВОСТЕЙ

ОБЗОРНЫЕ
СТАТЬИ,
ИНФОТЕЙНМЕНТ
16

НАШИ ВОЗМОЖНОСТИ

ПРЕСС-КОНФЕРЕНЦИИ

ежедневно в режиме онлайн
Площадка, которая позволяет донести информацию до всех СМИ Татарстана

17

НАШИ ВОЗМОЖНОСТИ

ПРЕСС-КОНФЕРЕНЦИИ
Аккредитация, анонсирование, размещение итоговых материалов

18

НАШИ ВОЗМОЖНОСТИ

ВИДЕОИНТЕРВЬЮ

19

НАШИ ВОЗМОЖНОСТИ

ИНТЕРВЬЮ

на главной странице

20

НАШИ ВОЗМОЖНОСТИ

ФОТОРЕПОРТАЖ

21

ВЫ МОЖЕТЕ С НАМИ СВЯЗАТЬСЯ

КОНТАКТЫ
420066 г. Казань, ул. Декабристов, 2

ОТДЕЛ ПРОДАЖ

Телефон

НАИЛЬ СЕЛИВАНОВ

(843) 222-09-99
Коммерческая служба:
(843) 222-09-98 (вн. 1340)

СВЕТЛАНА ЯБЛОНСКАЯ

info@tatar-inform.ru
advert@tatar-inform.ru

ДИНА ИЛЬЯСОВА

Генеральный директор

ЕЛЕНА ВОЛКОВА

Ш. М. САДЫКОВ

Коммерческий директор

Д. А. ХАЛИМОВА

22

+7 917 906-26-84

+7 960 050-08-02
+7 917 885-28-05
+7 917 885-09-79

