Conference on the Marquis de Traversay and Franco-American-Russian
naval relations

March 27th: Service historique de la Défense, Pavillon du Roi, Château de Vincennes,
avenue de Paris, Vincennes
Preamble:
13. 45: Welcome address by the director of the Historical Defense Service, P. Laugeay
13. 48: Response by the director of the Polar Sciences Public Academy, V. Mitko, initiator of the
Traversay project
13. 51: Response by the Ernest J King Professor Emeritus of Maritime History and Senior Mentor,
Hattendorf Historical Center, US Naval War College, J. Hattendorf
13. 54: Welcome coffee
Introduction (moderator: M.P. Rey):
14. 15 – 14. 45: Franco-American-Russian naval relations between two wars of independence, two
oceans and two poles: A. Sheldon-Duplaix
14. 45 – 15. 15: Genealogy and historiography of Traversay: M. du Chateney
15. 15 – 15. 45: questions / discussion
15. 45 – 16. 15: coffee break
Traversay in the service of France and the Independence of the United States (moderator: H.
Drévillon):
16. 15 – 16. 45: the Marquis de Traversay and naval operations in American waters, 1778-1783, J.
Hattendorf

1

16. 45 – 17. 15: A promising and lost generation: Traversay among young officers in the American
War of Independence: O. Chaline
17. 30 – 18. 00: questions / discussion
Traversay in the Russian service, 1st Part (moderator: M. du Chateney)
18. 00 – 18.30: The Marquis de Traversay at Rochensalme (1795-1802), G. Vangonen
18. 45: departure for the restaurant (upon invitation)

March 28th: Maison de la Recherche de l'Université, Paris-Sorbonne, salle D035, 28
rue Serpente - Paris
Traversay in the Russian service, 2nd Part (moderator: M. du Chateney)
9. 00 – 9.30: Admiral Traversay, Russia’s Marine Minister (1809-27), M. Minina
9.30 – 10.00: Questions / discussion
Traversay’s personality and legacy (moderator: J. Hattendorf)
10.00 – 10.30: Traversay's contribution to Russian successes and international exchanges, V. Mitko
10.30 – 11.00: Was Traversay a mason? A. Terukov
11.00 – 11.30: Traversay and the maritime expeditions, M. du Chateney
11.30 – 12.00: Discussion
Conclusions
12.00 – 12.10: V. Mitko
12.10- 12.20: J. Hattendorf
12.30: Farewell buffet (upon invitation)

2

