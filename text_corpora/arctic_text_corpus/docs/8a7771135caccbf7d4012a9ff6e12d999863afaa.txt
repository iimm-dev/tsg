АРКТИЧЕСКИЕ ТУНДРЫ ТАЙМЫРА И ОСТРОВОВ КАРСКОГО МОРЯ

Том II

1994

УДК 571.511

ИСТОРИЯ ОТКРЫТИЯ И ИССЛЕДОВАНИЯ АРХИПЕЛАГА ИЗВЕСТИЙ ЦИК И
ОСТРОВА СВЕРДРУП
Ф.А. Романенко
Вскоре после своего открытия небольшие острова центральной части Карского моря
были почти забыты, и до сих пор многие из них изучены в географическом отношении
совершенно недостаточно. При обработке полевых материалов были составлены краткие
очерки по истории исследования этих островов, чтобы вспомнить полузабытые имена
первооткрывателей и исследователей.
Остров Свердруп был открыт Ф. Нансеном с борта «Фрама» 18 августа 1893 г. и
назван в честь капитана судна Отто Свердрупа (1854-1930), одного из самых известных
полярных судоводителей. Первая высадка на остров произошла 8 августа 1932 г., когда
небольшая группа моряков и ученых с судов «Сибиряков» и «Русанов» обследовала его и
составила карту. В 1930-40-х гг. здесь неоднократно бывали гидрографы. В 1970-х гг. на
острове работало две экспедиции, построившие стационарные жилые поселки. Сейчас эти
постройки брошены и медленно разрушаются.
Архипелаг Известий ЦИК был открыт в 1932-33 гг. советскими экспедициями на
пароходе «Сибиряков». 14 августа 1933 г. небольшая группа специалистов впервые
высадилась на остров Тройной и провела там комплекс наблюдений. Впоследствии его
неоднократно посещали гидрографы. В 1953 г. на острове Тройном открылась полярная
станция, действующая и поныне. Зимовщиков возглавил Ю.Т. Плеханов. В 1957 г. на
архипелаге была проведена геологическая съемка. В 1962 г. станцию перенесли на ее
нынешнее место. Сейчас из-за трудностей с финансированием и оттока людей значительно
сокращена программа наблюдений. Станция не закрывается только благодаря
самоотверженности ее начальника А.М. Бабко и других зимовщиков.

Небольшие острова центральной части Карского моря, лежащие вдали от
таймырского побережья, были нанесены на географическую карту совсем недавно, в
конце XIX - начале XX вв. Вскоре после своего открытия они были почти забыты, и до
сих пор многие из них изучены в географическом отношении совершенно недостаточно. В
связи с проектированием Большого Арктического заповедника некоторые острова были
обследованы Арктической экспедицией Института эволюционной морфологии и экологии
животных РАН (ИЭМЭЖ РАН). Летом 1992 года небольшой отряд Арктической
экспедиции под руководством Е.Е. Сыроечковского-младшего работал на островах
Тройном (архипелаг Известий ЦИК) и Свердруп. При обработке полевых материалов нам
показалось интересным составить краткие очерки по истории исследования этих островов,
чтобы вспомнить полузабытые имена первооткрывателей и исследователей.
Хронологически первым был открыт остров Свердруп. Это произошло 18 августа
1893 года во время знаменитого плавания Фритьофа Нансена на «Фраме». Норвежцы на
остров не высаживались и координаты его определили довольно приблизительно.
Новооткрытая земля была названа в честь капитана «Фрама» Отто Свердрупа (1854-1930),
одного из самых известных полярных судоводителей [Попов, Троицкий, 1972].

В сентябре 1915 года в 3-4 милях к востоку от острова села на песчаную банку
шхуна «Эклипс» под командованием того же О. Свердрупа. Корабль с норвежской
командой был нанят русским правительством для поисков пропавших в 1912 году
отечественных экспедиций В.А. Русанова на «Геркулесе» и Г.Л. Брусилова на «Святой
Анне». На борту находился представитель правительства доктор И.И. Тржемесский. С
большим трудом экипажу шхуны удалось снять ее с мели.
Летом 1930 года над юго-восточной частью Карского моря летал один из первых
ледовых разведчиков пилот И.К. Иванов на самолете «Комсеверпуть-2». Во время одного
из полетов летчик-наблюдатель В. Вердеревский зарисовал очертания острова Свердруп с
высоты 15 метров [Визе, 1934]. В следующем году над ним проходили маршруты ледовой
разведки самолета «Комсеверпуть-3». 29 июля и 8 августа 1931 года находившийся на
борту самолета гидрограф С.С. Лаппо тоже выполнил несколько зарисовок контуров
острова [Лаппо, 1932]. Наблюдатель пришел к выводу, что остров Свердруп сложен
илисто-песчаными отложениями Обь-Енисейского течения за счет взаимодействия
водных потоков нескольких направлений. Глубина лагуны, названной лагуной Самолета,
по его мнению, была не более 2-3 футов.
8 августа 1932 года к берегам острова Свердруп подошли сразу два ледокольных
парохода - «Сибиряков» и «Русанов». Они ожидали доставки угля в порту Диксона и
воспользовались этим временем для исследования еще никем не посещенной земли
[Гаккель, 1933]. Сибиряковцы Я.Я. Гаккель и И.Л. Русинова провели астрономические и
магнитные наблюдения, точно определив координаты острова и построив астропункт.
В.И. Влодавец занимался геологией. Русановцы Н.Н. Колчин и Б.М. Михайлов выполнили
глазомерную съемку западной части острова в масштабе 1:10 000. Затем Я.Я. Гаккель свел
все съемки в одну и составил первую карту острова. По мнению В.И. Влодавца [1933],
остров обязан своим образованием движению ледника из района побережья между устьем
реки Пясины и Диксоном. Были убиты два белых медведя и найдено много следов песцов
и гусей.
Дальнейшие исследования острова Свердруп связаны с гидрографами. В 1933 году
здесь работало гидрографическое судно «Циркуль». В 1936 году гидрографы поставили в
южной части острова (рис. 1) небольшой деревянный столбик с надписью на жестяной
расплющенной консервной банке. К сожалению, часть ее проржавела и недоступна для
прочтения. В 1940 г. на острове работали гидрографы с судна «Полярник», поставившие
высокий деревянный столб с его именем. Гидрографами был построен маяк и несколько
навигационных знаков. Впоследствии вместо деревянного маяка, от которого сейчас
осталась груда бревен, был построен высокий современный маяк.
В середине 1970-х годов на острове появились люди. В его южной части
гидрографы построили радионавигационную станцию (РНС), работавшую до начала или
середины следующего десятилетия. На юго-западном берегу примерно в это же время
была построена крупная зимовочная база экспедиции АНРП (расшифровка нам
неизвестна). База состояла из деревянного одноэтажного барака на 20-25 комнат с
пищеблоком и спортзалом, большой кирпичной бани и нескольких мелких деревянных
построек. Местность вокруг сильно захламлена бочками, бытовым мусором, кусками
дерева и железа. В русле небольшой речки построена высокая плотина, но следов
существования вод хранилища, хотя бы и краткое время, нами не обнаружено. Геодезисты
экспедиции построили несколько геодезических деревянных знаков-пирамид, большая
часть которых сейчас повалена ветром.
Обе группы построек - гидрографическая и геологическая - брошены, забиты
снегом и постепенно разрушаются. В одном из более или менее уцелевших балков
гидрографов в период с 27 июля по 7 августа 1992 года размещался отряд Арктической
экспедиции ИЭМЭЖ РАН в составе орнитологов Е.Е. Сыроечковского-младшего и
Е.Г. Лаппо (Россия), А.К. Прево и Р. Жуллиара (Франция), ботаника Ю.П. Кожевникова и

географа-геоморфолога Ф.А. Романенко. Были проведены комплексные географические
наблюдения, основные результаты которых изложены в других статьях данного сборника.

Рассмотрим теперь историю открытия архипелага Известий ЦИК.
После высадки 8 августа 1932 года на остров Свердруп ледокольные пароходы
«Сибиряков» и «Русанов» направились далее на северо-восток. 12 августа с обоих судов,

шедших разными курсами, был замечен небольшой низменный остров, получивший
название острова Сидорова (из архипелага Арктического Института). А еще через
несколько дней с «Русанова», на котором находились участники экспедиции под
руководством P.Л. Самойловича, увидели три небольших островка, самому крупному из
которых присвоили имя Н.И. Бухарина. Точные его координаты определить не удалось.
На следующий год исследованиями юго-восточной части Карского моря
занималась экспедиция на «Сибирякове» под руководством В.Ю. Визе (капитан
Ю.К. Хлебников). 14 августа 1933 года судно встало на якорь у юго-восточного
побережья острова Бухарина. Во время двух высадок на берег маленькая группа
специалистов провела небольшой комплекс наблюдений. Д.А. Мохнач определил
координаты острова, был построен астропункт. Б.И. Данилов составил глазомерную карту
участка побережья. Г.Д. Адлер описал несколько обнажений коренных пород - сланцев и
роговиков, пронизанных многочисленными кварцевыми жилами [Адлер, Уль, 1936]. Они
напомнили ему палеозойские отложения полуострова Челюскина. Выяснилось, что
архипелаг состоит не из трех, а из шести островов. Ему было присвоено имя Известий
ЦИК в честь газеты «Известия», а один из островов назван в честь капитана судна
островом Хлебникова [Лесс, 1963; Попов, Троицкий, 1972]. 15 августа «Сибиряков»
покинул пределы архипелага, направившись в Архангельск.
30 августа 1933 года к острову Бухарина подошла шхуна «Белуха» под
командованием М. Сергеева, входившая в Западно-Таймырскую гидрографическую
экспедицию. Гидрографы произвели рекогносцировочную морскую и топографическую
съемки, построили два навигационных знака. Второй по величине остров архипелага
получил сразу два имени, так как был принят за два участка суши. Один получил название
острова Сергеева в честь командира «Белухи», другой был назван по имени работника
НКВД И.М. Гронского (1894-1985). На картах появились также названия пролив Белухи,
остров Гаврилина (в честь старшего помощника капитана), мыс Эверлинга (в честь
участника экспедиции астронома А.В. Эверлинга) [Попов, Троицкий, 1972]. Экспедиция
находилась у берегов архипелага до 3 сентября, после чего направилась к острову
Уединения.
В 1937 и 1939 гг. острова Известий ЦИК посетили гидрографические экспедиции
на моторных ботах «Мурманец» (капитан И.Н. Ульянов) и «Полярник» (капитан
Г.Ф. Сулаков, начальник экспедиции Г.С. Крутов). Часть географических названий была
изменена. Мыс Эверлинга превратился в мыс Западный, остров Гройского - в остров
Пологий. В 1940 г. после экспедиции на судне «Полярник» изменил свое название остров
Бухарина, названный топографом К.М. Овчаренко Тройным (остров состоит из трех
куполообразных массивов, сложенных коренными породами, которые соединены низкими
галечными террасами и косами). В сентябре 1940 г. «Полярник» входил в бухту,
названную и его честь (рис. 2), и выяснил, что глубина ее позволяет подходить крупным
судам близко к берегу. В этом же году на острове Тройном высаживались участники
гидрографической экспедиция под руководством В.И. Воробьева на ледокольном
пароходе «Г. Седов», оставившие на деревянном столбе вырезанную надпись «МП
Мурманск Седов 1940».
В 1941 году на необследованную каменистую банку на северо-востоке от
архипелага натолкнулся ледокольный пароход «Садко». 13 сентября, продержавшись на
воде почти двое суток, судно затонуло. Экипаж был спасен ледоколом «Ленин» [Белов,
1969].
В октябре 1945 г. на ледоколе на острова была доставлена экспедиция Главного
Управления Северного морского пути (ГУСМП), которая установила две автоматические
метеорологические станция «Порфир», сконструированные в Москве под руководством
Б.Н. Коноплева [Горелейченко, 1972]. Одна из этих станций проработала около полугода,
другая вскоре вышла из строя. В следующем году была установлена автоматическая

метеостанция, проработавшая около двух месяцев. Обслуживание их производилось
экспедицией 1947 г., в которой участвовал, видимо, Я.Я. Гаккель [Дибнер, Захаров, 1970].

Осенью 1952 года на юго-восточном берегу острова Тройного построили
зимовочную базу (два щитовых финских деревянных дома и склад из бревен) участники
гидрографической экспедиции № 6 И.Г. Аристова (1913-1972). Экспедиция работала здесь
до 15 сентября 1953 г. Летом съемка и промер осуществлялись с борта судна
«Исследователь». Аэровизуальным обследованием архипелага занимался А.Б. Винник,
наземным - С.С. Яновский. Пролив между островами Тройным и Пологий-Сергеева (так
стал называться остров, две части которого ранее назывались островами Пологий и
Сергеева) получил имя М.И. Калинина.
6 сентября 1953 г. на остров высадились с парохода «Мста» сотрудники
организованной в этом году полярной станции, разместившиеся на базе гидрографов.
Зимовщиков возглавлял работавший с 1950 года на Диксоне Юрий Тимофеевич Плеханов.

Первая зимовка состояла из шести человек, вторая (1954-55 гг.) - из десяти человек (табл.
1). На станции соблюдался режим секретности, полярникам запрещалось
фотографировать строения. Сразу стало ясно, что новая станция расположена в неудачном
месте: разгрузка судов-снабженцев затруднена, а мареограф все время ломает
торосящимся льдом. Ю.Т. Плеханов предложил перенести станцию в кутовую часть
бухты Полярника, достаточно глубокую для подхода морских судов и защищенную от
торошения.
Условия жизни на новой станции также оставляли желать лучшего. Домики были
не приспособлены для такого количества людей, печи были сложены плохо и температура
в жилых помещениях часто не поднималась до комфортных отметок. Не всегда хватало
продуктов - мяса, сахара, масла. Зимовщики получали бесплатный продуктовый паек по
рабочей норме военного времени. Летом было очень тяжело обслуживать самолеты
ледовой разведки сводками погоды - каждый час или чаще. Почта приходила редко, хотя
полярные летчики старшего поколения - И.П. Мазурук, И.И. Черепичный,
Н.Л. Сырокваша, Дондуков и другие никогда не забывали взять ее на Диксоне и сбросить
на станции. Как-то на Диксоне И.П. Мазурук даже отобрал у начальника аэропорта
свежий помер журнала «Огонек», чтобы отдать его изголодавшимся но новостям
зимовщикам.
Первые зимовщики наблюдали в районе станции моржей (1954-55 - две особи) и
большие стада белух. В начале 1960-х годов промыслом белухи в водах архипелага
Известий ЦИК занимались зверобойные шхуны «Апшерон» и «Зверобой». Работники
станции занимались также отловом песцов.
В 1962 году, когда станцией руководил Г.П. Ильченко, она была перенесена на
новое место, предложенное Ю.Т. Плехановым, и 1 декабря там начались наблюдения.
Сейчас «Острова Известий ЦИК» - морская гидрометеорологическая станция II-го
разряда, опорная при измерениях уровни моря. Состав зимовщиков-сотрудников станции
в 1953-1993 гг. восстановлен нами по научно-техническим отчетам и паспортам станции
за различные годы, а также по разрозненным архивным материалам (табл. 1). Поэтому в
списке зимовщиков возможны ошибки, пропуски, названы далеко не все полярники. Нам
хотелось вспомнить побольше имен людей, чьим каждодневным героическим трудом
создавались и держались полярные станции. Дольше всех на островах Известий ЦИК
работали Л.Я. Грабер, Ю.А. Помазкин, Н.П. Помазкина, А.М. Бабко (начальник станции
во время нашего пребывания) и Н.П. Павлов. Сейчас станция состоит из жилого дома,
дизельной, нескольких служебных деревянных строений и отдельно стоящей бани, одной
из лучших в Арктике, построенной под руководством А.М. Бабко.
В июле 1957 года па островах Тройном и Пологом-Сергеева работал геолог
В.В. Захаров из НИИ геологии Арктики (НИИГА), в августе его коллега А.С. Зеленко
обследовал острова Тройной и Хлебникова [Дибнер, Захаров, 1970]. Гидрографические
экспедиции Министерства морского флота МГЭ-1 и СТ-1 посещали острова в 1963, 1965,
1971-72 гг. В восточной части острова Тройной (рис. 2) расположен маяк, который
регулярно обслуживают лоцмейстерские суда. В 1965 г. на карте архипелага появились
следующие названия [Попов, Троицкий, 1972]: мыс Жарова (о. Гаврилина), названный в
честь героически погибшего астронома Северо-Таймырской экспедиции С.В. Жарова
(1911-1952); бухта Сергеева и мыс Халилецкого (о. Пологий-Сергеева). Мыс назван в
честь руководителя многих арктических экспедиций 1940-50-х гг. Г.X. Халилецкого
(1898-1960).
В 1987-91 гг. на станции работало несколько экспедиций Арктического и
Антарктического НИИ, а также журнала «Радио». Они занимались наладкой
метеорологической и радиоаппаратуры, проводили эксперименты по установлению
радиосвязи на коротких волнах. 19-29 марта 1988 г. станцию посетил специальный
корреспондент газеты «Известия», шефствовавшей над ней [Сварцевич, 1988]. На
помещенных в газете фотографиях изображена Н.П. Помазкина, много лет зимовавшая на

островах Известий ЦИК, на руках ее сидит кот Нюрик. Кот этот уникален, так как зимует
на полярной станции более 12 лет. Он пережил несколько поколений зимовщиков, а также
всех станционных собак.
Таблица 1. Зимовщики полярной станции «Острова Известий ЦИК»
Год
зимовки

Начальник станции

Зимовщики

1953—54
1954—55

Ю.Т. Плеханов
Ю.Т. Плеханов

1955—56
1956—57
1957—58
1958—59
1959—60
1960—61

Е.И. Цымотыш
Е.И. Цымотыш
Е.И. Цымотыш
А.Ф. Пастухов
А.Ф. Пастухов
Л.Н. Пеклер

1961—62

Л.Н. Пеклер

1962—63
1963—64

Г.П. Ильченко
Г.П. Ильченко

1964—65

М.Е. Захаров

1965—66

Л.Н. Фигуровский

1966—67
1967—68
1968—69
1969—70
1970—72

и.о. А.С. Любухин
Л.Я. Грабер
Л.Я. Грабер
Л.Я. Грабер
К.А. Соловьев

П.М. Кораблин, В.Н. Архипов, Мячин, Соколов, Кудинов
Кораблин, Архипов, Мячин, Соколов, Кудинов, A.М. Тепляков,
Р.Н. Теплякова, Г.М. Богданов, П.В. Шишкин
Ю.П. Ноздрин, П.Е. Бобров
Ноздрин, Бобров, Н.Ж. Францис, Ю.И. Багдасарьян
B.М. Каменский, Ю.Д. Лукинский
Лукинский
Л.Г. Забелина, И.А. Забелин, В.С. Шалухо
Шалухо, А.А. Иванов, Д.М. Овчаров, М.К. Абраменков, О.В. Панасюк,
Ососков
Иванов, Н.И. Кирдяшов, Г.Е. Судин, А.И. Савельев, В.И. Саплин,
Замураев
Иванов, Судин, Абраменков, М.Г. Стружков, Судина
Стружков, Н.А. Кошелёв, Н.В. Ратушный, В.В. Русаков, Киреева,
Ярычевский, А.П. Козлов, Толстых, Ярычевская
Козлов, В.К. Кривошеин, В.Г. Маркович, В.Н. Петров, Н.Г. Сталеев,
А.И. Лыхина
Н.Ф. Бобышев, Л.М. Бобышева, Т.А. Поддубная, А.В. Хацкевич,
Г.А. Гордеев
Бобышев, Бобышева, Г.А. Ионенко, Г.Э. Юргенфельд, Д.К. Клочков
В.И. Андрющенко, М.Б. Шифрин, Л.Н. Кондратьева
Кондратьева, Бобышев, Бобышева, В.И. Логиновский
Л.Н. Кондратьева, А.П. Бубнов, В.А. Шевчук, Т.С. Шевчук
B.А. Шевчук, Т.С. Шевчук, М.Е. Соловьева, И.С. Гоцалюк,
Т.С. Гоцалюк, С.А. Кравченко, Е.А. Зайцев, Л.И. Коханевич
C.Г. Волков, С.В. Иванов. Ю.Д. Крюков
Кравченко, Волков
Кравченко, Волков, Крюков, Н.С. Ужегова, В.П. Бурылин,
Л.П. Мокроусова. Б.В. Соловьев
Мокроусова, Бурылин, Соловьев, А.М. Самсонов, А.Д. Евтеев,
А.А. Минеев
Евтеев, Н.Л. Кучеренко, З.Ф. Лукинская, A.А. Заикин
Н.П. Помазкина, А.И. Двойных, М.И. Кепко, Г.В. Заикина,
И.Ф. Леванидова, Н.Г. Угловскнй, А.В. Безруков, В.Е. Монахов,
B.П. Сизов, В.В. Юдин, Н.П. Павлов, С.И. Соловьев, Ерилов,
Щелуканова
Соловьев, Павлов, Н.И. Юдина, В.В. Домкин, Юдин, Соловьев,
Л.К. Грищук, В.Е. Грищук, Помазкнна, И.А. Полянин, С.А. Черных,
В.Ф. Гудков, Н. Родионова
Павлов, Гудков, В.М. Корсунов, В.П. Корсунова, Волков (с 12.10 по
16.11.1990)
Павлов, В.П. Ляшенко, В.Н. Вощенкова

1972—73
1973—74
1974—75

1977—79
1979—86

Л.Я. Грабер
Л.Я. Грабер, затем
М.А. Борисов
В.М. Мокроусов,
затем - Л.Я. Грабер
Г.П. Кучеренко
Ю.А. Помазкин

1986—90

А.М. Бабко

1990—91

А.М. Бабко

1991—93

А.М. Бабко

1975—77

Примечание. В связи с недостаточной надежностью имевшихся в нашем распоряжении источников,
в таблице могут содержаться неточности: во-первых, в датах зимовок (список зимовщиков может не совсем
соответствовать указанным годам работы на станции) и, во-вторых, в инициалах полярников. К сожалению,
назвать всех работников станции мы пока тоже не можем. Здесь нам хотелось бы выразить благодарность
руководству Диксонского управления по гидрометеорологии за любезно предоставленную возможность
ознакомиться с архивными материалами.

15 сентября 1988 г. в связи с 35-летием станции ее гостем в течение полутора часов
был Герой Советского Союза А.Н. Чилингаров. 8 декабря того же года авиационная
парашютная экспедиция «ЭКСПАРК» сбросила на станцию свежие овощи. Сброс был
точен, хотя качество овощей - неважное.
К сожалению, в настоящее время в связи с ухудшением снабжения и условий труда
на полярных станциях идет интенсивный отток персонала с зимовок. Штат станции
«Острова Известий ЦИК» в 1988 - начале 1991 гг. состоял из пяти человек вместо семи, а
в 1991-93 гг. зимовало всего три человека. Значительно сокращена программа
наблюдений, состоящая сейчас из четырех синоптических сроков и двух гидрологических.
Станция не закрывается только благодаря самоотверженности начальника А.М. Бабко и
других зимовщиков, труд которых можно назвать героическим. Только они спасают сеть
полярных станций от полного развала. А ведь сеть полярных станций, созданная
усилиями многих поколений советских полярников, является уникальной. Создавалась эта
сеть многие десятилетия, а разрушить ее можно за считанные годы. Восстанавливать же
законсервированные станции гораздо дороже, чем поддерживать их существование.
Поэтому мы хотели бы особо отметить, что полярные станции являются ценнейшими
памятниками культуры и техники, и сохранение их - наш долг не только перед потомками,
но и перед теми, кто вложил в их создание свое здоровье, молодость и все силы души и
тела.
С 1 по 27 июля 1992 г. остров Тройной обследовался одним из отрядов
Арктической экспедиции ИЭМЭЖ РАН в том же составе, что и на острове Свердрупа.
Изучались геологическое строение и рельеф острова, флора и фауна, составлялись
различные специальные карты. Неоценимую помощь в исследованиях оказали нам
сотрудники полярной станции А.М. Бабко и В.Н. Вощенкова, которым автор приносит
глубокую благодарность. При подготовке настоящего очерка мы воспользовались
советами и рекомендациями первого начальника полярной станции Ю.Т. Плеханова,
которому выражаем самую искреннюю признательность.
ЛИТЕРАТУР А
1. Адлер Г.Д., Уль Г.Ф. Острова «Известий ЦИК». - Труды Арктического института,
1936, т. XLI, с. 73-80.
2. Белов М.И. Научное и хозяйственное освоение Советского Севера 1933-1945.
История открытия и освоения Северного морского пути. Л., Гидрометеоиздат, 1969, 616 с.
3. Визе В.Ю. История исследования Советской Арктики. Архангельск, Севкрайгиз,
1934, 212 с.
4. Влодавец В.И. Геолого-петрографические наблюдения, произведенные во время
экспедиции на л/п «Сибиряков» в 1932 г. - Труды Арктического института, 1933, т. X, с.
175-202.
5. Гаккель Я.Я. Материалы по картографии Карского моря. - Труды Арктического
института, 1933, т. X, с. 165-175.
6. Дибнер В.Д., Захаров В.В. Острова Карского моря. - Геология СССР. М., Недра,
1970, т. 26, с. 196-207.
7. Горелейченко А.В. АТМС передает погоду. Л., Гидрометеоиздат. 1972, с. 122-123.
8. Лаппо. Остров Свердруп по наблюдениям с самолета. - Бюллетень Арктического
института, 1932, № 4, с. 74-75.
9. Лесс А. Острова «Известий». Советская печать, 1963, № 7. с. 50-51.
10. Попов С.В., Троицкий В.А. Топонимика морей Советской Арктики. - Л.,
Гидрографическое предприятие ММФ, 1972, с. 137-140.
11. Сварцевич В. За полярным кругом. - Известия, 14 и 27 апреля 1988 года.
12. Сергеев М. Таймырская экспедиция. - Советский Север, 1934, № 1, с. 141-149.

Ссылка на статью:

Романенко Ф.А. История открытия и исследования архипелага Известий ЦИК и
острова Свердруп // Арктические тундры Таймыра и островов Карского моря: природа,
животный мир и проблемы их охраны. Том II. ИПЭЭ РАН, М.: 1994. С. 162-172.

