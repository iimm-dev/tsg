НЕЙРОСЕТЕВАЯ МОДЕЛЬ КАК СПОСОБ ОБРАБОТКИ СЛОЖНЫХ СИСТЕМ ЭКОНОМЕТРИЧЕСКИХ УРАВНЕНИЙ, ХАРАКТЕРИЗУЮЩИХ АРКТИЧЕСКУЮ ЗОНУ

Аннотация. В данной статье рассматривается возможность применения нейросетевого моделирования с целью автоматизации вычисления большого числа коэффициентов систем эконометрических уравнений для получения адекватных прогностических результатов.
Ключевые слова: Арктика, субъекты РФ, нейронные сети, персептрон, синапс.


Введение.
В реалиях современного мира одной из важнейших задач для любого государства является расширение геополитических взглядов на те области земного шара, где точная принадлежность еще не установлена или установлена весьма размыто.
На сегодняшний день для Российской Федерации одной из важнейших геополитических задач является развитие Арктики. Но, как известно, никакое положительное развитие невозможно в том случае если оно экономически не целесообразно. При каком же основном условии это развитие может быть оправдано? На этот вопрос можно дать очень простой ответ – как только в нем будут заинтересованы инвесторы, способные обеспечить поток капитала. Однако, даже несмотря на то, что
 
любые инвестиции неотъемлемо сопряжены с рисками, для привлечения крупных инвесторов необходимо иметь грамотную стратегию, обуславливающую потенциальную рентабельность прогнозируемого развития. Учитывая особые климатические условия, крайне непростой рельеф местности, обилие неосвоенных минеральных ресурсов и географического расположения можно с уверенностью сказать, что прогностические методы, успешно зарекомендовавшие себя при исследовании экономического развития других регионов, не могут быть использованы в Арктике, а иметь максимально достоверные прогнозы в этом регионе намного важнее.
Совершенно очевидно, что для составления качественного прогноза и оценки рентабельности нужно иметь точные и надежные модели, способные автоматизировано оценивать текущую экономическую ситуацию, путем анализа максимально возможного числа факторов, ее характеризующих.
1.	Для первичной оценки и последующей аналитики можно использовать линейные или нелинейные уравнения. С точки зрения увеличения точности оптимальным может стать использование ADL- моделей, способных описывать не только прямые взаимосвязи между экзогенными и эндогенными переменными, но также отражающими лаговые смещения, очень значимые при исследовании эконометрических показателей. [1]
Фактически, общей вид уравнений такой модели может быть следующим:
???? = ??(??1 , ??2 . . ???? , ??1 , ??2  . . ???? ),
 
где:
 
??	?????	?????	?????	?????	?????	?????
 
•	Y – эндогенные переменные,
•	X- экзогенные переменные,
•	t – параметр времени,
•	l – лаг,
•	n, m – порядковые индексы.
Оптимальным с точки зрения моделирования является индуктивный подход, при котором вначале строятся модели самых низших уровней, включающие в себя частные параметры по определенным секторам исследования, затем формируются модели второго уровня, отражающие взаимосвязи между моделями первого уровня и так далее, пока не будет построена конечная модель верхнего уровня. При таком варианте рассмотрения результирующая модель будет напрямую описывать ключевые аспекты экономического развития, косвенно же отражая все факторы, включенные в модели нижних уровней. [2, 3]
2.	При прогнозировании экономического развития российской Арктики удобнее всего использовать трехуровневую модель (рис. 1).
Из данного рисунка видно, что общая модель должна отслеживать разноуровневые взаимодействия. Необходимо иметь оценку
 
взаимодействий как между определенными зонами, так и отдельными регионами и субъектами. Наряду с этим важно иметь представление о том, какой вклад в формирование национальной экономики вносит каждый из представленных регионов, что так же необходимо отражать в модели. [4, 5]


Рисунок. 1. Структурная схема модели взаимодействия Арктических и приарктических регионов.

Построение ADL-модели можно разбить на 4 основных этапа:
Этап 1. Определение ключевых факторов, оказывающих максимальное влияние на экономику региона. На этом этапе выбираются экономические показатели и собирается статистика динамики по ним.
Этап 2. Выявление взаимосвязи между выбранными факторами. Определение экзогенных и эндогенных переменных. Собранные показатели проверяются на предмет взаимосвязи друг с другом. В простейшем варианте, когда речь идет о системе линейных уравнений, строится корреляционная матрица и выявляются зависимые и влияющие факторы на основе линейных оценок.
 
Этап 3. Построение системы независимых ADL уравнений. Для каждого региона на основе полученных взаимосвязей из п.2 строятся системы уравнений, алгоритмически отражающих эту взаимосвязь. Ключевой особенностью ADL-модели является так же отражение взаимосвязи не только между переменным в определенные временные моменты, но так же и взаимосвязи с временным смещением.
Этап 4. Определение межрегионального взаимодействия. Формирование конечной системы уравнений, отражающей взаимосвязи между регионами по зависимым переменным.
В конечном счете, на каждом уровне будет построена система взаимосвязанных уравнений, такого вида:


3.	Если говорить о моделях низших уровней, то таких систем уравнений будет крайне много, учитывая, что предполагается подробное описание каждой сферы экономической деятельности каждого региона. [6] Решение множества всех систем приведет к предельно точному пониманию экономической деятельности отдельно взятых регионов, их взаимодействий, возможно, их внешнеэкономической деятельности, а также влияние на экономику России в целом.
Однако, вычисление такого количества параметров будет слишком затратно по времени и не всегда даст актуальную аналитику. В связи с этим, возникает задача оптимизации расчета представленных систем уравнений. Для этих целей проще логичнее всего использовать нейронные сети. Общая суть нейросетевого моделирования достаточно проста и логически понятна. Идея заключается в том, что существует ряд входных параметров и ставится задача в определении некоторых выходных параметров. Для реализации такой взаимосвязи нейронная модель определяет такие понятия как веса – некие коэффициенты, указывающие силу воздействия входных параметров на результирующие выходные. [7,8,9,10,11,12,13]
На рис. 2 показано как во входной сумматор поступают входные данные, путем нелинейного преобразования вычисляются веса этих параметров, затем, после точки ветвления, формируется результат в виде выходных данных. Таким образом, полностью описываются все связи между входными и выходными значениями. Меняя наборы параметров можно менять модель и прослеживать развитие различных закономерностей.
 
 

Рисунок 2. Общий вид модели нейронной сети

Возвращаясь к задаче определения параметров систем взаимосвязанных уравнений, в качестве входных данных будут выступать экономические показатели, задаваемые исследователем. Нелинейный преобразователь же будет выполнять роль вычислительной системы, которая как раз и будет определять веса (коэффициенты) уравнений системы.
На рис. 3 показан примерный принцип действия нейронной модели.

Обратная связь

Рисунок. 3. Принцип действия нейронной модели.

На первом этапе после задания входных показателей алгоритм анализирует введенные данные и оценивает степень их взаимосвязи. Выявляются эндогенные и экзогенные переменные. На втором этапе выявляется форма взаимосвязи между эндогенными и экзогенными переменными, для дальнейшего выбора оптимального вида уравнения. На третьем этапе для выбранного типа уравнения рассчитываются коэффициенты. Последний этап заканчивает алгоритм вычисления нейронной сети сбором всех полученных коэффициентов и формирования конечно количественной модели. Так же в предлагаемом варианте предусмотрена обратная связь, которая позволит настраивать вычисления на каждом проводимом этапе, с целью обучения самой нейронной сети, а также улучшения результата, путем коррекции как параметров, так и вычислительных методик.
Заключение.
В данном варианте нейронная модель используется как вспомогательный элемент выбора взаимосвязанных переменных и расчета коэффициентов уравнений. Такой комплексный подход – синтез ADL- модели и нейросетевого моделирования – позволит упростить
 
вычислительные задачи исследователю, всегда будет отражать актуальную информацию за счет ускорения процесса вычисления и даст возможность наблюдать изменения на всех уровнях модели в режиме реалтайм.
