# Text corpus of russian texts about Arctic regions

Authors: Lomov Pavel, Malozemova Marina

## Structure
* `docs` - files with corpus docs.
* `twins` - possible doc duplicates detected among corpus docs.