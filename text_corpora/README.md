# Text corpora & utils 

Authors: Lomov Pavel, Malozemova Marina

## Structure
* `arctic_text_corpus` - arctic text corpus containing about 600 txt files.
* `logist_text_corpus` - logistic text corpus containing about 160 txt files.
* `test_text_corpus` - files for testing.
    * `resources` - additional linguistic resources.
* `utils` - python nltk-based utils to operate with the corpus.
    * `test` - unit tests.
    * `nltk` - additional NLKT-based resources.

## Corpora processing
Use `__corpus_processing_example.py` as template for processing text corpus.

## Used utils and additional software

* `utils/nltk/russian.pickle` - [NLTK's PunktSentenceTokenizer for Russian language](https://github.com/Mottl/ru_punkt).

## Notes

Install NLTK stop-words:
```
>>> import nltk
>>> nltk.download('stopwords')

[nltk_data] Downloading package stopwords to /home/user/nltk_data...
[nltk_data]   Unzipping corpora/stopwords.zip.
True
```