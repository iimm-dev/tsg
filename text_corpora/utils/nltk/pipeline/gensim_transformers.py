from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import Pipeline
import gensim
from gensim.models import Word2Vec
from gensim.models.phrases import Phraser, Phrases
from typing import List

from gensim.models.doc2vec import Doc2Vec, TaggedDocument


# === Create tagged corpus
# model_path = "doc2vec"
# tag_corpus = [
#     TaggedDocument(words, ["d{}".format(idx)])
#     for idx, words in enumerate(corpus.tokenized_docs())
# ]
#
# # === Get model
# model = Doc2Vec(tag_corpus, size=5, min_count=0)
# model.save(model_path)


class GensimDoc2VecTransformer(BaseEstimator, TransformerMixin):

    def __init__(self, dm=None, vector_size=None, min_count=None, window=None):
        """
        Init transformer.
        :param dm:  {1,0}, optional
            Defines the training algorithm. If `dm=1`, 'distributed memory' (PV-DM) is used.
            Otherwise, `distributed bag of words` (PV-DBOW) is employed.
        :param vector_size: Dimensionality of the feature vectors.
        :param min_count: Ignores all words with total frequency lower than this.
        :param window: The maximum distance between the current and predicted word within a sentence.
        """
        self.dm = dm
        self.vector_size = vector_size
        self.min_count = min_count
        self.window = window
        self.model = None

    def fit(self, X):
        """
        Create doc2vec model.
        :param X: tokenized_docs List[List[str]]
        :return:
        """

        self.model = Doc2Vec(X, dm=self.dm, min_count=self.min_count, vector_size=self.vect, window=self.window)
        return self

    def transform(self, X):
        return res

    def _get_taggeg_docs(self, tokenized_docs: List[List[str]], doc_tags: List[List[str]] = None):
        # if doc_tags:
        #     assert (len(doc_tags) != len(tokenized_docs)), "Tag lists quantity must be equal to docs quantity!"
        #     return [TaggedDocument(words, tags) for tags, words in zip(doc_tags, tokenized_docs)]
        # else:
        #     return [TaggedDocument(words, ["d{}".format(num)]) for num, words in enumerate(tokenized_docs())]
        return
        return [TaggedDocument(words, ["d{}".format(num)])
                for num, words in enumerate(tokenized_docs())]
