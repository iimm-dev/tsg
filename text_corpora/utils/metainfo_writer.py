from text_corpora.utils.corpus_readers import RussianPlainTextCorpusReader
import csv
import shutil
import os
from text_corpora.utils.utils import create_dir
import text_corpora.utils.helpers as hp


def __get_normalised_tags(tags_file):
    """
    Get tag words in normal form
    :param tags_file: path to tags file
    :return: set(normalised_tags)
    """
    with open(tags_file, 'r', encoding='utf8') as file:
        tags = file.read()
        tags = tags.split(' ')

    normalised_tags = set(hp.get_lemma(tag) for tag in tags)
    return normalised_tags


def __remove_existing_files(dir_path):
    """
    Remove all files from given directory
    :param dir_path:
    :return: dir_path
    """
    for file in os.listdir(dir_path):
        if file != 'tags.txt':
            if os.path.isfile(os.path.join(dir_path, file)):
                os.remove(os.path.join(dir_path, file))
            else:
                shutil.rmtree(os.path.join(dir_path, file))
    return dir_path


def __get_relevant_docs(root_dir, corpusReader:RussianPlainTextCorpusReader, tags_file, percentage=0.25):
    """
    Get relevant docs for given tags and read registry with this docs
    :param root_dir: path to root directory
    :param corpusReader: RussianPlainTextCorpusReader
    :param tags_file: path to tags file
    :param percentage: double, percentage of true matches with tag words
    :return: None
    """
    tag_data_dir = os.path.split(tags_file)[0]
    __remove_existing_files(tag_data_dir)
    registry_path = os.path.join(tag_data_dir, "registry.csv")
    tags = __get_normalised_tags(tags_file)

    for (doc_name, word_quantity) in corpusReader.get_frequent_docs_words(n_frequent_words=200):
        words = [word[0] for word in word_quantity]
        if len(tags.intersection(set(words))) != 0:
        # if (len(tags.intersection(set(words))) / len(tags)) >= percentage:
            relevant_docs_dir = create_dir(tag_data_dir, "relevant_docs")
            shutil.copy(os.path.join(root_dir, doc_name), relevant_docs_dir)
            print("{} was copied successfully into {}".format(doc_name, relevant_docs_dir))
            __write_metainfo(registry_path, doc_name, words)

        else:
            irrelevant_docs_dir = create_dir(tag_data_dir, "irrelevant_docs")
            shutil.copy(os.path.join(root_dir, doc_name), irrelevant_docs_dir)
            print("{} was copied successfully into {}".format(doc_name, irrelevant_docs_dir))


def __write_metainfo(path, doc_name, words):
    """
    Write metainfo of given relevant doc
    :param path: path to registry
    :param doc_name: path to doc name
    :param words: [words] of given doc name
    :return: None
    """
    with open(path, mode='a', encoding='utf8') as fw:
        writer = csv.writer(fw, delimiter=',')
        writer.writerow([doc_name, words])
