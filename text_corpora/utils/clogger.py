import logging
# == To get memory consumption of some object
from pympler import asizeof

"""
see Logging python
https://docs.google.com/document/d/1t7Ia5zCNe7UiY4EHHJ43kKoXBzilNwJi4VTy9fzqQLI/edit?usp=sharing
"""


def get_logger(log_path, file_name, level=logging.INFO):
    """
    Create logger.
    :param log_path: path to save logs
    :param file_name: logfile name
    :param level:
    :return: logger
    """
    logging.basicConfig(
        level=level,
        format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s",
        handlers=[
            logging.FileHandler("{0}/{1}.log".format(log_path, file_name)),
            logging.StreamHandler()
        ])
    return logging.getLogger()


def get_used_memory_size_mb(object):
    """
    Return memory size of the given object.
    :param object:
    :return: memory size in Megabytes.
    """
    return asizeof.asizeof(object)/(1024*1024)


def log_bechmark(logger):
    """
    Decorator with logger. Show executed time AND function execution frontiers.
    Using:
    @log_bechmark(logger)
    def some_function(arg_a):
        return arg_a

    :param logger: logger to use
    :return:
    """
    def benchmark(func):
        import time

        def wrapper(*args, **kwargs):
            start_time = time.time()
            logger.info("=== START:{} ================".format(func.__name__))
            res = func(*args, **kwargs)
            exec_time = round(time.time() - start_time, 3)
            logger.info("=== END:{} for [{}] sec. ===".format(func.__name__, exec_time))
            return res

        return wrapper

    return benchmark
