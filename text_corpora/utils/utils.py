import random
import string
import shutil
import os
from os import linesep


def randomString(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))


def generate_random_content(lines_quantity=10, word_per_line=10):
    """ Generate random text strings"""
    lines = []
    for i in range(0, lines_quantity):
        line = " ".join([randomString(stringLength=random.randint(5, 12)) for j in range(0, word_per_line)]) + linesep
        lines.append(line)
    return lines


def generate_random_file(dir_path, file_name=None, file_strings=None):
    """
    Create random text file in pointed dir.
    :param dir_path:
    :param file_name:
    :param file_strings: [strings to write]
    :return:
    """

    # Generate new name for a file...
    file_name = randomString(stringLength=5) + ".txt" if file_name is None else file_name
    while True:
        file_path = os.path.join(dir_path, file_name)
        if os.path.exists(file_path):
            file_name = randomString(stringLength=5) + ".txt" if file_name is None \
                else randomString(stringLength=2)+"_"+file_name
        else:
            break
    # Write content to file ...
    with open(file_path, mode='w', encoding='utf8') as f:
        f.writelines(generate_random_content() if file_strings is None else file_strings)

    return file_path


def create_dir(path, dir_name):
    """
    Create subdir with given name.
    :param path: path to super dir.
    :param dir_name: name of subdir being created.
    :return:
    """
    dir_path = str(os.path.join(path, dir_name))
    try:
        os.mkdir(dir_path)
        return dir_path
    except:
        return dir_path


def remove_file(file_path):
    os.remove(file_path)


def remove_dir(dir_path):
    shutil.rmtree(dir_path, ignore_errors=True)


def copy_file(src, dst):
    os.makedirs(os.path.dirname(dst), exist_ok=True)
    shutil.copy(src, dst)


def move_file(src_file_path, dst_path):
    os.makedirs(os.path.dirname(dst_path), exist_ok=True)
    shutil.move(src_file_path, dst_path)
