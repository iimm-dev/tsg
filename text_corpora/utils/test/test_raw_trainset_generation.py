import unittest
from setgen.raw_trainset_generation import *
from text_corpora.utils.corpus_readers import RussianPlainTextCorpusReader
import os


class MyTestCase(unittest.TestCase):

    def setUp(self):
        self.corpus_root_path = os.getcwd()
        self.data_dir_name = "test_data"
        self.fileids_regex = self.data_dir_name + "/" + r"\w+.txt"
        self.corpus = RussianPlainTextCorpusReader(self.corpus_root_path, fileids=self.fileids_regex)

        self.entities_dict = {"ТСДПД": ["полярное исполнение", "ледокол", "белая сова"]}

        self.val = "ледокол"

        self.sentence = "Имея в виду популярность темы Арктики в средствах массовой информации в последние 10-15 лет, " \
            "нельзя не заметить, что исконно-русский термин полынья - просто чемпион по неправильному употреблению " \
            "в речи или текстах не только в среде отечественных журналистов или, скажем, путешественников-туристов, " \
            "но и, что действительно прискорбно, в среде моряков-подводников и руководителей различного уровня."
        # self.sentence = "В хрестоматийном толковом словаре живого великорусского языка Даля лёд определяется как " \
        #                 "«мёрзлая вода; застывшая и отверделая от стужи жидкость»."
        self.norm_entity = "арктика"

    def test__get_entity_from_sentence(self):
        entity = get_entity_from_sentence(self.sentence, self.norm_entity, self.corpus)
        self.assertTrue(entity == 'Арктики')

    def test__get_key(self):
        key = get_key(self.entities_dict, self.val)
        self.assertTrue(key == 'ТСДПД')

    def test__find_entity_inside_sentence(self):
        found_entity = find_entity_inside_sentence(self.corpus, self.sentence, self.norm_entity)
        self.assertTrue(found_entity == 'Арктики')


if __name__ == '__main__':
    unittest.main()
