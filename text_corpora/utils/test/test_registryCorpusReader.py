from unittest import TestCase
from text_corpora.utils.utils import *
import math
from text_corpora.utils.corpus_readers import RegistryCorpusReader
from pathlib import Path


class TestRegistryCorpusReader(TestCase):

    def setUp(self):
        # Create empty corpus with config...
        self.corpus_root_path = os.getcwd()
        self.data_dir_name = "test_data"
        self.data_dir_path = create_dir(self.corpus_root_path, self.data_dir_name)
        self.file_quantity = 3
        self.fileids_regex = self.data_dir_name + "/" + r"\w+.txt"

        # Create files in repository...
        for i in range(0, self.file_quantity):
            generate_random_file(self.data_dir_path, file_name="doc_" + randomString(stringLength=5) + ".txt")

        # Create test corpus reader
        self.corpus = RegistryCorpusReader(self.corpus_root_path, fileids=self.fileids_regex, encoding="utf8")

    def tearDown(self):
        remove_dir(self.data_dir_path)
        remove_dir(self.corpus.twin_dir_path)
        remove_file(os.path.join(self.corpus.root, self.corpus.registry_file_name))

    def test_segmentation_of_registry(self):
        # Generate many files and add to registry
        for i in range(0, 100):
            generate_random_file(self.data_dir_path, file_name="doc_" + randomString(stringLength=5) + ".txt")
        result = self.corpus.update_registry()

        # === Check that exists one 0-section with all files
        self.assertTrue(self.corpus.section_size == 0)
        self.assertTrue(0 in self.corpus._sections)
        self.assertTrue(len(self.corpus._sections.keys()) == 1)
        self.assertTrue(len(self.corpus._sections[0]) == len(self.corpus.fileids()))

        # === Make segmentation
        section_size = 13
        section_ids = self.corpus.split_to_sections(section_size)
        # === check section quantity
        corpus_size = len(self.corpus.fileids())
        section_quantity = len(self.corpus._sections.keys())
        assumed_section_quantity = math.ceil(corpus_size / section_size)
        self.assertTrue(section_quantity == assumed_section_quantity)

        # === check quantity of files in all sections
        all_section_file_quantity = sum(len(lst) for lst in self.corpus._sections.values())
        self.assertTrue(corpus_size == all_section_file_quantity)

        # === check that sections are the same after save and load corpus
        saved_sections = self.corpus._sections
        self.corpus = RegistryCorpusReader(self.corpus_root_path, fileids=self.fileids_regex, encoding="utf8")
        self.assertTrue(len(saved_sections.keys()) == len(self.corpus._sections.keys()))

        # === check get_section_files method
        # ... get current sections ...
        secid_to_files = [(id, files) for sec_id in section_ids for files in self.corpus._get_section_file_hasdids(sec_id)]
        saved_sections = {}
        for secid, files in secid_to_files:
            saved_sections[secid] = files

        # ... reload corpus and get them again AND compare...
        self.corpus = RegistryCorpusReader(self.corpus_root_path, fileids=self.fileids_regex, encoding="utf8")
        secid_to_files = [(id, files) for sec_id in section_ids for files in self.corpus._get_section_file_hasdids(sec_id)]
        loaded_sections = {}
        for secid, files in secid_to_files:
            loaded_sections[secid] = files
        self.assertTrue(saved_sections == loaded_sections)

        # ... add new file, reload corpus, check that it went to the last section...
        corpus_size = len(self.corpus.fileids())
        new_file_id = self.corpus._get_hash_of_file_rare_words(generate_random_file(self.data_dir_path, file_name="doc_" + randomString(stringLength=5) + ".txt"))
        self.corpus.update_registry()
        self.corpus = RegistryCorpusReader(self.corpus_root_path, fileids=self.fileids_regex, encoding="utf8")
        last_section_id = max(self.corpus._sections.keys())
        last_section_files = self.corpus._get_section_file_hasdids(last_section_id)
        self.assertTrue(new_file_id in last_section_files)

        # ... check other get_section_bla-bla methods
        for path in self.corpus.section_files_paths(last_section_id):
            self.assertTrue(os.path.exists(path))

    def test_rebuild_registry(self):
        file_quantity = len(self.corpus.rebuild_registry())

        # Delete one file ...
        filename = self.corpus._get_filenames()[0]
        file_path = os.path.join(self.corpus_root_path, filename)
        os.remove(file_path)

        # Check rebuilding
        self.corpus.rebuild_registry()
        self.assertTrue(file_quantity > len(self.corpus.rebuild_registry()))

    def test_update_registry(self):
        result = self.corpus.update_registry()
        self.assertTrue(result[0] == 0 and result[1] == 0)

        # Delete one file...
        filename = self.corpus._get_filenames()[0]
        file_path = os.path.join(self.corpus_root_path, filename)
        os.remove(file_path)
        result = self.corpus.update_registry()
        self.assertTrue(result[0] == 0 and result[1] == 1)

        # Add new file
        generate_random_file(self.data_dir_path, file_name="doc_" + randomString(stringLength=5) + ".txt")
        result = self.corpus.update_registry()
        self.assertTrue(result[0] == 1 and result[1] == 0)

    def test__add_new_file_to_registry(self):
        # Add new file
        file_name = "doc_" + randomString(stringLength=5) + ".txt"
        generate_random_file(self.data_dir_path, file_name=file_name)
        result = self.corpus.update_registry()
        self.assertTrue(result[0] == 1 and result[1] == 0)

        # Try to add twin-file ...
        src = os.path.join(self.data_dir_path, file_name)
        dst = os.path.join(self.data_dir_path, "doc_" + randomString(stringLength=5) + ".txt")
        copy_file(src, dst)
        result = self.corpus.update_registry()
        self.assertTrue(result[0] == 0 and result[1] == 0)

    def test__rename_file_ro_registry(self):
        # result = self.corpus.update_registry()
        some_filename = os.path.basename(self.corpus.fileids()[1])
        some_file_path = os.path.join(self.data_dir_path, some_filename)
        renamed_file_path = os.path.join(self.data_dir_path, "doc_renamed_{}.txt".format(randomString(3)))
        os.rename(some_file_path, renamed_file_path)

        result = self.corpus.update_registry()
        # print(os.path.join(self.corpus.root,some_filename),renamed_file_path)
        # print(result)
        self.assertTrue(result[0] == 1 and result[1] == 1)

    def test__get_twin_from_registry(self):
        file_quantity = len(self.corpus.fileids())
        # print(len(self.corpus.fileids()))
        print("Initial gen", file_quantity)
        # Create 2 files with same content
        content_strings = generate_random_content()
        file_A = "file_a.txt"
        file_B = "file_b.txt"

        generate_random_file(self.data_dir_path, file_name=file_A, file_strings=content_strings)
        generate_random_file(self.data_dir_path, file_name=file_B, file_strings=content_strings)
        print("2 gen", len(self.corpus.fileids()))
        # self.assertTrue(len(self.corpus.fileids()) == file_quantity + 2)

        # Now check dir for dublicates - one file should be removed...
        self.corpus.update_registry()
        print("1 gen", len(self.corpus.fileids()))

        self.assertTrue(len(self.corpus.fileids()) == file_quantity + 1)

    def test__get_hash_of_file_rare_words(self):
        # ... test with real files
        text_corpora_dir = Path(self.corpus_root_path).parents[1]
        text_A = os.path.abspath(os.path.join(text_corpora_dir, "test_text_corpus", "docs", "zapovedniki.txt"))
        # ... same as zapovedniki.txt minus 1 paragraph
        text_B = os.path.abspath(os.path.join(text_corpora_dir, "test_text_corpus", "docs", "zapoved123.txt"))
        text_C = os.path.abspath(os.path.join(text_corpora_dir, "test_text_corpus", "docs", "animals.txt"))

        rare_words_hash_A = self.corpus._get_hash_of_file_rare_words(text_A)
        rare_words_hash_B = self.corpus._get_hash_of_file_rare_words(text_B)
        rare_words_hash_C = self.corpus._get_hash_of_file_rare_words(text_C)

        # Check hashes of rare words
        self.assertTrue(rare_words_hash_A == rare_words_hash_B)
        self.assertTrue(rare_words_hash_A != rare_words_hash_C)
