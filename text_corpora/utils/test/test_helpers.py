import unittest
import text_corpora.utils.helpers as hp


class HelpersTestCase(unittest.TestCase):

    def get_sample_text_A(self):
        return """Но Тайваем новая земля оставалась недолго. Уже спустя полгода, в январе 1914, ей было присвоено имя Императора Николая Второго. Спустя ровно 12 лет, в январе 1926 года, Президиум ВЦИК издал Постановление, в соответствиии с которым остров переименовали в Северную Землю.
        Северная Земля не раз привлекала внимание ученых из разных стран. Известно, например, что в 1919 году ее хотел обследовать Руал Амундсен во время зимовки его судна “Мод” у мыса Челюскина, ее аэрофотосъемку планировал сделать в 1928 году Умберто Нобиле. Словом, к этой части суши в Северном Ледовитом океане проявляли интерес разные ученые и разные государства.
        Советское правительство, заявившее о принадлежности Северной Земли, открытой русскими моряками, России, организовало экспедицию, продолжавшуюся с 1931 по 1933 годы, в результате которой остров превратился в архипелаг, потому что члены экспедиции, — руководитель Георгий Ушаков, геолог Николай Урванцев, опытный промысловый охотник Сергей Журавлев и радист Василий Ходов — открыли другие острова вокруг Северной Земли. И самый крупный, и самый первый из открытых островов дал название всему архипелагу.
        """

    def test_sent_tokenizer(self):
        sentences = list(hp.sentences(self.get_sample_text_A()))
        print(sentences)
        self.assertTrue(len(sentences) == 8)

    def test_word_tokenizer(self):
        sentence = 'Но Тайваем новая земля оставалась недолго.'
        golden_tokens = ['Но', 'Тайваем', 'новая', 'земля', 'оставалась', 'недолго', '.']
        self.assertEqual(hp.get_words(sentence), golden_tokens)

    def test_ngrams(self):
        sentence = 'Но Тайваем новая земля оставалась недолго.'
        print(hp.get_ngrams(sentence.split(' '), 2))

    def test_morph_analyser(self):
        print("=== Test morphological analyser ===")
        for w in self.get_sample_text_A().split(' '):
            print(hp.get_lemma(w))

        ngram = self.get_sample_text_A()
        print(hp.get_ngram_lemmas(ngram))
        print("=== END - Test morphological analyser ===")

    def test_stemmer(self):
        ngram = self.get_sample_text_A()
        print("=== Test stemmer ===")
        print(hp.get_ngram_basics(ngram))

        for w in self.get_sample_text_A().split(" "):
            print(hp.get_basic(w))

        print("=== END - Test stemmer ===")
