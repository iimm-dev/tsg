from unittest import TestCase
from text_corpora.utils.utils import *
from text_corpora.utils.corpus_readers import RussianPlainTextCorpusReader
import pathlib
from sklearn.feature_extraction.text import CountVectorizer
import text_corpora.utils.helpers as hp


class TestRussianPlainTextCorpusReader(TestCase):

    def setUp(self):
        # Create empty corpus with config...
        self.corpus_root_path = os.getcwd()
        self.data_dir_name = "test_data"
        self.data_dir_path = create_dir(self.corpus_root_path, self.data_dir_name)
        self.file_quantity = 5
        self.fileids_regex = self.data_dir_name + "/" + r"\w+.txt"

        # Create files in repository...
        generate_random_file(self.data_dir_path, file_name="doc_" + randomString(stringLength=5) + ".txt",
                             file_strings=self.get_sample_text_A())
        generate_random_file(self.data_dir_path, file_name="doc_" + randomString(stringLength=5) + ".txt",
                             file_strings=self.get_sample_text_B())

        # Get path to sentence tokenizer
        upper_dir, current_dir = os.path.split(os.path.dirname(os.getcwd()))
        external_tokenizer_path = pathlib.Path(os.path.join(upper_dir, "utils", "nltk", "russian.pickle")).as_uri()
        print(external_tokenizer_path)
        # Create test corpus reader
        self.corpus = RussianPlainTextCorpusReader(self.corpus_root_path,
                                                   fileids=self.fileids_regex,
                                                   external_tokenizer_path=external_tokenizer_path,
                                                   encoding="utf8")

    def tearDown(self):
        remove_dir(self.data_dir_path)
        remove_dir(self.corpus.twin_dir_path)
        remove_file(os.path.join(self.corpus.root, self.corpus.registry_file_name))

    def test_tokenized_docs(self):
        golden_tokenized_doc = ["21", "мая", "самолет", "Водопьянова", "с", "четверкой", "папанинцев", "вылетел", "к",
                                "полюсу", "."]
        golden_tokenized_doc = [w for w in golden_tokenized_doc if w not in self.corpus.stop_words]
        lemmatized_golden_tokenized_doc = [hp.get_lemma(w) for w in golden_tokenized_doc if
                                           w not in self.corpus.stop_words]

        # Create new file in corpus...
        content = " ".join(golden_tokenized_doc)
        file_path = generate_random_file(self.data_dir_path, file_name="doc_" + randomString(stringLength=5) + ".txt",
                                         file_strings=content)
        self.corpus.update_registry()

        # Check tokenization - rihjt tokezized doc must be in corpus
        right_case_quantity = sum(1 for d in self.corpus.tokenized_docs() if d == golden_tokenized_doc)
        self.assertEqual(right_case_quantity, 1)

        right_case_quantity = sum(
            1 for d in self.corpus.tokenized_docs(make_lemmatization=True) if d == lemmatized_golden_tokenized_doc)
        self.assertEqual(right_case_quantity, 1)

    def test_contain_word(self):
        # Create docs with some words...
        doc_A = ["Гваделупа очень далеко."]
        doc_B = ["Василий вылетел к озеру."]
        doc_C = ["Василий вылетел вчера днем к озеру. Вечером мы вышли к озеру."]

        generate_random_file(self.data_dir_path, file_name="doc_" + randomString(stringLength=5) + ".txt",
                             file_strings=doc_A)
        generate_random_file(self.data_dir_path, file_name="doc_" + randomString(stringLength=5) + ".txt",
                             file_strings=doc_B)
        generate_random_file(self.data_dir_path, file_name="doc_" + randomString(stringLength=5) + ".txt",
                             file_strings=doc_C)

        # Check containing ...
        self.assertEqual(self.corpus.contain_word("Гваделупа"), 1)
        self.assertEqual(self.corpus.contain_word("Гваделупа", with_quantity=False), True)
        self.assertEqual(self.corpus.contain_word("озеро"), 3)
        self.assertEqual(self.corpus.contain_word("озеро", with_quantity=False), True)
        self.assertEqual(self.corpus.contain_word("озеру"), 3)
        self.assertEqual(self.corpus.contain_word("вертигон"), 0)

    def get_sample_text_A(self):
        return """Но Тайваем новая земля оставалась недолго. Уже спустя полгода, в январе 1914, ей было присвоено имя Императора Николая Второго. Спустя ровно 12 лет, в январе 1926 года, Президиум ВЦИК издал Постановление, в соответствиии с которым остров переименовали в Северную Землю.
        Северная Земля не раз привлекала внимание ученых из разных стран. Известно, например, что в 1919 году ее хотел обследовать Руал Амундсен во время зимовки его судна “Мод” у мыса Челюскина, ее аэрофотосъемку планировал сделать в 1928 году Умберто Нобиле. Словом, к этой части суши в Северном Ледовитом океане проявляли интерес разные ученые и разные государства.
        Советское правительство, заявившее о принадлежности Северной Земли, открытой русскими моряками, России, организовало экспедицию, продолжавшуюся с 1931 по 1933 годы, в результате которой остров превратился в архипелаг, потому что члены экспедиции, — руководитель Георгий Ушаков, геолог Николай Урванцев, опытный промысловый охотник Сергей Журавлев и радист Василий Ходов — открыли другие острова вокруг Северной Земли. И самый крупный, и самый первый из открытых островов дал название всему архипелагу.
        """

    def get_sample_text_B(self):
        return """У Седова уже был немалый опыт работы на Севере. После того как он в 24 года был прикомандирован к Гидрографическому управлению, ему пришлось несколько раз работать в экспедициях в Северном Ледовитом океане, изучая районы острова Вайгач и архипелага Новая Земля, устья реки Кары и Колымы. Седов считал исключительно важным для России развивать движение по Северному морскому пути и даже написал об этом статьи «Северный океанский путь» и «Значение Северного океанского пути для России».
        С самого начала предполагалось, что экспедиция будет финансироваться государством, однако комиссия Главного гидрографического управления не согласилась выделять средства – план Седова она считала слабо разработанным и неподготовленным. Против экспедиции Седова выступали многие, в том числе известный исследователь Арктики В.Русанов, который сомневался в профессионализме руководителя, в газетах публиковались фельетоны об арктическом предприятии Седова. Но это не помогло.
        В ответ Седов опубликовал в газете «Новое время» открытое письмо, в котором просил подержать свою экспедицию. Он говорил: «Русский народ должен принести на это национальное дело небольшие деньги, я приношу жизнь… Гарантия – моя жизнь. Она единственное, чем я могу гарантировать серьезность моей попытки». Ему поверили читатели, ему поверили меценаты. И вот уже с помощью одного из владельцев «Нового времени» Михаила Суворина, который организовал кампанию по сбору средств, Седов собирает 12 тысяч, 20 тысяч дает Суворин и 10 – сам император Николай Второй. В конечном итоге удалось собрать 108 тысяч рублей, из которых почти половина ушла на фрахт «Святого Мученика Фоки».
        Официально планировалось создать зимовье на Новой Земле и Земле Франца-Иосифа, потом вернуться в Архангельск, и уже в следующем году, посетив зимовья, двинуться к полюсу. А неофициально Седов планировал добраться до Земли Франца-Иосифа, создать базу и двинуться к полюсу на собачьих упряжках. На всю экспедицию он отводил полгода, предусматривалась и одна зимовка – в крайнем случае.
        """

    def get_sample_text_C(self):
        return """21 мая самолет Водопьянова с четверкой «папанинцев» и руководством экспедиции на борту вылетел к полюсу. """

    def test_frequency(self):

        vectoriser = CountVectorizer()
        vectors = vectoriser.fit_transform(list(self.corpus.docs()))

        print("Vocabulary: {}".format(vectoriser.vocabulary_))

        for n, doc in enumerate(vectors.toarray()):
            frequency = []
            for word in vectoriser.vocabulary_.keys():
                print("{} {}".format(word, doc[vectoriser.vocabulary_[word]]))
                frequency.append((word, doc[vectoriser.vocabulary_[word]]))
            print(frequency)
        # self.assertIsNotNone(frequency)

    def test_get_frequent_doc_words(self):
        text_corpora_dir = pathlib.Path(self.corpus_root_path).parents[1]
        text_A = os.path.abspath(os.path.join(text_corpora_dir, "test_text_corpus", "docs", "zapovedniki.txt"))
        # ... same as zapovedniki.txt minus 1 paragraph
        text_B = os.path.abspath(os.path.join(text_corpora_dir, "test_text_corpus", "docs", "zapoved123.txt"))

        freq_doc_words_A = self.corpus.get_frequent_doc_words(text_A)[1]
        freq_doc_words_B = self.corpus.get_frequent_doc_words(text_B)[1]

        # Check hashes of frequent words
        freq_words_hash_A = hp.get_hash(freq_doc_words_A)
        freq_words_hash_B = hp.get_hash(freq_doc_words_B)

        self.assertTrue(freq_words_hash_A != freq_words_hash_B)

    def test_get_rare_doc_words(self):
        # ... test with random words
        words_text_A = {'cat': 1, 'dog': 1, 'owl': 2, 'rabbit': 5}
        words_text_B = {'cat': 1, 'dog': 1, 'fox': 3, 'rabbit': 4}
        words_text_C = {}

        self.assertTrue(hp.get_hash(words_text_A) != hp.get_hash(words_text_B))
        self.assertTrue(hp.get_hash(words_text_A) != hp.get_hash(words_text_C))

        # ... test with real files
        text_corpora_dir = pathlib.Path(self.corpus_root_path).parents[1]
        text_A = os.path.abspath(os.path.join(text_corpora_dir, "test_text_corpus", "docs", "zapovedniki.txt"))
        # ... same as zapovedniki.txt minus 1 paragraph
        text_B = os.path.abspath(os.path.join(text_corpora_dir, "test_text_corpus", "docs", "zapoved123.txt"))

        rare_doc_words_A = self.corpus.get_rare_doc_words(text_A)
        rare_doc_words_B = self.corpus.get_rare_doc_words(text_B)

        # Check hashes of rare words
        rare_words_hash_A = hp.get_hash(rare_doc_words_A.values())
        rare_words_hash_B = hp.get_hash(rare_doc_words_B.values())

        self.assertTrue(rare_words_hash_A == rare_words_hash_B)
