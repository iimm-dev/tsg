from __future__ import print_function

import hashlib
import os
import pathlib
from csv import reader, writer, QUOTE_MINIMAL
from os.path import isfile, join, exists

import nltk
import pymorphy2
from nltk.corpus.reader.api import CorpusReader
from nltk.corpus.reader.plaintext import PlaintextCorpusReader
from nltk.corpus.reader.util import find_corpus_fileids
from nltk.data import FileSystemPathPointer
from nltk.stem import *
from nltk.tokenize.toktok import ToktokTokenizer
from nltk.util import skipgrams
from six import string_types
from sklearn.feature_extraction.text import CountVectorizer
from collections import Counter
from nltk import FreqDist
import string
from deprecated import deprecated

# --- init logger ---
from text_corpora.utils.clogger import get_logger
from text_corpora.utils.utils import create_dir, move_file
import text_corpora.utils.helpers as hp

logger = get_logger("./", "common")


class RegistryCorpusReader(CorpusReader):

    def __init__(self, root, fileids=None, encoding="utf8", registry_file_name="registry.csv",
                 use_twin_checking=True, **kwargs):
        super().__init__(root, fileids, encoding=encoding)
        self._registry_file_name = registry_file_name
        self._twin_dir_path = create_dir(self.root, "twins")
        self._use_twin_checking = use_twin_checking
        self._registry = {}
        self._sections = {}
        self._section_size = 0
        self._read_registry_file()
        self._name_pattern = fileids if isinstance(fileids, string_types) else None
        self.update_registry()

    @property
    def registry_file_name(self):
        return self._registry_file_name

    @property
    def twin_dir_path(self):
        return self._twin_dir_path

    @property
    def section_size(self):
        return self._section_size

    def rebuild_registry(self):
        """
        Reread files in registry dir and recreate rep-file.

        :return: [names of added files]
        """
        logger.info("== Rebuild registry =====")
        self._registry.clear()
        self._sections.clear()
        self._fileids = self._get_filenames()

        added_file_quantity = 0
        for file_name in self.fileids():
            if self._add_new_file_to_registry(file_name):
                added_file_quantity += 1
            self._write_registry_file()

        logger.info("[{}] files were added".format(added_file_quantity))
        logger.info("== End - Rebuild of registry ===")

        return self._fileids

    def update_registry(self):
        """
        Add/delete files from registry.
        :return: (added_file_quantity, deleted_file_quantity)
        """
        logger.info("=== Update registry ===")
        logger.info("=== Initial text corpus consist of [{}] files ===".format(len(self._get_filenames())))
        file_names = set(self._get_filenames())
        reg_names = set(self._registry.values())
        files_to_addition = file_names - reg_names
        files_to_deletion = reg_names - file_names

        # === Delete files from registry which are not existed ===
        deleted_file_quantity = self._del_files_from_registry(files_to_deletion)

        # === Add new files to registry ===
        added_file_quantity = sum(1 for file_name in files_to_addition
                                  if self._add_new_file_to_registry(file_name))
        self._write_registry_file()
        self._section_size = self._get_section_size()

        # === Reread filenames into repository...
        self._fileids = self._get_filenames()
        logger.info("[{}] files were added".format(added_file_quantity))
        logger.info("[{}] files were removed".format(deleted_file_quantity))
        logger.info("=== Final text corpus consist of [{}] files ===".format(len(self._registry.values())))
        logger.info("=== End - Update of registry ===")

        return [added_file_quantity, deleted_file_quantity]

    def split_to_sections(self, section_size):
        """
        Split text corpus into given section size
        :param section_size: int
        :return: section keys
        """
        # see https://stackoverflow.com/questions/312443/how-do-you-split-a-list-into-evenly-sized-chunks
        logger.info(
            "=== Split [{}] files to sections with size [{}] ===".format(len(self._registry.keys()), section_size))
        files_quantity = len(self._registry.keys())
        self._sections.clear()
        start_section_border = 0
        section_id = 0
        file_ids = list(self._registry.keys())
        for end_section_border in range(0, files_quantity, section_size):
            if start_section_border == end_section_border: continue
            self._sections[section_id] = file_ids[start_section_border:end_section_border]
            logger.info("... section [{}] : range [{} - {}], files quantity: [{}]"
                        .format(section_id, start_section_border, end_section_border, len(self._sections[section_id])))
            section_id += 1
            start_section_border = end_section_border

            # === Add remain files in last section ...
            if end_section_border < files_quantity:
                end_section_border = files_quantity
                self._sections[section_id] = file_ids[start_section_border:end_section_border]
                logger.info("... section [{}] : range [{} - {}], files quantity: [{}]"
                            .format(section_id, start_section_border, end_section_border, len(self._sections[section_id])))
            self._section_size = self._get_section_size()
            self._write_registry_file()
        logger.info("=== Split files to sections complete - section quantity [{}] ===".format(section_id + 1))
        return self._sections.keys()

    def _get_section_file_hasdids(self, section_id):
        """
        Return [file_hash_ids] for given section by section id.

        :param section_id:
        :return: [file_hash_ids] or ValueError if section doesn't exist.
        """
        try:
            section_id = int(section_id)
            return self._sections[section_id]
        except ValueError:
            raise ValueError("!!! Section with id:[{}] doesn't exist. Correct ids are: [{}] "
                             .format(section_id, self._sections.keys()))

    def section_files_ids(self, section_id):
        """
        Generator of [file_ids] for section with given id.

        :param section_id:
        :return: generator of file_ids or ValueError if section doesn't exist.
        """
        for file_hashid in self._get_section_file_hasdids(section_id):
            yield self._registry[file_hashid]

    def section_files_paths(self, section_id):
        """
        Generator of paths to files of given section by section id.

        :param section_id:
        :return: generator of file_paths for the section or ValueError if section doesn't exist.
        """
        for file_id in self.section_files_ids(section_id):
            yield join(self.root, file_id)

    def _get_section_size(self):
        """
        Get quantity of files in section
        :return: quantity of files in section or 0
        """
        if len(self._sections.keys()) > 1:
            return sum(len(section_elements) for section_elements in self._sections.values()) \
                   // len(self._sections.keys())
        else:
            return 0

    def _add_new_file_to_registry(self, file_name):
        """
        Add given file into registry
        :param file_name: str
        :return: True
        """
        file_path = self.abspath(file_name).__str__()

        # Check twin file in registry...

        if self._use_twin_checking:
            file_hash_id = self._get_hash_of_file_rare_words(file_path, encoding=self._encoding)
            twin_file = self._get_twin_from_registry(file_hash_id)  # file_path
            twin_file_path = join(self.root, twin_file) if twin_file is not None else None
            if (twin_file_path is not None) and exists(twin_file_path) and isfile(twin_file_path):
                logger.info("File [{}] has twin in registry - [{}], "
                            "so that it can't be added.".format(file_name, twin_file))
                # ... move file being added to twins dir
                move_file(file_path, join(self._twin_dir_path, file_name))
                logger.info("... move [{}] to [{}] dir".format(file_name, self._twin_dir_path))
                return False

            # Add file to registry ...
            logger.info("Add new file to registry [{}] with content hash [{}]"
                        .format(file_name, file_hash_id))
            self._registry[file_hash_id] = file_name
            self._add_new_file_to_sections(file_hash_id)
            return True

        else:
            file_idx = self._get_filenames().index(file_name)
            logger.info("Add new file to registry [{}]".format(file_name))
            self._registry[file_idx] = file_name
            self._add_new_file_to_sections(file_idx)
            return True

    def _add_new_file_to_sections(self, file_hash_id):
        """
        Add file to section by its hash id
        :param file_hash_id:
        :return: NONE
        """
        # If no any section exists (i.e .section_size == 0) then add to 0-section...
        if self.section_size == 0:
            if 0 in self._sections:
                self._sections[0].append(file_hash_id)
            else:
                self._sections[0] = [file_hash_id]
        # ... else add file to last OR new section
        else:
            last_section_id = len(self._sections.keys()) - 1
            if len(self._sections[last_section_id]) < self.section_size:
                self._sections[last_section_id].append(file_hash_id)
            else:
                self._sections[last_section_id + 1] = [file_hash_id]

    def _del_files_from_registry(self, file_names):
        """
        Delete pointed files from registry.
        :param file_names: set of files to be removed
        :return: removed files quantity
        """
        del_keys = {key for key, val in self._registry.items() if val in file_names}
        # delete file from some section ...
        for k in del_keys:
            # TODO make optimisation of delete from sections
            for section, filehashes in self._sections.items():
                filehashes.remove(k)
            filename = self._registry.pop(k)
            logger.info("File [{}] removed from registry".format(filename))

        self._write_registry_file()
        return len(del_keys)

    def _get_twin_from_registry(self, hash):
        """ ищем такой же в репе
            для каждого найденного
            TODO если находим - сравнение 2файлов по md5
                если равны то не добавляем + завешраем добавление
                добавляем файл + его хэш
        """
        if hash in self._registry.keys():
            return self._registry[hash]
        else:
            return None

    def _get_hash_of_file_rare_words(self, file_path, encoding="utf-8", n_rare_words=10):
        """
        Get list of file's rare words and its hash
        :param file_path: str
        :param encoding: utf-8
        :return:
        """
        with open(file_path, 'r', encoding=encoding) as file:
            text = file.read().strip()
        counted_text_tokens = FreqDist(text.split(' '))
        rare_words = sorted(counted_text_tokens.items(), key=lambda pair: pair[1])[:n_rare_words]
        rare_words_hash = hashlib.md5(str(rare_words).encode()).hexdigest()
        return rare_words_hash

    @deprecated(reason="This function will be removed soon")
    def _get_file_hash_id(self, file_path, encoding="utf-8"):
        """
        Get some string from file and get hash from it.
        :param file_path:
        :param encoding:
        :return:
        """
        hash_string_numbers = {0, 5, 15, 40, 200}
        hash_symbols_quantity = 100
        hashing_file = open(file_path, mode='r', encoding=encoding)
        hash_string = ""
        for i, line in enumerate(hashing_file):
            if i in hash_string_numbers:
                hash_string_numbers.remove(i)
                hash_string += str(line[:hash_symbols_quantity]).strip()
            if not hash_string_numbers:
                break

        name_hash = hashlib.md5(hash_string.encode()).hexdigest()
        hashing_file.close()
        return name_hash

    def _read_registry_file(self):
        """
        Read content of registry from registry file.
        :return: NONE
        """
        self._registry.clear()
        self._sections.clear()
        file_path = join(self.root, self._registry_file_name)
        try:
            with open(file_path, newline='') as csvfile:
                spamreader = reader(csvfile, delimiter=',', quotechar='|')
                for row in spamreader:
                    # Each row = 0-section_id, 1-rare_words_hash, 2-real_file_name
                    self._registry[row[1]] = row[2]
                    self._update_section_by_row(row)

            logger.info("== Loaded registry from [{}] contains [{}] files."
                        .format(file_path, len(self._registry.keys())))
        except FileNotFoundError:
            logger.info("== Registry file [{}] is not found - so the registry is empty".format(file_path))

    def _update_section_by_row(self, csv_row):
        """
        Update section by given csv_row.
        :param: csv_row: [section_id, file_id, file_path]
        :return: NONE
        """
        file_id = csv_row[1]
        sect_id = int(csv_row[0])
        if sect_id not in self._sections:
            self._sections[sect_id] = []
        self._sections[sect_id].append(file_id)

    def convert_corpus_encoding(self, encoding_from="cp1251", encoding_to="utf-8", backup_dir=None):
        # TODO сделать тесты
        if backup_dir is not None:
            if (not os.path.exists(backup_dir)) or (not os.path.isdir(backup_dir)):
                raise ValueError("!!! Wrong backup path [{}]".format(backup_dir))

        for file_name in self._fileids:
            new_filename = file_name + hash(file_name)+"_conv"
            file_path = join(self.root, file_name)
            new_file_path = join(self.root, new_filename)
            with open(file_path, "r", encoding=encoding_from) as fr:
                with open(new_file_path, "w", encoding=encoding_to) as fw:
                    for line in fr:
                        fw.write(line)

            if backup_dir is not None:
                backup_path = join(backup_dir, file_name)
                os.replace(file_path, backup_path)
                os.replace(new_file_path, file_path)
            else:
                os.replace(new_file_path, file_path)

    def _write_registry_file(self):
        """
        Write content of registry to registry file.

        :return: NONE
        """
        file_path = join(self.root, self._registry_file_name)
        with open(file_path, 'w', newline='') as csvfile:
            spamwriter = writer(csvfile, delimiter=',',
                                quotechar='|', quoting=QUOTE_MINIMAL)

            # === Each row = 0-section_id, 1-rare_words_hash, 2-real_file_name ===
            rows = ([sect_id, rare_words_hash, self._registry[rare_words_hash]]
                    for sect_id in self._sections.keys() for rare_words_hash in self._sections[sect_id])

            for row in rows:
                spamwriter.writerow(row)

    def _get_filenames(self, path=None, name_pattern=None):
        """
        Get filenames from pointed path by regex.
        :param path:
        :param name_pattern: regular expression for filenames
        :return:
        """
        path = self.root if path is None else path
        name_pattern = self._name_pattern if name_pattern is None else name_pattern
        root_path_pointer = FileSystemPathPointer(path)

        return find_corpus_fileids(root_path_pointer, name_pattern)


class RussianPlainTextCorpusReader(RegistryCorpusReader, PlaintextCorpusReader):
    """
    Plain text corpus reader with additional features for russian language.
    """

    def __init__(self, root, fileids=None, encoding="utf8",
                 ngram_range=(1, 3), registry_file_name="registry.csv", twin_checking=True, **kwargs):
        """
        Construct a new plaintext corpus reader for a set of documents
        located at the given root directory.  Example usage:

            >>> root = '/usr/local/share/nltk_data/corpora/webtext/'
            >>> reader = PlaintextCorpusReader(root, '.*\.txt') # doctest: +SKIP

        :param root: The root directory for this corpus.
        :param fileids: A list or regexp specifying the fileids in this corpus.
        :param word_tokenizer: Tokenizer for breaking sentences or
            paragraphs into words.
        :param sent_tokenizer: Tokenizer for breaking paragraphs
            into words.
        :param para_block_reader: The block reader used to divide the
            corpus into paragraph blocks.
        :param external_tokenizer_path: optional external tokenizer
        """
        RegistryCorpusReader.__init__(self, root, fileids=fileids, encoding=encoding,
                                      registry_file_name=registry_file_name,
                                      twin_checking=twin_checking, **kwargs)

        self._stop_words = nltk.corpus.stopwords.words('russian')
        self._ngrams_vectorizer = CountVectorizer(lowercase=False, ngram_range=ngram_range)

    @property
    def stop_words(self):
        return self._stop_words

    def section_docs(self, section):
        """
        Return generator to docs of the section of corpus.
        Each doc is represented as string.
        :return: doc_as_string generator.
        """
        for doc_name in self.section_files_ids(section):
            yield self.raw(doc_name)

    def docs(self):
        """
        Return generator to docs of the corpus.
        Each doc is represented as string.
        :return: doc_as_string generator.
        """
        for doc_name in self._get_filenames():
            yield self.raw(doc_name)

    def tokenized_doc(self, doc, stop_words=None, use_stop_words=True, make_lemmatization=False):
        """
        Return generator to doc of the corpus.
        Doc is represented as token lists.
        :param doc: doc to sent. tokenization
        :param stop_words: custom list of stop words to remove (by default NLTK set is used)
        :param use_stop_words: exclude stop words
        :param make_lemmatization: convert each word to normal form.
        :return: [tokens]
        """
        stop_words = set(self.stop_words if stop_words is None else stop_words) if use_stop_words else set()

        tokenized_doc = []
        for sentence in hp.sentences(doc):
            tokens = [w for w in hp.get_words(sentence) if w not in stop_words]
            if make_lemmatization:
                tokens = [hp.get_lemma(w) for w in tokens]
            tokenized_doc.extend(tokens)
        return tokenized_doc

    def tokenized_docs(self, make_lemmatization=False, use_stop_words=True):
        """
        Return generator to docs of the corpus.
        Each doc is represented as token lists.

        :return: doc token list generator.
        """
        for doc in self.docs():
            yield self.tokenized_doc(doc, make_lemmatization=make_lemmatization, use_stop_words=use_stop_words)

    def contain_word(self, word, with_quantity=True):
        """
        Check that corpus contain given word.
        :param word:
        :param with_quantity: If true the method return number of occurrences.
        :return: True/False OR number of occurrences
        """
        if with_quantity:
            return sum(1 for doc in self.tokenized_docs() for token in doc if
                       hp.get_lemma(word) == hp.get_lemma(token))
        else:
            for doc in self.tokenized_docs(make_lemmatization=True):
                if word.lower() in doc:
                    return True
            return False

    def get_frequent_doc_words(self, doc_name, n_frequent_words=20):
        """
        Get frequent words for given doc
        :param doc_name: path to doc
        :param n_frequent_words: int, quantity of frequent words
        :return: (doc name, [(word 1, quantity), (word 2, quantity), ...])
        """
        doc = self.tokenized_doc(self.raw(doc_name), make_lemmatization=True)

        word_to_tf = []
        for word, quantity in Counter(doc).items():
            if word not in string.punctuation and word not in self.stop_words and len(word) > 3:
                tf_of_single_word = quantity / float(len(doc))
                word_to_tf.append((word, quantity, tf_of_single_word))
        sorted_tfs = sorted(word_to_tf, key=lambda x: x[2], reverse=True)[:n_frequent_words]
        sorted_words = [(el[0], el[1]) for el in sorted_tfs]
        return doc_name, sorted_words

    def get_frequent_docs_words(self, n_frequent_words=20):
        """
        :param n_frequent_words: int, quantity of frequent words
        :return: (doc_name, [(word, quantity)]) generator for each doc from registry
        """
        for doc_name in self._registry.values():
            yield self.get_frequent_doc_words(doc_name, n_frequent_words)

    def get_rare_doc_words(self, doc_name, n_rare_words=10):
        """
        Get n rare words (sorted in ascending order) for given doc
        :param doc_name: path to doc
        :param n_rare_words: int, quantity of rare words
        :return: {doc name: {word 1: quantity, word 2: quantity, ...}}
        """
        doc = self.tokenized_doc(self.raw(doc_name), make_lemmatization=True)

        doc_to_sorted_words = {}
        word_to_quantity = []
        for word, quantity in Counter(doc).items():
            if word not in string.punctuation and word not in self.stop_words and len(word) > 3:
                word_to_quantity.append((word, quantity))
        sorted_words = sorted(word_to_quantity, key=lambda x: x[1])[:n_rare_words]
        doc_to_sorted_words[doc_name] = dict(sorted_words)
        return doc_to_sorted_words

    def get_rare_docs_words(self, n_rare_words=10):
        """
        :param n_rare_words: int, quantity of rare words
        :return: {doc_name: {word: quantity}} generator for each doc from registry
        """
        for doc_name in self._get_filenames():
            yield self.get_rare_doc_words(doc_name, n_rare_words)

    def get_frequent_docs_words_by_countvectorizer(self, n_words=20):
        """
        Count TF for each word of each document.
        TF(WORD) = quantity_of_this_WORD_in_doc/quantity_of_all_words_in_doc)

        :param n_words: int, quantity of frequent words
        :return: word list generator
        """
        tokenized_docs = [self.tokenized_doc(self.raw(doc_name), make_lemmatization=True)
                          for doc_name in self._registry.values()]
        docs = [" ".join(doc) for doc in tokenized_docs]

        vectorizer = CountVectorizer(stop_words=self.stop_words)
        vectors = vectorizer.fit_transform(docs)

        for n, doc in enumerate(vectors.toarray()):
            word_to_tf = []
            for word in vectorizer.vocabulary_.keys():
                tf_of_single_word = doc[vectorizer.vocabulary_[word]] / float(len(doc))
                word_to_tf.append((word, tf_of_single_word))
            sorted_tfs = sorted(word_to_tf, key=lambda kv: kv[1], reverse=True)[:n_words]
            sorted_words = [word[0] for word in sorted_tfs]
            yield sorted_words
