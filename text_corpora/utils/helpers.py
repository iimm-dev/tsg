import os
import pathlib
import nltk
from pymorphy2 import MorphAnalyzer
from nltk.tokenize.toktok import ToktokTokenizer
from nltk.stem import SnowballStemmer
from nltk.util import skipgrams
import hashlib
import text_corpora.utils

pymorphy_morph = MorphAnalyzer()
stem = SnowballStemmer("russian")

module_path = os.path.dirname(text_corpora.utils.__file__)
path = os.path.abspath(os.path.join(module_path, "nltk", "russian.pickle"))
external_tokenizer_path = pathlib.Path(path).as_uri()
sent_tokenizer = nltk.data.load(external_tokenizer_path)

word_tokenizer = ToktokTokenizer()


def stop_words():
    return nltk.corpus.stopwords.words('russian')


def sentences(doc: str):
    """
    Return generator of sentences of given doc.
    :param doc: doc to sent. tokenization.
    :return: sentence generator.
    """
    for sent in sent_tokenizer.tokenize(doc):
        yield sent


def get_words(sentence: str):
    """
    Perform tokenization of given sentence.
    !!! Do no make tokenization of entire doc - that gives bad results.
    Make sent. tokenization at first and then word tokenization.
    :param sentence: sentence as string
    :return: [tokens]
    """
    return word_tokenizer.tokenize(sentence, return_str=False)


def get_lemma(word):
    """
    Return lemma of given word.
    :param word:
    :return: lemma
    """
    return pymorphy_morph.parse(word)[0][2]


def get_basic(word):
    """
    Stem word - return basic of given word.
    :param word:
    :return: word basic.
    """
    return stem.stem(word)


def get_ngrams(tokens: list, length):
    """
    Get 2,3 or n-grams from list of tokens
    :param: tokens: sentence tokens
    :param: length: n-gram length
    :return: [n-grams]
    """
    ngrams = skipgrams(tokens, length, len(tokens))  # skip-distance = len(tokens)
    # ... join each tuple into string
    return [' '.join(ngram) for ngram in ngrams]


def get_ngram_lemmas(ngram):
    """
    Return lemma of given ngram (i.e. string with some words)
    :param ngram: string with some words (e.g. "angry cats")
    :return: string with lemma of each word (e.g. "angry cat")
    """
    words = [wf for wf in [w.strip() for w in ngram.split(" ")] if wf]
    normalised_words = [pymorphy_morph.parse(w)[0].normal_form for w in words]

    return " ".join(normalised_words)


def get_ngram_basics(ngram):
    """
    Use stemmer to return basic of given ngram (i.e. string with some words)
    :param ngram: string with some words (e.g. "angry cats")
    :return: string with basics of each word (e.g. "angry cat")
    """
    words = [wf for wf in [w.strip() for w in ngram.split(" ")] if wf]
    stemmed_words = [stem.stem(w) for w in words]

    return " ".join(stemmed_words)


def get_hash(obj):
    """
    Return hash for an object of any type
    :param obj: e.g. dict/list/set
    :return:
    """
    to_string = str(obj)
    words_hash = hashlib.md5(to_string.encode()).hexdigest()
    return words_hash
