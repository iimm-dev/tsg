
from text_corpora.utils.corpus_readers import RussianPlainTextCorpusReader
import os

# --- Init text corpus reader ---
# corpus_root: tsg/text_corpuses/test_text_corpus/docs
corpus_root = os.path.abspath("./test_text_corpus")

# DOC_PATTERN - where corpus files are located: corpus_root/docs
DOC_PATTERN = r"docs/\w+.txt"
corpusReader = RussianPlainTextCorpusReader(corpus_root, fileids=DOC_PATTERN, encoding="utf-8",
                                            use_twin_checking=True)

# ... split/resplit corpus into sections (can be done single time for new text corpus) ---
# corpusReader.split_to_sections(20)

# get corpus elements ...
for doc in corpusReader.docs():
    print(corpusReader.tokenized_doc(doc))
