Стенин В.А. ©
Доктор технических наук,
Северный Арктический федеральный университет
ТЕРМОДИНАМИКА ОСЕВОГО РАСТЯЖЕНИЯ СТЕРЖНЯ
Аннотация
В работе предлагается учитывать плотность поверхностной энергии, когда
внешнее воздействие существенно повышает подвижность поверхностного слоя.
Расчетом показано, что в этом случае относительные продольные и поперечные
деформации при осевом растяжении стального стержня распределяются неравномерно по
длине, причем максимальное их значение наблюдается в средней части стержня.
Ключевые слова: энергия
натяжение, продольные и
Пуассона.

деформации, осевое растяжение стержня, поверхностное
поперечные деформации, модуль упругости, коэффициент

Keywords: strain energy, axial stretching rod, surface tension, longitudinal and transverse
deformation, elastic modulus, Poisson's ratio.
Допущение о существовании удельной потенциальной энергии деформации
находится в соответствии с предположением об обратимости изотермического и
адиабатического процессов деформации и определяет тем самым упругое поведение
материалов [1,54]. Для линейно – упругого тела, находящегося в равновесии при заданных
объемных и поверхностных силах, в соответствии с теоремой Клапейрона энергия
деформации равна алгебраической сумме поверхностной и объемной энергий и составляет
половину работы, которая совершается внешними силами на перемещениях из исходного
состояния равновесия в конечное состояние. В дифференциальной форме это уравнение
имеет вид [1,89;2,85]:
F ⋅ dL
dU =
= σ ⋅ dV + f ⋅ dS ,
(1)
2
где U - энергия деформации, Дж; σ - осевое напряжение, Н м 2 ; V - объем деформируемого
тела, м 3 ; f - плотность поверхностной энергии, Н м ; S площадь
поверхности
2
деформируемого тела, м ; F - внешняя сила, Н ; L - длина деформируемого тела, м .
Плотность поверхностной энергии (поверхностное натяжение) можно найти по
следующей зависимости [3,132]:
dW
f =
,
(2)
dS
где W – работа, требуемая для увеличения площади поверхности, Дж.
Поверхностное натяжение ориентировочно можно определить, по аналогии с
жидкостью, измеряя силу, которую нужно приложить, чтобы изменить площадь
поверхности [3,132]. В нашем случае (см. рис.1), внешняя сила F, растягивающая стержень,
изменяет его диаметр на ∆d = D − d и длину на ∆L . Работа ∆W равна произведению силы
на перемещение ∆W = F ⋅ ∆d , а изменение площади поверхности соответствует ∆S = π ⋅ L ⋅ ∆d
. Изменением площади поверхности на приращении ∆L можно пренебречь, так как
∆S ∆L = π ⋅ ∆L ⋅ ∆d . Тогда зависимость (2) запишем так:
dW
F
f =
=
.
(3)
dS π ⋅ L
В случае одноосного растяжения осевое напряжение σ определяется по формуле
σ = F / A [2,49], тогда с учетом (3) представим величину σ в следующем виде:
©

Стенин В.А., 2014 г.

4⋅ f ⋅ L
,
(4)
D2
где А – площадь сечения стержня, м 2 ; D - диаметр стержня, м .
В соответствии с (4), для стержней, у которых D << L σ >> f , поэтому в расчетной
практике величиной f
пренебрегают. Для примера, вычислим энергию деформации
L = 1м , площадью сечения A = 1cм 2 , модулем упругости
стального стержня длиной
Е = 20 ⋅1010 Н м 2 и
коэффициентом
Пуассона µ = 0,25 . На
стержень
действует
растягивающая сила F = 20кН . Абсолютное удлинение равно: ∆L = F ⋅ L / E ⋅ A = 0,001м .
ε = 0,001 . Величина энергии деформации:
Линейная
деформация составляет
∆U = F ⋅ ∆L / 2 = 10 Н ⋅ м . Значение ∆U , определенное из правой части уравнения (1), если
считать, что f ≈ 0 , составляет такую же величину: ∆U = σ ⋅ ∆V = 2 ⋅ F ⋅ µ ⋅ ε ⋅ L = 10 Н ⋅ м .
Однако, в ряде случаев, игнорирование силой поверхностного натяжения может
способствовать некорректной трактовке результатов физических экспериментов. К примеру,
в [4,15;5,10] доказано существование поверхностного натяжения твердых тел. Изменение
формы
в результате поверхностного натяжения в твердых телах всегда гораздо
медленнее, чем в жидкостях, что объясняется существенно меньшей подвижностью
поверхностного слоя. В случае внешнего воздействия значительно повышается
подвижность поверхностного слоя твердого тела. Условие равновесия, при котором
поверхностная
энергия
минимальна, достигается, когда
поверхность
приобретает
оптимальную форму. Для жидкости – сферическая форма поверхности, при которой ее
кривизна постоянная для всех точек. Это условие минимума площади поверхности.
Рассмотрим стержень, к которому приложена растягивающая сила F (рис.1). Относительная
продольная деформация стержня равна ε = ∆L / L , относительная поперечная деформация
составляет ε ′ = ∆d / D . Коэффициент Пуассона определяется по уравнению µ = ε ′ / ε и
считается величиной постоянной для данного материала. Если при растяжении
проявляются силы поверхностного натяжения, то они
стремятся оптимизировать
поверхность деформируемого тела, приближая ее к вогнутой сферической.

σ=

Рис.1. Принятая схема изменения формы стержня при растяжении

Рис.2.Предлагаемая схема изменения формы стержня при растяжении

Продольное сечение стержня приобретает вид фигуры ADCRML (см. рис.2).
Прямоугольное удвоенное изменение площади сечения ACHE (KNRL) заменено на

удвоенное сегментное ABCD (LPRM), одинаковое по площади прямоугольному. Высота
сегмента ABCD равна BD и по величине больше, чем высота AE прямоугольника ACHE.
Для
стержня, на
который
действует
растягивающая
сила F = 20кН , площадь
2
прямоугольника ACHE равна 0,0141м . При той же площади сегмент ABCD имеет высоту
2,11 ⋅10 −4 м . Высота прямоугольника ACHE составляет 1,41 ⋅10 −4 м . В сечении BP изменение
диаметра стержня при растяжении равно ∆d = 4,22 ⋅10 −4 м , относительная поперечная
деформация ε ′ = 3,74 ⋅10 −4 . При известном значении
коэффициента Пуассона µ = 0,25
величина относительной продольной деформации стержня
в сечении BP составит
−3
ε = 1,5 ⋅10 , что в 1,5 раза больше расчетной.
Таким образом, если при деформации стержня учитывать силы поверхностного
натяжения, то следует полагать, что относительные продольные и поперечные деформации
распределены неравномерно по длине стержня, причем максимальное их значение
наблюдается в средней части стержня. Данная гипотеза в определенной мере поясняет
появление шейки на образце при достижении пределов упругости и прочности материала,
а также бочкообразную форму образца при сжатии [2,75,87].
Литература
1.
2.
3.
4.
5.

Хан Х. Теория упругости: Основы линейной теории и ее применения. - М.:Мир,1988. – 344с.
Александров А.В. Сопротивление материалов. – М.: Высшая школа, 2003. – 560с.
Кухлинг Х. Справочник по физике. - М.: Мир, 1982. -520с.
Адам Н.К. Физика и химия поверхностей. - М.:ОГИЗ, 1947. – 552с.
Физика и химия твердого тела. Т.2: Фистуль В.И. – М.: Металлургия, 1995. – 320с.

