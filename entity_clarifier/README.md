## Clarifier module

Use `entity_clarifier.py` script to get clarified entity boundaries in sentences according to dependency tree.

Requirements:

* [Russian spaCy model](https://spacy.io/models/ru#ru_core_news_sm)

Use `_example_entity_clarification.py` test script as entity clarifier template. 
