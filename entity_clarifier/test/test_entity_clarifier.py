import os
import re

import augmentator.helpers as hp
import spacy
import unittest
from spacy.tokens import Doc, Token
import augmentator.augmentation as aa
import utils.file_utils as ut
import entity_clarifier.entity_clarifier as cl


class EntityClarifierTestCase(unittest.TestCase):
    def setUp(self):
        return

    def tearDown(self):
        return

    def initModelAndTokenizer(self):
        # init spacy model ...
        self.nlp = spacy.load("ru_core_news_sm")

    def test_create_and_validate_clarified_entities(self):
        in_dir = "./data/test_no_border_csv"

        tmp_output_dir = "./data/out/draft_clarified_entities_border_csv"
        out_draft_clarified_entities_csv_file = "./data/out/draft_clarified_entities.csv"
        output_dir = "./data/out/clarified_entities_border_csv"

        cl.create_clarified_entities(in_dir, out_draft_clarified_entities_csv_file, tmp_output_dir, input_files_quantity = 38)

        # === draft clarified entity file for manual filtering
        validated_clarified_entities_file_path = out_draft_clarified_entities_csv_file

        cl.remove_bad_clarified_entities(tmp_output_dir, validated_clarified_entities_file_path, output_dir)


