import csv
import logging
import os
import re
import sre_constants
import string
from setgen.boundaries_defenition import __get_corrected_russian_sentence
from setgen.boundaries_defenition import __get_correct_entity
from setgen.boundaries_defenition import __get_entity_boundaries
import spacy
import augmentator.helpers as hp
import glob
import utils.file_utils as ut

# --- init logger ---
from text_corpora.utils.clogger import get_logger

logger = get_logger("./", "common")


def create_clarified_entities(input_dir, draft_clarified_entities_file, output_dir, input_files_quantity=0):
    """
    @param input_dir: dir containing csv-files - [sentence, entity, category] (i.e. without entity borders)
    @param draft_clarified_entities_file: file with [clarified_entity, category] for validation.
    @param output_dir: dir containing csv-files with clarified entities - [sentence, clarified_entity, category] (i.e without entity borders)
    @return:
    """
    ut.recreate_dirs([output_dir])

    #TODO cosider using - for n, line in enumerate(ut._rows_from_csv_files(in_dir_of_csv_files_with_borders)):
    # Define files for processing ...
    filenames = sorted(os.listdir(input_dir), key=lambda x: str(len(x)) + x)
    input_files_quantity = len(filenames) if input_files_quantity > len(filenames) else input_files_quantity
    filenames = filenames if input_files_quantity == 0 else filenames[:input_files_quantity]

    # clarified_entities = [(clarified_entity, cat)]
    clarified_entities = set()
    # init spacy model ...
    nlp = spacy.load("ru_core_news_sm")

    stop = 45

    for file_name in filenames:
        logger.info("=== Processing file - [{}] ===".format(file_name))
        csv_path = os.path.join(input_dir, file_name)
        unvalidated_entities_filepath = os.path.join(output_dir, file_name)

        with open(unvalidated_entities_filepath, "w") as fw:
            with open(csv_path, "r") as fr:
                for row in csv.reader(fr):  # row = [sentence, entity, category]
                    sentence = __get_corrected_russian_sentence(row[0])
                    entity = __get_correct_entity(row[1])
                    category = row[2]

                    # === Clarify entity...
                    parsed_sent = nlp(sentence)
                    if parsed_sent is None:
                        continue
                    try:
                        lb, rb = __get_entity_boundaries(sentence, entity)
                    except (TypeError, sre_constants.error):
                        logger.warn(f"__get_entity_boundaries(sentence,entity) return None from [{sentence}]")
                        continue

                    entity_tokens = hp._get_entity_tokens(parsed_sent, lb, rb)
                    if len(hp._get_head_tokens(entity_tokens)) == 0:
                        continue
                    head_token = hp._get_head_tokens(entity_tokens)[0]
                    clarified_entity_tokens = hp._clarify_entity_tokens_by_nearest_childs_of_head_token(entity_tokens,
                                                                                                      head_token)

                    # === Remove first and last "bad" tokens from clarified entity tokens ...
                    first = clarified_entity_tokens[0]
                    if len(clarified_entity_tokens) > 0:
                        if (first.pos_ in ["PUNCT", "CCONJ", "ADP"] or first.is_stop):
                            clarified_entity_tokens = clarified_entity_tokens[1:]

                    if len(clarified_entity_tokens) > 0:
                        last = clarified_entity_tokens[-1]
                        if (last.pos_ in ["PUNCT", "CCONJ", "ADP"] or last.is_stop):
                            clarified_entity_tokens = clarified_entity_tokens[:-1]

                    # print("==" +" ".join([t.text for t in clarified_entity_tokens]))
                    # if len(clarified_entity_tokens)>1:
                    #     print(sentence)
                    #     print("==" + " ".join([f"{t.text}[{t.dep_}]  " for t in clarified_entity_tokens]))

                    # === Create and write clarified entity ...
                    clarified_entity = " ".join([f"{t.text}" for t in clarified_entity_tokens])
                    print(clarified_entity)
                    clarified_entities.add((clarified_entity, category))

                    writer = csv.writer(fw, delimiter=',')
                    writer.writerow([sentence, clarified_entity, category])

                    # if stop==0:
                    #     break
                    # else:
                    #     stop-=1

    # === Write clarified_entities to file ===
    clarified_entities = sorted(clarified_entities, key=lambda x: x[0])
    with open(draft_clarified_entities_file, "w") as fw:
        writer = csv.writer(fw, delimiter=',')
        for entity, category in clarified_entities:
            writer.writerow([entity, category])


def remove_bad_clarified_entities(input_dir, validated_clarified_entities_file_path, output_dir):
    """
    Save only sentences with validated_clarified_entities in output dir.
    @param input_dir: dir csv-files with [sentence, clarified_entity]
    @param validated_clarified_entities_file_path: csv-file with [validated_clarified_entity, validated_category]
    @param output_dir: dir csv-files with [sentence, validated_clarified_entity, validated_category]
    @return:
    """
    ut.recreate_dirs([output_dir])

    if not ut.is_file_exists(validated_clarified_entities_file_path):
        raise Exception(
            f"Can't validate clarified entities - bad validated_clarified_entities_file [{validated_clarified_entities_file_path}]")

    # validated_clarified_entities = set()
    validated_clarified_entities = {}  # validated_clarified_entities = {entity : cat}
    filenames = sorted(os.listdir(input_dir))

    all_sentence_quantity = 0
    bad_entities_sentence_quantity = 0
    # Read validated entities ...
    with open(validated_clarified_entities_file_path, "r") as ent_file:
        for row in csv.reader(ent_file):  # row = [entity]
            if len(row) == 0:
                continue
            entity = row[0]
            category = row[1]
            # validated_clarified_entities.add((entity, category))
            validated_clarified_entities[entity] = category

    # For each files with unvalidated entities ...
    for file_name in filenames:
        logger.info("=== Processing file - [{}] ===".format(file_name))
        csv_path = os.path.join(input_dir, file_name)
        new_csv_path = os.path.join(output_dir, file_name)

        with open(csv_path, "r") as fr:
            with open(new_csv_path, "w") as fw:
                writer = csv.writer(fw, delimiter=',')
                # ... read each row with unvalidated clarified entity ...
                for row in csv.reader(fr):  # row = [sentence, entity, category]
                    sentence = row[0]
                    entity = __get_correct_entity(row[1])
                    category = row[2]
                    all_sentence_quantity += 1
                    # ... remain row if clarified entity is in validated_clarified_entities ...
                    if entity in validated_clarified_entities:
                        validated_category = validated_clarified_entities[entity]
                        writer.writerow([sentence, entity, validated_category])
                    else:
                        logger.info("Skip bad_entities_sentence: [%s]", " ".join([sentence, entity, category]))
                        bad_entities_sentence_quantity += 1

    logger.info(
        "\n=== All sentence quantity: [%d] = bad_entities_sentence_quantity [%d] + good_entities_sentence_quantity [%d]\n",
        all_sentence_quantity, bad_entities_sentence_quantity, all_sentence_quantity - bad_entities_sentence_quantity)
