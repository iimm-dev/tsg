import os
import sys
import entity_clarifier.entity_clarifier as rd

from setgen.final_trainset_generation import get_trainset_with_boundaries
from setgen.final_trainset_generation import get_final_SPACY_trainsets

# --- init logger ---
from text_corpora.utils.clogger import get_logger
logger = get_logger("./", "common")


# ====================================================================================================
# === Create draft clarified entities from csv-files [sentence, entity, category]
# ====================================================================================================
in_dir = "./test/data/test_no_border_csv"

# === Draft clarified entity file for manual validation
out_draft_clarified_entities_csv_file = "./test/data/out/draft_clarified_entities.csv"

# === Draft clarified entity out file
tmp_output_dir = "./test/data/out/draft_clarified_entities_border_csv"

temp_out_dir_for_csv_files_with_good_boundaries="./test/data/out/tmp_files_with_good_boundaries"
temp_out_dir_for_csv_files_with_wrong_boundaries="./test/data/out/tmp_files_with_wrong_boundaries"

# === Final clarified entity csv-files with validated entities [sentence, clarified entity, category]
output_dir = "./test/data/out/clarified_entities_border_csv"

output_dir_with_train_files = "./test/data/out/train_files"

rd.create_clarified_entities(in_dir, out_draft_clarified_entities_csv_file, tmp_output_dir, input_files_quantity=38)

# ==========================================================================
# === Perform manual validation of out_draft_clarified_entities_csv_file -
# remove rows and/or change category of clarified entity..
# ==========================================================================
validated_clarified_entities_file_path = out_draft_clarified_entities_csv_file
# sys.exit(0). _exit()


# ====================================================================================================================
# === Perform correction of out_draft_clarified_entities_csv_file in accordance with validated_clarified_entities_file
# ====================================================================================================================
in_dir = tmp_output_dir
rd.remove_bad_clarified_entities(in_dir, validated_clarified_entities_file_path, output_dir)

# =======================================
# === Add entity borders to csv-files
# =======================================
in_dir = output_dir
dir_for_files_with_boundaries = get_trainset_with_boundaries(in_dir,
                                                             temp_out_dir_for_csv_files_with_good_boundaries,
                                                             temp_out_dir_for_csv_files_with_wrong_boundaries)

# ====================================================================================================================
# === OPTIONAL Perform augmentation of csv-files - go to augmentation/__augmentation_example.py
# Note: augmentation erase entity borders
# ====================================================================================================================


# ================================
# === get final trainset files ===
# ================================

trainset_file_paths = get_final_SPACY_trainsets(dir_for_files_with_boundaries, output_dir_with_train_files,
                                                row_quantity_per_file="600000",
                                                out_file_prefix="train_")


files = [f for f in os.listdir(output_dir_with_train_files) if
         os.path.isfile(os.path.join(output_dir_with_train_files, f))]

logger.info(f"=== Got {len(files)} train files in {output_dir_with_train_files}")
